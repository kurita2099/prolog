
//Palette extern variables declaration
extern const INT16U _TEXT_field1_Palette0[];
extern const INT16U _TEXT_field1_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _ff01a_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _ff01a_IMG0000_CellData[];
extern const INT16U _ff01b_IMG0000_IndexData[];
extern const INT16U _ff01b_IMG0000_CellData[];
