
//Palette extern variables declaration
extern const INT16U _TEXT_user_Palette0[];
extern const INT16U _TEXT_user_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _fs02003_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _fs02003_IMG0000_CellData[];
extern const INT16U _fs02006_IMG0000_IndexData[];
extern const INT16U _fs02006_IMG0000_CellData[];
extern const INT16U _fs02009_IMG0000_IndexData[];
extern const INT16U _fs02009_IMG0000_CellData[];
extern const INT16U _fs02011_IMG0000_IndexData[];
extern const INT16U _fs02011_IMG0000_CellData[];
extern const INT16U _us_fl1010a_IMG0000_IndexData[];
extern const INT16U _us_fl1010a_IMG0000_CellData[];
extern const INT16U _us_fl1010b_IMG0000_IndexData[];
extern const INT16U _us_fl1010b_IMG0000_CellData[];
extern const INT16U _us_ff01001_IMG0000_IndexData[];
extern const INT16U _us_ff01001_IMG0000_CellData[];
extern const INT16U _us_fl01011a_IMG0000_IndexData[];
extern const INT16U _us_fl01011a_IMG0000_CellData[];
extern const INT16U _us_f01011b_IMG0000_IndexData[];
extern const INT16U _us_f01011b_IMG0000_CellData[];
