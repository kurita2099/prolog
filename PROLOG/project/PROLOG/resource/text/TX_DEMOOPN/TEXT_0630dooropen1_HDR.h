
//Palette extern variables declaration
extern const INT16U _TEXT_0630dooropen1_Palette0[];
extern const INT16U _TEXT_0630dooropen1_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _do1_fl01001_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _do1_fl01001_IMG0000_CellData[];
extern const INT16U _do1_fl01002a_IMG0000_IndexData[];
extern const INT16U _do1_fl01002a_IMG0000_CellData[];
extern const INT16U _do1_fl01002b_IMG0000_IndexData[];
extern const INT16U _do1_fl01002b_IMG0000_CellData[];
extern const INT16U _do1_fl01011a_IMG0000_IndexData[];
extern const INT16U _do1_fl01011a_IMG0000_CellData[];
extern const INT16U _do1_fl01011b_IMG0000_IndexData[];
extern const INT16U _do1_fl01011b_IMG0000_CellData[];
