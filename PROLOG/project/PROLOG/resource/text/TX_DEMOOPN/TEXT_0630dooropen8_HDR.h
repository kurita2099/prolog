
//Palette extern variables declaration
extern const INT16U _TEXT_0630dooropen8_Palette0[];
extern const INT16U _TEXT_0630dooropen8_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _do8_fl01001_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _do8_fl01001_IMG0000_CellData[];
extern const INT16U _do8_fl01009a_IMG0000_IndexData[];
extern const INT16U _do8_fl01009a_IMG0000_CellData[];
extern const INT16U _do8_fl01009b_IMG0000_IndexData[];
extern const INT16U _do8_fl01009b_IMG0000_CellData[];
extern const INT16U _do8_fl01011a_IMG0000_IndexData[];
extern const INT16U _do8_fl01011a_IMG0000_CellData[];
extern const INT16U _do8_fl01011b_IMG0000_IndexData[];
extern const INT16U _do8_fl01011b_IMG0000_CellData[];
