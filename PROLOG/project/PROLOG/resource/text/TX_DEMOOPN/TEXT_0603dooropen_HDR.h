
//Palette extern variables declaration
extern const INT16U _TEXT_0603dooropen_Palette0[];
extern const INT16U _TEXT_0603dooropen_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _dooropen_01_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _dooropen_01_IMG0000_CellData[];
extern const INT16U _dooropen_02_IMG0000_IndexData[];
extern const INT16U _dooropen_02_IMG0000_CellData[];
extern const INT16U _dooropen_03_IMG0000_IndexData[];
extern const INT16U _dooropen_03_IMG0000_CellData[];
extern const INT16U _dooropen_04_IMG0000_IndexData[];
extern const INT16U _dooropen_04_IMG0000_CellData[];
extern const INT16U _dooropen_bg_IMG0000_IndexData[];
extern const INT16U _dooropen_bg_IMG0000_CellData[];
