
//Palette extern variables declaration
extern const INT16U _TEXT_0630dooropen3_Palette0[];
extern const INT16U _TEXT_0630dooropen3_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _do3_fl01001_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _do3_fl01001_IMG0000_CellData[];
extern const INT16U _do3_fl01004a_IMG0000_IndexData[];
extern const INT16U _do3_fl01004a_IMG0000_CellData[];
extern const INT16U _do3_fl01004b_IMG0000_IndexData[];
extern const INT16U _do3_fl01004b_IMG0000_CellData[];
extern const INT16U _do3_fl01011a_IMG0000_IndexData[];
extern const INT16U _do3_fl01011a_IMG0000_CellData[];
extern const INT16U _do3_fl01011b_IMG0000_IndexData[];
extern const INT16U _do3_fl01011b_IMG0000_CellData[];
