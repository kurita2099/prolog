
//Palette extern variables declaration
extern const INT16U _TEXT_0630dooropen7_Palette0[];
extern const INT16U _TEXT_0630dooropen7_Palette1[];

//Text Cell index extern variables declaration
extern const INT16U _do7_fl01001_IMG0000_IndexData[];

//Text Cell data extern variables declaration
extern const INT16U _do7_fl01001_IMG0000_CellData[];
extern const INT16U _do7_fl01008a_IMG0000_IndexData[];
extern const INT16U _do7_fl01008a_IMG0000_CellData[];
extern const INT16U _do7_fl01008b_IMG0000_IndexData[];
extern const INT16U _do7_fl01008b_IMG0000_CellData[];
extern const INT16U _do7_fl01011a_IMG0000_IndexData[];
extern const INT16U _do7_fl01011a_IMG0000_CellData[];
extern const INT16U _do7_fl01011b_IMG0000_IndexData[];
extern const INT16U _do7_fl01011b_IMG0000_CellData[];
