
//Palette extern variables declaration
extern const INT16U _SPRITE_userf_Palette1[];
extern const INT16U _SPRITE_userf_Palette3[];

//Sprite Cell index extern variables declaration
extern const INT16U _charaA_IMG0000_CellIdx[];
extern const INT16U _charaA_IMG0001_CellIdx[];
extern const INT16U _charaA_IMG0002_CellIdx[];
extern const INT16U _charaA_IMG0003_CellIdx[];
extern const INT16U _charaA_IMG0004_CellIdx[];
extern const INT16U _charaA_IMG0005_CellIdx[];
extern const INT16U _charaA_IMG0006_CellIdx[];
extern const INT16U _charaA_IMG0007_CellIdx[];
extern const INT16U _charaA_IMG0008_CellIdx[];
extern const INT16U _charaA_IMG0009_CellIdx[];
extern const SPRITE charaA_SP[];
extern const INT16U _charaB_IMG0000_CellIdx[];
extern const INT16U _charaB_IMG0001_CellIdx[];
extern const INT16U _charaB_IMG0002_CellIdx[];
extern const INT16U _charaB_IMG0003_CellIdx[];
extern const INT16U _charaB_IMG0004_CellIdx[];
extern const INT16U _charaB_IMG0005_CellIdx[];
extern const INT16U _charaB_IMG0006_CellIdx[];
extern const INT16U _charaB_IMG0007_CellIdx[];
extern const INT16U _charaB_IMG0008_CellIdx[];
extern const INT16U _charaB_IMG0009_CellIdx[];
extern const SPRITE charaB_SP[];
extern const INT16U _charaD_IMG0000_CellIdx[];
extern const INT16U _charaD_IMG0001_CellIdx[];
extern const INT16U _charaD_IMG0002_CellIdx[];
extern const INT16U _charaD_IMG0003_CellIdx[];
extern const INT16U _charaD_IMG0004_CellIdx[];
extern const INT16U _charaD_IMG0005_CellIdx[];
extern const INT16U _charaD_IMG0006_CellIdx[];
extern const INT16U _charaD_IMG0007_CellIdx[];
extern const INT16U _charaD_IMG0008_CellIdx[];
extern const INT16U _charaD_IMG0009_CellIdx[];
extern const SPRITE charaD_SP[];
extern const SPRITE* SpritesPool[];

//Sprite Cell data extern variables declaration
extern const INT16U _SPRITE_userf_CellData[];
