#ifndef	__IO_EXTENDER_HEADER__
#define	__IO_EXTENDER_HEADER__
//======================================================================
//======================================================================
/* Normal registers */
#define	GPBA02_BUFA					0x00
#define	GPBA02_BUFB					0x01
#define	GPBA02_BUFC					0x02
#define GPBA02_NA03					0x03
#define	GPBA02_DIRA					0x04
#define	GPBA02_DIRB					0x05
#define	GPBA02_DIRC					0x06
#define GPBA02_NA07					0x07
#define	GPBA02_ATTA					0x08
#define	GPBA02_ATTB					0x09
#define	GPBA02_ATTC					0x0a
#define GPBA02_NA0B					0x0b
#define GPBA02_DATA					0x0c
#define GPBA02_DATB					0x0d
#define GPBA02_DATC					0x0e
#define GPBA02_STATUS				0x0f
/* And registers */
#define	GPBA02_BUFA_AND				0x10
#define	GPBA02_BUFB_AND				0x11
#define	GPBA02_BUFC_AND				0x12
#define GPBA02_NA13					0x13
#define	GPBA02_DIRA_AND				0x14
#define	GPBA02_DIRB_AND				0x15
#define	GPBA02_DIRC_AND				0x16
#define GPBA02_NA14					0x17
#define	GPBA02_ATTA_AND				0x18
#define	GPBA02_ATTB_AND				0x19
#define	GPBA02_ATTC_AND				0x1a
#define GPBA02_NA1B					0x1b
#define GPBA02_NA1C					0x1c
#define GPBA02_NA1D					0x1d
#define GPBA02_NA1E					0x1e
#define GPBA02_NA1F					0x1f
/* Or registers */
#define	GPBA02_BUFA_OR				0x20
#define	GPBA02_BUFB_OR				0x21
#define	GPBA02_BUFC_OR				0x22
#define GPBA02_NA23					0x23
#define	GPBA02_DIRA_OR				0x24
#define	GPBA02_DIRB_OR				0x25
#define	GPBA02_DIRC_OR				0x26
#define GPBA02_NA24					0x27
#define	GPBA02_ATTA_OR				0x28
#define	GPBA02_ATTB_OR				0x29
#define	GPBA02_ATTC_OR				0x2a
#define GPBA02_NA2B					0x2b
#define GPBA02_NA2C					0x2c
#define GPBA02_NA2D					0x2d
#define GPBA02_NA2E					0x2e
#define GPBA02_NA2F					0x2f
/* Xor registers */
#define	GPBA02_BUFA_XOR				0x30
#define	GPBA02_BUFB_XOR				0x31
#define	GPBA02_BUFC_XOR				0x32
#define GPBA02_NA33					0x33
#define	GPBA02_DIRA_XOR				0x34
#define	GPBA02_DIRB_XOR				0x35
#define	GPBA02_DIRC_XOR				0x36
#define GPBA02_NA34					0x37
#define	GPBA02_ATTA_XOR				0x38
#define	GPBA02_ATTB_XOR				0x39
#define	GPBA02_ATTC_XOR				0x3a
#define GPBA02_NA3B					0x3b
#define GPBA02_NA3C					0x3c
#define GPBA02_NA3D					0x3d
#define GPBA02_NA3E					0x3e
#define GPBA02_NA3F					0x3f

//----------------------------------------------
//	PA
//----------------------------------------------
#define	GPBA02_SD_CD_BIT		0x01
#define	GPBA02_SD_WP_BIT		0x02
#define	GPBA02_PHONE_DET_BIT	0x04
#define	GPBA02_LINE_DET_BIT		0x08
#define	GPBA02_CHARGE_DET_BIT	0x10
#define	GPBA02_USB_DET_BIT		0x20
#define	GPBA02_ALM_DET_BIT		0x40
#define	GPBA02_WIFI_DET_BIT		0x80

#define	gpba02_get_sd_cd()			(gpba02_read(GPBA02_DATA) & GPBA02_SD_CD_BIT)
#define	gpba02_get_sd_wp()			(gpba02_read(GPBA02_DATA) & GPBA02_SD_WP_BIT)
#define	gpba02_get_phone_det()		(gpba02_read(GPBA02_DATA) & GPBA02_PHONE_DET_BIT)
#define	gpba02_get_line_det()		(gpba02_read(GPBA02_DATA) & GPBA02_LINE_DET_BIT)
#define	gpba02_get_charge_det()		(gpba02_read(GPBA02_DATA) & GPBA02_CHARGE_DET_BIT)
#define	gpba02_get_usb_det()		(gpba02_read(GPBA02_DATA) & GPBA02_USB_DET_BIT)
#define	gpba02_get_alm_det()		(gpba02_read(GPBA02_DATA) & GPBA02_ALM_DET_BIT)
#define	gpba02_get_wifi_det()		(gpba02_read(GPBA02_DATA) & GPBA02_WIFI_DET_BIT)
//----------------------------------------------
//	PB
//----------------------------------------------
#define	GPBA02_SD_POWER_BIT		0x01
#define	GPBA02_CSI_POWER_BIT	0x02
#define	GPBA02_CSI_RESET_BIT	0x04
#define	GPBA02_CSI_PWDN_BIT		0x08
#define	GPBA02_WIFI_RESET_BIT	0x10
#define	GPBA02_AMP_EN_BIT		0x20
#define	GPBA02_LCD_EN_BIT		0x40
#define	GPBA02_CHARGE_EN_BIT	0x80

#define	gpba02_get_sd_power()		(gpba02_read(GPBA02_DATB) & GPBA02_SD_POWER_BIT)
#define	gpba02_set_sd_power()		gpba02_write(GPBA02_BUFB_OR, GPBA02_SD_POWER_BIT)
#define	gpba02_clr_sd_power()		gpba02_write(GPBA02_BUFB_AND, ~GPBA02_SD_POWER_BIT)
#define	gpba02_inv_sd_power()		gpba02_write(GPBA02_BUFB_XOR, GPBA02_SD_POWER_BIT)
#define	gpba02_get_csi_power()		(gpba02_read(GPBA02_DATB) & GPBA02_CSI_POWER_BIT)
#define	gpba02_set_csi_power()		gpba02_write(GPBA02_BUFB_OR, GPBA02_CSI_POWER_BIT)
#define	gpba02_clr_csi_power()		gpba02_write(GPBA02_BUFB_AND, ~GPBA02_CSI_POWER_BIT)
#define	gpba02_inv_csi_power()		gpba02_write(GPBA02_BUFB_XOR, GPBA02_CSI_POWER_BIT)
#define	gpba02_get_csi_reset()		(gpba02_read(GPBA02_DATB) & GPBA02_CSI_RESET_BIT)
#define	gpba02_set_csi_reset()		gpba02_write(GPBA02_BUFB_OR, GPBA02_CSI_RESET_BIT)
#define	gpba02_clr_csi_reset()		gpba02_write(GPBA02_BUFB_AND, ~GPBA02_CSI_RESET_BIT)
#define	gpba02_inv_csi_reset()		gpba02_write(GPBA02_BUFB_XOR, GPBA02_CSI_RESET_BIT)
#define	gpba02_get_csi_pwdn()		(gpba02_read(GPBA02_DATB) & GPBA02_CSI_PWDN_BIT)
#define	gpba02_set_csi_pwdn()		gpba02_write(GPBA02_BUFB_OR, GPBA02_CSI_PWDN_BIT)
#define	gpba02_clr_csi_pwdn()		gpba02_write(GPBA02_BUFB_AND, ~GPBA02_CSI_PWDN_BIT)
#define	gpba02_inv_csi_pwdn()		gpba02_write(GPBA02_BUFB_XOR, GPBA02_CSI_PWDN_BIT)
#define	gpba02_get_wifi_reset()		(gpba02_read(GPBA02_DATB) & GPBA02_WIFI_RESET_BIT)
#define	gpba02_set_wifi_reset()		gpba02_write(GPBA02_BUFB_OR, GPBA02_WIFI_RESET_BIT)
#define	gpba02_clr_wifi_reset()		gpba02_write(GPBA02_BUFB_AND, ~GPBA02_WIFI_RESET_BIT)
#define	gpba02_inv_wifi_reset()		gpba02_write(GPBA02_BUFB_XOR, GPBA02_WIFI_RESET_BIT)
#define	gpba02_get_amp_en()			(gpba02_read(GPBA02_DATB) & GPBA02_AMP_EN_BIT)
#define	gpba02_set_amp_en()			gpba02_write(GPBA02_BUFB_OR, GPBA02_AMP_EN_BIT)
#define	gpba02_clr_amp_en()			gpba02_write(GPBA02_BUFB_AND, ~GPBA02_AMP_EN_BIT)
#define	gpba02_inv_amp_en()			gpba02_write(GPBA02_BUFB_XOR, GPBA02_AMP_EN_BIT)
#define	gpba02_get_lcd_en()			(gpba02_read(GPBA02_DATB) & GPBA02_LCD_EN_BIT)
#define	gpba02_set_lcd_en()			gpba02_write(GPBA02_BUFB_OR, GPBA02_LCD_EN_BIT)
#define	gpba02_clr_lcd_en()			gpba02_write(GPBA02_BUFB_AND, ~GPBA02_LCD_EN_BIT)
#define	gpba02_inv_lcd_en()			gpba02_write(GPBA02_BUFB_XOR, GPBA02_LCD_EN_BIT)

//----------------------------------------------
//	PC
//----------------------------------------------
#define	GPBA02_NAND_WP_BIT			0x01
#define	GPBA02_CHARGE_DET_EN_BIT	0x02
#define	GPBA02_DC_DET_BIT			0x04
#define	GPBA02_WIFI_POWER_BIT		0x08
#define	GPBA02_HOME_BTN_BIT			0x20
#define	GPBA02_CAPTURE_BTN_BIT		0x80

#define	gpba02_get_nand_wp()		(gpba02_read(GPBA02_DATC) & GPBA02_NAND_WP_BIT)
#define	gpba02_set_nand_wp()		gpba02_write(GPBA02_BUFC_OR, GPBA02_NAND_WP_BIT)
#define	gpba02_clr_nand_wp()		gpba02_write(GPBA02_BUFC_AND, ~GPBA02_NAND_WP_BIT)
#define	gpba02_inv_nand_wp()		gpba02_write(GPBA02_BUFC_XOR, GPBA02_NAND_WP_BIT)
#define	gpba02_get_charge_det_en()	(gpba02_read(GPBA02_DATC) & GPBA02_CHARGE_DET_EN_BIT)
#define	gpba02_set_charge_det_en()	gpba02_write(GPBA02_BUFC_OR, GPBA02_CHARGE_DET_EN_BIT)
#define	gpba02_clr_charge_det_en()	gpba02_write(GPBA02_BUFC_AND, ~GPBA02_CHARGE_DET_EN_BIT)
#define	gpba02_inv_charge_det_en()	gpba02_write(GPBA02_BUFC_XOR, GPBA02_CHARGE_DET_EN_BIT)
#define	gpba02_get_wifi_power()		(gpba02_read(GPBA02_DATC) & GPBA02_WIFI_POWER_BIT)
#define	gpba02_set_wifi_power()		gpba02_write(GPBA02_BUFC_OR, GPBA02_WIFI_POWER_BIT)
#define	gpba02_clr_wifi_power()		gpba02_write(GPBA02_BUFC_AND, ~GPBA02_WIFI_POWER_BIT)
#define	gpba02_inv_wifi_power()		gpba02_write(GPBA02_BUFC_XOR, GPBA02_WIFI_POWER_BIT)
#define	gpba02_get_home_btn()		(gpba02_read(GPBA02_DATC) & GPBA02_HOME_BTN_BIT)
#define	gpba02_get_capture_btn()	(gpba02_read(GPBA02_DATC) & GPBA02_CAPTURE_BTN_BIT)

//======================================================================
//======================================================================
extern void gpba02_init();
extern INT8U gpba02_read(INT8U addr);
extern void gpba02_write(INT8U addr, INT8U data);

extern INT8U gpba02_get_charge_en();
extern void gpba02_set_charge_en();
extern void gpba02_clr_charge_en();

#endif
