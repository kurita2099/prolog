#ifndef	__SDC_HEADER__
#define	__SDC_HEADER__
//======================================================================
//======================================================================
#define	D_SDC_UNKNOWN		0
#define	D_SDC_DETECT		1
#define	D_SDC_NOT_DETECT	2

#define	D_SDC_WP_DISABLE	1
#define	D_SDC_WP_ENABLE		2
//======================================================================
//======================================================================
extern INT8U R_SdcFlag;
extern INT16U R_SdcState;

//======================================================================
//======================================================================
extern void sdc_init();
extern int sdc_mount();
extern int sdc_unmount();

extern void F_SdcCheckInit();
extern void F_SdcCheckUninit();
extern void F_SdcCheck();
extern INT8U F_SdcGetDetect();
extern INT8U F_SdcGetDetectD();
extern INT8U F_SdcGetWp();
extern INT8U F_SdcGetWpD();

extern int nand_mount();
extern int nand_unmount();

#endif
