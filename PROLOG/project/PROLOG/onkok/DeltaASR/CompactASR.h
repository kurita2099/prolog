/* ---------------------------------------------------------------------------
 * Cpmpany : Delta Electronics Inc.
 * Author :  Delta Voice Division
 * Date: 	 2013/05/16
 * Purpose:  Delta Speech Recognition Engine v1.0.1 
 -----------------------------------------------------------------------------*/

extern void GP_ASR_Init(void);
extern void GP_ASR_SendModelData(INT16	*p16ModelData);
extern void GP_ASR_Start(void);
extern void GP_ASR_SetVADParm(UINT16 u16VADvalue);
extern void GP_ASR_SetPauseNum(UINT16 u16SpeechFrame, UINT16 u16LongPauseNum, UINT16 u16ShortPauseNum);
extern void	GP_ASR_SendRunTimeBuf(INT16 *p16RunTimeBuf1, INT16	*p16RunTimeBuf2);
extern BOOL GP_ASR_IfinDuration(UINT32 u32Min, UINT32	u32Max, UINT32 *p32VoiceFrame);
extern INT32 GP_ASR_GetCmdScore(INT32	i32Idx);
extern BOOL GP_ASR_CheckState(INT16 s16CmdIdx, INT16	s16Num);
extern UINT8 GP_ASR_isDone(INT16 *i16ASRResult);
extern BOOL GP_ASR_IsCMReject(INT32 s32CMthreshold, INT32* s32CMScore);
extern void GP_ASR_GetLibVersion(int* pnID, int* pnNum);
extern VOID GP_ASR_SetMaxRecordingTime(UINT16 u16MaxSec);
extern void GP_ASR_SendAudioADCBuf(INT16S*	pi16ADCBuf,INT8U u8ADCBufSize);

// Get ASR Top5 Score
typedef struct {
   INT32   i32Score;  // Score 
   INT32   i32Idx;    // recognized command index to indicate which voice command is recorded.
}S_ASR_SCORE_TOPN;

typedef struct {                            
   S_ASR_SCORE_TOPN sASRTop5[5];//report top five score.
} S_ASR_SCORE, *PS_ASR_SCORE;

