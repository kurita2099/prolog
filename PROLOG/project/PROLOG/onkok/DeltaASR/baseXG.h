#ifndef __HEADER_BASEFIXEDG_H__
#define __HEADER_BASEFIXEDG_H__

/* general definition */
#define TRUE				1
#define FALSE				0
#define MAX_PATH			260
#define MAX_LINE			1024
#define SSE_PACK_N			4

#define MAX_VAL				(long) 1e9
#define MIN_VAL				(long) -1e9
#define ZERO_P				(long) 1e-9
#define ZERO_N				(long) -1e-9
#define ZERO_LOG			(long) -1e9
#define MIN_VAL_LOG			(long) -300			/* means exp(-300) */
#define LOG2PI				(long) 100

#define	SCALE_VALUE			262144		

typedef void				VOID;
typedef int					BOOL;
typedef int					intBOOL;
typedef char*				LPTSTR;
typedef char				TCHAR;
typedef void*				LPVOID;
typedef unsigned long		DWORD;
typedef unsigned char		byte;
typedef unsigned short int	WORD;
typedef unsigned int 		uint32_t;

typedef char				INT8;
typedef char *				PINT8;
typedef unsigned char		UINT8;
typedef unsigned char *		PUINT8;

typedef short				INT16;
typedef short *				PINT16;
typedef unsigned short		UINT16;
typedef unsigned short *	PUINT16;

typedef int					INT32;
typedef int *				PINT32;
typedef unsigned int		UINT32;
typedef unsigned int *		PUINT32;

typedef  unsigned char		U8;
typedef  signed char		S8;
typedef  unsigned short		U16;
typedef  signed short		S16;
typedef  unsigned long		U32;
typedef  signed long		S32;

#endif /* __HEADER_BASEFIXEDG_H__ */






