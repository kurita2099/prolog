#ifndef	__TOUCHPAD_HEADER__
#define	__TOUCHPAD_HEADER__
//======================================================================
//======================================================================
#define	D_TP_CF_CHK			0x8000
#define	D_TP_CF_VALID		0x4000
#define	D_TP_CF_LONG_NG		0x0020
#define	D_TP_CF_LONG		0x0010
#define	D_TP_CF_TAP_NG		0x0008
#define	D_TP_CF_TAP			0x0004
#define	D_TP_CF_TRIG_OFF	0x0002
#define	D_TP_CF_TRIG_ON		0x0001

#define	D_TP_CALC_X1		16
#define	D_TP_CALC_X2		256
#define	D_TP_CALC_X_MAX		D_JPAD_LCM_HEIGHT
#define	D_TP_CALC_Y1		16
#define	D_TP_CALC_Y2		464
#define	D_TP_CALC_Y_MAX		D_JPAD_LCM_WIDTH

#define	D_TP_NONE	0x00
#define	D_TP_SINGLE	0x01
#define	D_TP_MULTI	0x02

#define	D_TP_HIS_MAX	4
#define	D_TP_ZOOM_NONE	0
#define	D_TP_ZOOM_IN	1
#define	D_TP_ZOOM_OUT	2
#define	D_TP_PINCH_IN	D_TP_ZOOM_OUT
#define	D_TP_PINCH_OUT	D_TP_ZOOM_IN

//#define	C_TPCAL_PATH	"A:\\tpcal.bin"

//======================================================================
//======================================================================
//----- TPキャリブレーション用 -----
typedef struct tagTpCalc {
	INT16U	sx;
	INT16U	sy;
	INT16U	ex;
	INT16U	ey;
	INT16U	xlim;
	INT16U	ylim;
} TPBASE, *PTPBASE;

//----- TPから取得できるＡＤ値 -----
typedef struct tagTpAdc {
	INT16U	x;
	INT16U	y;
	INT16U	z1;
	INT16U	z2;
	INT16U	x2;
	INT16U	y2;
	INT32U	rtouch;
} TPADC, *PTPADC;

//----- TP制御 -----
typedef struct tagTpCtrl {
	INT16U	flag;
	INT16U	old_io;
	INT16U	low_cnt;
	INT16U	high_cnt;
	TPADC	now;
	TPADC	st;
	TPADC	ed;
} TPCTRL, *PTPCTRL;

//----- TP範囲 -----
typedef struct tagTpRange {
	INT16S	sx;
	INT16S	sy;
	INT16S	ex;
	INT16S	ey;
} TPRANGE, *PTPRANGE;

//----- TP履歴 -----
typedef struct tagTpHis {
	INT8U	cnt;
	INT32U	rt_max;
	INT32U	rt_min;
	INT32U	addr;
} TPHIS, *PTPHIS;

//----- TP汎用 -----
typedef struct tagTpCommon {
	INT16U	flag;
	INT16U	dbt;
	INT16S	old_x;
	INT16S	old_y;
	INT16S	sld_x;
	INT16S	sld_y;
} TPCOMMON, *PTPCOMMON;

//======================================================================
//======================================================================
extern INT8U R_TpIrqFlag;
extern TPCTRL R_TpCtrl;
extern TPBASE R_TpBase;
extern TPHIS R_TpHis;

//======================================================================
//======================================================================
extern	void F_TpInit();
extern	void F_TpTest();
extern	void F_TpUpdate();
extern	void F_TpGetNowAd(PTPADC tp);
extern	void F_TpGetStAd(PTPADC tp);
extern	void F_TpGetEdAd(PTPADC tp);
extern	void F_TpSaveCalcBase(PTPBASE pBase);
extern	INT8U F_TpLoadCalcBase(PTPBASE pBase);
extern	void F_TpDelCalcBase();
extern	INT8U F_TpIsCalcBase();
extern	void F_TpSetCalcBase(PTPBASE pBase);
extern	void F_TpGetCalcBase(PTPBASE pBase);
extern	void F_TpCalcXY(PTPADC tp, INT16S *px, INT16S *py);
extern	INT32U F_TpGetRtouch(PTPADC tp);
extern	INT8U F_TpCheckSingleMulti(PTPADC tp);
extern	INT8U F_TpGetXY(INT16S *px, INT16S *py);
extern	INT8U F_TpGetValid();
extern	INT8U F_TpGetTrigOnXY(INT16S *px, INT16S *py);
extern	INT8U F_TpGetTrigOn();
extern	INT8U F_TpGetTrigOffXY(INT16S *px, INT16S *py);
extern	INT8U F_TpGetTrigOff();
extern	INT8U F_TpGetTapXY(INT16S *px, INT16S *py);
extern	INT8U F_TpGetTap();
extern	INT8U F_TpGetLongXY(INT16S *px, INT16S *py);
extern	INT8U F_TpGetLong();
extern	INT8U F_TpGetStNowXY(INT16S *px, INT16S *py);
extern	INT8U F_TpGetStEdXY(INT16S *px, INT16S *py);
extern	INT8U F_TpCheckRange(INT16S px, INT16S py, INT16S sx, INT16S sy, INT16S ex, INT16S ey);
extern	INT16U F_TpCheckRangeEx(INT16S px, INT16S py, PTPRANGE ra, INT16U cnt);
extern	void F_TpZoomCheckInit();
extern	void F_TpZoomCheckUninit();
extern	void F_TpZoomHisClear();
extern	INT8U F_TpZoomCheck();

#endif
