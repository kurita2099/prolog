#ifndef __I2C_H__
#define __I2C_H__

#include "drv_l1_gpio.h"

void i2c_init(void);
void i2c_start(void);
void i2c_stop(void);
INT8U i2c_read(INT8U ack);
INT8U i2c_write(INT8U data);

#endif
