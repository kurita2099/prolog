#ifndef	__ZT2092_H__
#define	__ZT2092_H__
#include "drv_l1_gpio.h"
#include "i2c.h"

//define	rtouch ic register 
#define	SYS_RESET_REG		(0X00)
#define	STEUP_CMD_REG		(0X01)
#define	SEQUE_CMD_REG		(0X02)
#define	CTRL_CMD_REG		(0X03)
#define	STATUS_REG			(0X10)
#define	SEQUE_DATA1H_REG	(0X11)
#define	SEQUE_DATA1L_REG	(0X12)	
#define	SEQUE_DATA2H_REG	(0X13)
#define	SEQUE_DATA2L_REG	(0X14)	
#define	SEQUE_DATA3H_REG	(0X15)
#define	SEQUE_DATA3L_REG	(0X16)	
#define	SEQUE_DATA4H_REG	(0X17)
#define	SEQUE_DATA4L_REG	(0X18)	
#define	SEQUE_DATA5H_REG	(0X19)
#define	SEQUE_DATA5L_REG	(0X1A)	
#define	SEQUE_DATA6H_REG	(0X1B)
#define	SEQUE_DATA6L_REG	(0X1C)	

//sequence mode
#define	SEQUE_MODE_0		(0x00)	//X->Y->Z1->Z2(default scan mode)
#define	SEQUE_MODE_1		(0x10)	//X->Y
#define	SEQUE_MODE_2		(0x20)	//X
#define	SEQUE_MODE_3		(0x30)	//Y
#define	SEQUE_MODE_4		(0x40)	//Z1->Z2
#define	SEQUE_MODE_5		(0x50)	//X2
#define	SEQUE_MODE_6		(0x60)	//X->Y->Z1->Z2->X2->Y2
#define	SEQUE_MODE_7		(0x70)	//Y2
//A/D conversion count
#define	CONVER_COUNT_6		(0x00)
#define	CONVER_COUNT_10		(0x80)
//Sampling interval times
#define	INTERVAL_TIME_0		(0x00)	//0us
#define	INTERVAL_TIME_5		(0x01)	//5us
#define	INTERVAL_TIME_10		(0x02)	//10us
#define	INTERVAL_TIME_20		(0x03)	//20us
#define	INTERVAL_TIME_50		(0x04)	//50us
#define	INTERVAL_TIME_100	(0x05)	//100us
#define	INTERVAL_TIME_200	(0x06)	//200us
#define	INTERVAL_TIME_500	(0x07)	//500us


BOOLEAN zt2092_write_byte(const INT8U addr,const INT8U wda);
BOOLEAN zt2092_read_byte(const INT8U add,INT8U *rda);
BOOLEAN zt2092_read_data(const INT8U add,INT8U *rda,const INT32U nbyte);
BOOLEAN zt2092_power_up_sequ(void);
BOOLEAN zt2092_system_reset(void);

INT16U noise_filter(INT16U *da_buf,const INT16U n);
BOOLEAN zt2092_read_xy(INT16U *x,INT16U *y);
BOOLEAN zt2092_read_all(INT16U *x,INT16U *y,INT16U *z1,INT16U *z2,INT16U *x2,INT16U *y2);
BOOLEAN zt2092_read_xy_filter(INT16U *x,INT16U *y);
BOOLEAN zt2092_read_x2_y2(INT8U type,INT16U *x2_y2);
BOOLEAN get_limits(INT16U *xlimit,INT16U *ylimit);








#endif	/*__ZT2092_H__*/