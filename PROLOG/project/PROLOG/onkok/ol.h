#ifndef	__OL_HEADER__
#define	__OL_HEADER__
//======================================================================
//======================================================================

//======================================================================
//======================================================================
#define	D_JPAD_JPEG_WIDTH	640
#define	D_JPAD_JPEG_HEIGHT	480

#define	D_OL_TEXT_POS_X_HOSEI	(512-96)
#define	D_OL_TEXT_POS_Y_HOSEI	(512-16)

//------------------------------
enum tagEnumOlResult {
	D_OL_SAVE_OK,
	D_OL_SAVE_ERR,
	D_OL_SAVE_MALLOC_ERR,
	D_OL_SAVE_ENCODE_ERR,
	D_OL_SAVE_CAPACITY_ERR,
	D_OL_SAVE_FILEOPEN_ERR,
};

//======================================================================
//======================================================================
typedef struct tagFrameInfo {
	INT16U	*cell;
	INT16U	*pal;
	INT32U	info;
} FRAMEINFO, *PFRAMEINFO;

//======================================================================
//======================================================================
extern void ol_init();
extern void ol_uninit();
extern void ol_capture();
extern INT32U get_ol_save_addr();
extern void set_ol_save_addr(INT32U addr);
extern INT8U ol_save_camera(char *path, INT32U quality, PFRAMEINFO pFrame);
extern INT8U ol_save_clip(char *path, INT32U quality, INT16U sx, INT16U sy, INT16U clip_w, INT16U clip_h, INT16U save_w, INT16U save_h);
extern INT8U ol_save_deco(char *path, INT32U quality, INT16U sx, INT16U sy, INT16U clip_w, INT16U clip_h, INT16U save_w, INT16U save_h);
extern INT32U ol_jpeg_encode(INT32U src, INT32U dst, INT16U width, INT16U height, INT32U quality);
extern INT8U ol_qrcode_decode(INT8U *buff, INT32U size);

#endif
