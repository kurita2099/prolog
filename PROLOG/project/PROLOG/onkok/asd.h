#ifndef	__ASD_HEADER__
#define	__ASD_HEADER__

//======================================================================
//======================================================================
//----- 再生種類 -----
#define	D_ASD_MIDI		MIDI
#define	D_ASD_WAV		WAV
#define	D_ASD_JMP		WAV
#define	D_ASD_A18		A1800

//----- 状態 -----
#define	D_ASD_STOP		AUDIO_CODEC_PROCESS_END
#define	D_ASD_PLAY		AUDIO_CODEC_PROCESSING
#define	D_ASD_PAUSE		AUDIO_CODEC_PROCESS_PAUSED
#define	D_ASD_BREAK_OFF	AUDIO_CODEC_BREAK_OFF

//----- volume -----
#define	D_ASD_VOL_MIN		0
#define	D_ASD_VOL_MAX		15

//----- speed / pitch -----
#define	D_ASD_SPEED_DEFAULT	12
#define	D_ASD_SPEED_MAX		24
#define	D_ASD_PITCH_DEFAULT	12
#define	D_ASD_PITCH_MAX		24

//----- resule -----
#define	D_ASD_PLAY_OK			0
#define	D_ASD_PLAY_OPEN_ERR		1
#define	D_ASD_PLAY_MALLOC_ERR	2
#define	D_ASD_PLAY_READ_ERR		3
#define	D_ASD_PLAY_ERR			0xFF

//----- sound flag -----
#define	D_ASD_ING_AUDIO		0x0001
#define	D_ASD_ING_VIDEO		0x0002
#define	D_ASD_ING_RECORD	0x0004
#define	D_ASD_AUDIO_MIDI	0x4000
#define	D_ASD_AUDIO_SD		0x8000

//----- ch -----
#define	D_ASD_BGM_CH	1
#define	D_ASD_SE_CH		2
#define	D_ASD_CH_MAX	32

//----- voice changer -----
#define RPRAM_SAMPLE_RATE		(22050)
#define RPRAM_SAMPLE_BITS		(16)
#define RPRAM_SAMPLE_TIME		(10)		/* unit = second */
#define	RPRAM_DATA_LEN			(RPRAM_SAMPLE_RATE * RPRAM_SAMPLE_BITS / 8 * (RPRAM_SAMPLE_TIME + 5))
#define	RPRAM_DATA_LEN_LIMIT	(RPRAM_SAMPLE_RATE * RPRAM_SAMPLE_BITS / 8 * RPRAM_SAMPLE_TIME)

#define	D_REC_STATUS_ING		AUDIO_CODEC_PROCESSING
#define	D_REC_STATUS_STOP		AUDIO_CODEC_PROCESS_END
#define	D_REC_STATUS_PAUSE		AUDIO_CODEC_PROCESS_PAUSE

#define	D_VC_STOP_START_BIT		0x01
#define	D_VC_STOP_END_BIT		0x02

//----- line-in -----
#define	D_REC_KIND_NAND		FS_NAND1
#define	D_REC_KIND_SD		FS_SD

#define	D_REC_START_OK			0
#define	D_REC_START_KIND_ERR	1
#define	D_REC_START_OPEN_ERR	2
#define	D_REC_START_ERR			0xFF

#define	D_LINEIN_SAMPLE_RATE	(11025)
#define	D_LINEIN_SAMPLE_BITS	(8)
#define	D_LINEIN_SAMPLE_TIME	(600)

//#define	C_STR_REC_FOLDER_NAND		"A:\\JMPREC"
//#define	C_STR_REC_FOLDER_SD			"C:\\JMPREC"
//#define	C_STR_REC_TEMP_PATH_NAND	"A:\\JMPREC\\temp.jmp"
//#define	C_STR_REC_TEMP_PATH_SD		"C:\\JMPREC\\temp.jmp"

//----------------------------------------------
#include	"asd_define.h"

//======================================================================
//======================================================================
extern INT8U R_AsdVolume;
extern INT8U R_AsdEarphone;
extern INT8U R_AsdMicInputEnable;
extern INT16U R_AsdBgmFlag;
extern INT32U R_AsdDrmFlag;
extern INT32U R_AsdBufAddr[D_ASD_CH_MAX];
extern INT32U R_AsdMicInputBuff;
extern INT32U R_AsdMicLocalThr;
extern INT32U R_AsdMicGlobalThr;

extern INT8U R_RecFatKind;
extern INT8U R_VCStopFlag;

//======================================================================
//======================================================================
extern void F_AsdInit();
extern void F_AsdUninit();
extern void F_AsdPowerOnOff(INT8U on_off);
extern INT8U F_AsdGetPowerState();
extern INT8U F_AsdEarphoneCheck();
extern void F_AsdBufFree(INT8U ch);
extern INT32U F_AsdGetAudioArgument();
extern INT32U F_AsdGetMediaSource();
extern INT8U F_AsdGetVolume();
extern void F_AsdCheck();
extern INT8U F_AsdGetSoundIng();

extern INT8U F_CheckDrm(INT8U ch);
extern INT8U F_CheckDrmAll();
extern void F_StopDrm(INT8U ch);
extern void F_StopDrmAll();
extern INT8U F_PlayDrmDirect(INT8U ch, INT32U addr, INT8U mem_set);
extern INT8U F_PlayDrm(INT8U ch, INT16U no);

extern int F_GetBgmStatus();
extern INT8U F_CheckBgm();
extern void F_StopBgm();
extern INT8U F_PlayBgmFAT(const char *path, INT8U kind, INT16U index, INT32U offset);
extern INT8U F_PlayBgmRES(INT16U no, INT8U kind, INT32U offset);
extern INT8U F_PlayBgmMEM(INT32U addr, INT32U len, INT16U speed, INT16U pitch);
extern void F_PauseBgm();
extern void F_ResumeBgm();
extern void F_SpeedPitchSet(INT16U speed, INT16U pitch);
extern void F_VolumeSet(INT8U vol);
extern void F_VolumeUp();
extern void F_VolumeDown();

extern void F_VCRecordInit();
extern void F_VCRecordUninit();
extern INT8U F_VCRecordStart();
extern int F_GetRecordStatus();
extern void F_VCRecordStop();
extern void F_VCPlay(INT16U speed, INT16U pitch);
extern void F_VCPlayStop();
extern INT8S* F_VCGetRecordAddr();
extern INT32U F_VCGetRecordLen();

extern void F_LineInRecordInit();
extern void F_LineInRecordUninit();
extern INT8U F_LineInRecordStart(INT8U kind);
extern void F_LineInRecordStop();
extern INT8U F_LineInDummyRecordStart();
extern void F_LineInDummyRecordStop();

extern void F_AsdMicInputInit();
extern void F_AsdMicInputUninit();
extern INT8U F_AsdMicInputStart(INT32U local, INT32U global);
extern void F_AsdMicInputStop();

#endif
