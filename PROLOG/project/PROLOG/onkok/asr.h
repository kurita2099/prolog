#ifndef	__ASR_HEADER__
#define	__ASR_HEADER__
//======================================================================
//======================================================================
enum tagAsrLib {
	D_ASR_LIB1 = 1,
	D_ASR_LIB2,
	D_ASR_LIB3,
	D_ASR_LIB4,
	D_ASR_LIB5,
//	D_ASR_LIB6,
//	D_ASR_LIB7,
//	D_ASR_LIB8,
	D_ASR_LIB_MAX
};
//------------------------------
#define	D_ASR_LIB_NO_MAX	(60)
#define	D_ASR_CMD_SCR_MIN	(-1000)
#define	D_ASR_RES_MIN		(1)
#define	D_ASR_RES_MAX		(100)
#define	D_ASR_CHK_ST		(6)
#define	D_ASR_CM_REJECT		(-500)
#define	D_ASR_DUR_MIN		(10)
#define	D_ASR_DUR_MAX		(200)

#define	D_ASR_ERR			0xFFFF
//------------------------------
#define	D_ASR_CTRL_CHK	0x01
#define	D_ASR_CTRL_RES	0x02

//======================================================================
//======================================================================
extern const short g_a16ModelData_GP1[];
extern const short g_a16ModelData_GP2[];
extern const short g_a16ModelData_GP3[];
extern const short g_a16ModelData_GP4[];
extern const short g_a16ModelData_GP5[];
//extern const short g_a16ModelData_GP6[];
//extern const short g_a16ModelData_GP7[];
//extern const short g_a16ModelData_GP8[];

//======================================================================
//======================================================================
extern void F_GpAsrInit();
extern void F_GpAsrUninit();
extern void F_GpAsrSendModelData(short *model);
extern void F_AsrSetModelData(INT8U no);
extern void F_GpAsrStart();
extern void F_GpAsrSetVADParm(short value);
extern void F_GpAsrSetPauseNum(short sf, short lpn, short spn);
extern void F_GpAsrSendRunTimeBuf(short *buf1, short *buf2);
extern void F_AsrSetRunTimeBuf();
extern int F_GpAsrIfinDuration(unsigned int min, unsigned int max, unsigned int *vf);
extern int F_GpAsrGetCmdScore(int index);
extern int F_GpAsrCheckState(short index, short num);
extern unsigned char F_GpAsrIsDone(short *result);
extern int F_GpAsrIsCMReject(int threshold, int *score);
extern void F_GpAsrGetLibVersion(int *id, int *ver);
extern void F_GpAsrSetMaxRecordingTime(short max);
extern void F_GpAsrSendAudioADCBuf(short *buff, unsigned short size);

extern void F_AsrInit(INT8U no);
extern void F_AsrUninit();
extern void F_AsrLibChange(INT8U no);
extern void F_AsrStart();
extern void F_AsrStop();
extern void F_AsrCheck();
extern INT16U F_AsrCheckResult();
extern INT8U F_AsrGetCtrlFlag();
extern INT8U F_AsrGetLibNo();

#endif
