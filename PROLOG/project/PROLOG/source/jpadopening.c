//======================================================================
//	インクルード
//======================================================================
#include	"gplib.h"
#include	"jpad_header.h"
#include 	"jpad_celldata_attach.h"
///#include	"jpad_AP29_Image.h"

///#include	"SP_DEMO\\SPRITE_0503chara_HDR.h"
///#include	"SP_DEMO\\SP_AP29_HDR.h"
//#include	"SP_DEMO_HOME\\SPRITE_0525home_HDR.h"
#include	"TX_DEMOOPN/TEXT_0603dooropen_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen1_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen2_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen3_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen4_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen5_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen6_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen7_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen8_HDR.h"
#include	"TX_DEMOOPN/TEXT_0630dooropen9_HDR.h"
#include	"TX_DEMOOPN/TEXT_user_HDR.h"
#include	"SP_DEMOOPN/SPRITE_user_HDR.h"

//#include	"TX_DEMO_HOME\\TX_AP29_HDR.h"
//#include	"Text_demo.h"
//#include	"Sprite_demo.h"
//#include	"jpdp_common.h"
#include 	<stdio.h>
#include	<stdlib.h>
//#include 	<math.h>


///#include	"SP_DEMO\\sp_demo.dat"
///#include	"TX_DEMO\\text_demo.dat"
///#include	"SP_DEMO\\SP_AP29.dat"
#include	"TX_DEMOOPN/text_door.dat"
#include	"TX_DEMOOPN/text_dooropen1.dat"
#include	"TX_DEMOOPN/text_dooropen2.dat"
#include	"TX_DEMOOPN/text_dooropen3.dat"
#include	"TX_DEMOOPN/text_dooropen4.dat"
#include	"TX_DEMOOPN/text_dooropen5.dat"
#include	"TX_DEMOOPN/text_dooropen6.dat"
#include	"TX_DEMOOPN/text_dooropen7.dat"
#include	"TX_DEMOOPN/text_dooropen8.dat"
#include	"TX_DEMOOPN/text_dooropen9.dat"
#include	"TX_DEMOOPN/text_user.dat"
#include	"SP_DEMOOPN/sp_user.dat"
//#include	"TX_DEMO\\TX_AP29.dat"

#include	"jpad_common_S.h"
#include	"text.h"
#include	"asd.h"
#define  STAGENO   99;
#define  CHARACTER_OFST   1900
#define		CHARADATA_ARRAY(_ofst)	*(((INT8U*)&mp_R_UserData->key[CHARACTER_OFST])+ _ofst)


//ワーク用の構造体を宣言
typedef struct {
	INT8U	id;
	INT8U	result;
} DEMOWORK,*PDEMOWORK;


#define		pHoshi		((INT32S*)m_R_WorkAddr)
#define	mcrFadeIn(next)		mf_F_SetFadeInOka(D_FADE_DEFAULT_FRAME, D_FADE_DEFAULT_VALUE, next, D_DEMO_SEQ_FADE_IN)
#define	mcrFadeOut(next)	mf_F_SetFadeOutOka(D_FADE_DEFAULT_FRAME, D_FADE_DEFAULT_VALUE, next, D_DEMO_SEQ_FADE_OUT)
//void Init_DEMO_Start(void);
//void Seq_DEMO_Start00(void);
//======================================================================
//	define定義
//======================================================================
#define		D_DEMO_SEQ_START		(0)
#define		D_DEMO_SEQ_FADE_IN		(10)
#define		D_DEMO_SEQ_FADE_OUT		(20)
#define		D_DEMO_SEQ_MAIN			(30)
#define		D_DEMO_SEQ_TO_HOME		(100)


void	F_DEMOOPEN_Main(void);

void	Seq_DEMOOPEN_Fade_In(void);
void	Seq_DEMOOPEN_Fade_Out(void);
void	Disp_DEMOOPEN_Sp( INT16U, INT16U, INT16S, INT16S );
void    Seq_DEMOOPEN_Main00(void);
void    Seq_DEMOOPEN_Main01(void);
void    Seq_DEMOOPEN_Main02(void);

void    Seq_DEMOOPEN_Start00(void);

extern INT32U	R_AppSystem;
extern INT32U	R_AppWorkAddr;
extern INT16U	R_SubProgState;
//======================================================================
// スプライトデータ
//======================================================================
const COMMON_S_SPINFO T_DEMOOPN_SPRITE[] = {
{(INT32U)fs02001_SP, 0 },
{(INT32U)fs02001_SP, 1 },
{(INT32U)fs02001_SP, 2 },
{(INT32U)fs02001_SP, 3 },
{(INT32U)fs02001_SP, 4 },
{(INT32U)fs02001_SP, 5 },
{(INT32U)fs02001_SP, 6 },
{(INT32U)fs02001_SP, 7 },
{(INT32U)fs02001_SP, 8 },
{(INT32U)fs02001_SP, 9 },
{(INT32U)fs02001_SP, 10 },//10
{(INT32U)fs02001_SP, 11 },
{(INT32U)fs02001_SP, 12 },
{(INT32U)fs02001_SP, 13 },
{(INT32U)fs02001_SP, 14 },
{(INT32U)fs02001_SP, 15 },
{(INT32U)fs02004_SP, 0 },
{(INT32U)fs02004_SP, 1 },
{(INT32U)fs02005_SP, 0 },
{(INT32U)fs02007_SP, 0 },
{(INT32U)fs02007_SP, 1 },//20
{(INT32U)fs02007_SP, 2 },
{(INT32U)fs02007_SP, 3 },
{(INT32U)fl02001_SP, 0 },
{(INT32U)us_charaA_SP, 0 },
{(INT32U)us_charaA_SP, 1 },
{(INT32U)us_charaA_SP, 2 },
{(INT32U)us_charaA_SP, 3 },
{(INT32U)us_charaA_SP, 4 },
{(INT32U)us_charaA_SP, 5 },
{(INT32U)us_charaA_SP, 6 },//30
{(INT32U)us_charaA_SP, 7 },
{(INT32U)us_charaA_SP, 8 },
{(INT32U)us_charaA_SP, 9 },
{(INT32U)us_charaB_SP, 0 },
{(INT32U)us_charaB_SP, 1 },
{(INT32U)us_charaB_SP, 2 },
{(INT32U)us_charaB_SP, 3 },
{(INT32U)us_charaB_SP, 4 },
{(INT32U)us_charaB_SP, 5 },
{(INT32U)us_charaB_SP, 6 },//40
{(INT32U)us_charaB_SP, 7 },
{(INT32U)us_charaB_SP, 8 },
{(INT32U)us_charaB_SP, 9 },
{(INT32U)us_charaD_SP, 0 },
{(INT32U)us_charaD_SP, 1 },
{(INT32U)us_charaD_SP, 2 },
{(INT32U)us_charaD_SP, 3 },
{(INT32U)us_charaD_SP, 4 },
{(INT32U)us_charaD_SP, 5 },
{(INT32U)us_charaD_SP, 6 },//50
{(INT32U)us_charaD_SP, 7 },
{(INT32U)us_charaD_SP, 8 },
{(INT32U)us_charaD_SP, 9 },
{(INT32U)cursor00_SP,0}
};

//======================================================================
//	フェードイン
//======================================================================
void Seq_DEMOOPEN_Fade_In(void)
{
	if( mf_F_GetFadeFlag() ) return;

	switch( m_R_NextSeq )
	{
		default:
			m_R_SeqNo = m_R_NextSeq;
			break;
	}
}
//======================================================================
//	[描画] スプライト
//======================================================================
void Disp_DEMOOPN_Sp( INT16U sp_no, INT16U sp_id, INT16S x, INT16S y )
{
	mf_F_SpriteUpdate1( sp_no, T_DEMOOPN_SPRITE[sp_id].addr, T_DEMOOPN_SPRITE[sp_id].ofs, x, y );
	mf_F_PPU_DispFlagOn();
}
//======================================================================
//	フェードアウト
//======================================================================
void Seq_DEMOOPEN_Fade_Out(void)
{
	if( mf_F_GetFadeFlag() ) return;

	switch( m_R_NextSeq )
	{
	default:
		m_R_SeqNo = m_R_NextSeq;
		break;
	}
}
//======================================================================
//	[描画] スプライト
//======================================================================
void Disp_DEMOOPEN_Sp( INT16U sp_no, INT16U sp_id, INT16S x, INT16S y )
{
	mf_F_SpriteUpdate1( sp_no, T_DEMOOPN_SPRITE[sp_id].addr, T_DEMOOPN_SPRITE[sp_id].ofs, x, y );
	mf_F_PPU_DispFlagOn();
}
//======================================================================
//	デモメイン

//======================================================================
void F_DEMOOPEN_Main()
{
	m_R_SoundNo = 0xFFFF;

		switch(m_R_SeqNo)
		{
		case	D_DEMO_SEQ_START:
			Seq_DEMOOPEN_Start00();
			break;
		// フェードイン
		case	D_DEMO_SEQ_FADE_IN:
			Seq_DEMOOPEN_Fade_In();
			break;
		// フェードアウト
		case	D_DEMO_SEQ_FADE_OUT:
			Seq_DEMOOPEN_Fade_Out();
			break;
		case	D_DEMO_SEQ_MAIN:
			Seq_DEMOOPEN_Main00();
			break;
		case	(D_DEMO_SEQ_MAIN+1):
			Seq_DEMOOPEN_Main01();
			break;
		case	D_DEMO_SEQ_TO_HOME:
			Seq_DEMOOPEN_Main02();
			//Seq_DEMO_To_Home00();
			break;
		case	(D_DEMO_SEQ_TO_HOME+1):
			//Seq_DEMO_To_Home01();
			break;
		}
		//if(m_R_SoundNo != 0xFFFF) {
		//	mf_F_PlayDrm(D_ASD_SE_CH, m_R_SoundNo);
		//}
	}
	
/*
extern const INT16U _fs02003_IMG0000_CellData[];
extern const INT16U _fs02006_IMG0000_IndexData[];
extern const INT16U _fs02006_IMG0000_CellData[];
extern const INT16U _fs02009_IMG0000_IndexData[];
extern const INT16U _fs02009_IMG0000_CellData[];
extern const INT16U _fs02011_IMG0000_IndexData[];
extern const INT16U _fs02011_IMG0000_CellData[];
extern const INT16U _us_fl1010a_IMG0000_IndexData[];
extern const INT16U _us_fl1010a_IMG0000_CellData[];
extern const INT16U _us_fl1010b_IMG0000_IndexData[];
extern const INT16U _us_fl1010b_IMG0000_CellData[];
extern const INT16U _us_ff01001_IMG0000_IndexData[];
extern const INT16U _us_ff01001_IMG0000_CellData[];
extern const INT16U _us_fl01011a_IMG0000_IndexData[];
extern const INT16U _us_fl01011a_IMG0000_CellData[];
extern const INT16U _us_f01011b_IMG0000_IndexData[];
extern const INT16U _us_f01011b_IMG0000_CellData[];
*/
const TEXTINFO T_TX_USER_Info[] = {
		{ 15,17, TXN_PALETTE0, 0, (INT32U)_fs02003_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE1, 0, (INT32U)_fs02003_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE2, 0, (INT32U)_fs02003_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE3, 0, (INT32U)_fs02003_IMG0000_IndexData},

		{ 15,17, TXN_PALETTE1+3, 0x90, (INT32U)_fs02006_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE2+3, 0x90+0x53,(INT32U)_fs02009_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE3+3, 0x90+0x53+0x59,(INT32U)_fs02011_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE4+3, 0x90+0x53+0x59+0x53,(INT32U)_us_fl1010a_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE5+3, 0x90+0x53+0x59+0x53+0x55,(INT32U)_us_fl1010b_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE6+3, 0x90+0x53+0x59+0x53+0x55+0xb0,(INT32U)_us_ff01001_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE7+3, 0x90+0x53+0x59+0x53+0x55+0xb0+0xcb,(INT32U)_us_fl01011a_IMG0000_IndexData},
		{ 15,17, TXN_PALETTE8+3, 0x90+0x53+0x59+0x53+0x55+0xb0+0xcb+0xe6,(INT32U)_us_f01011b_IMG0000_IndexData}
};
const TEXTINFO T_TX_DEMOOPEN_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
///{ 15,9, TXN_PALETTE0, 0, (INT32U)_Text001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_dooropen_01_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0x52, (INT32U)_dooropen_02_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0x52+0xad, (INT32U)_dooropen_03_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0x52+0xad+0xe6, (INT32U)_dooropen_03_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0x52+0xad+0xe6+0xf2, (INT32U)_dooropen_bg_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN1_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do1_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do1_fl01002a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x52, (INT32U)_do1_fl01002b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x52+0xad, (INT32U)_do1_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x52+0xad+0xe6, (INT32U)_do1_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN2_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do2_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do2_fl01003a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x52, (INT32U)_do2_fl01003b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x52+0xad, (INT32U)_do2_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x52+0xad+0xe6, (INT32U)_do2_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN3_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do3_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do3_fl01004a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x52, (INT32U)_do3_fl01004b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x52+0xac, (INT32U)_do3_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x52+0xac+0xe6, (INT32U)_do3_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN4_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do4_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do4_fl01005a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x52, (INT32U)_do4_fl01005b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x52+0xac, (INT32U)_do4_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x52+0xac+0xe6, (INT32U)_do4_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN5_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do5_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do5_fl01006a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x51, (INT32U)_do5_fl01006b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x51+0xad, (INT32U)_do5_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x51+0xad+0xe6, (INT32U)_do5_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN6_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do6_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do6_fl01007a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x53, (INT32U)_do6_fl01007b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x53+0xab, (INT32U)_do6_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x53+0xab+0xe6, (INT32U)_do6_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN7_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do7_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do7_fl01008a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x52, (INT32U)_do7_fl01008b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x52+0xad, (INT32U)_do7_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x52+0xad+0xe6, (INT32U)_do7_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN8_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do8_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do8_fl01009a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x52, (INT32U)_do8_fl01009b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x52+0xab, (INT32U)_do8_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x52+0xab+0xe6, (INT32U)_do8_fl01011b_IMG0000_IndexData},
};
const TEXTINFO T_TX_DEMOOPEN9_Info[] = {
//x = 15 words, y = 17 words, x cell size = 32, y cell size = 16
	{ 15,17, TXN_PALETTE0, 0, (INT32U)_do9_fl01001_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE1, 0xcb, (INT32U)_do9_fl01010a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE2, 0xcb+0x57, (INT32U)_do9_fl01010b_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE3, 0xcb+0x57+0xaf, (INT32U)_do9_fl01011a_IMG0000_IndexData},
	{ 15,17, TXN_PALETTE4, 0xcb+0x57+0xaf+0xe6, (INT32U)_do9_fl01011b_IMG0000_IndexData},
};

//======================================================================
//	[初期] 遷移初期
//======================================================================
void Init_DEMOOPEN_Start(void)
{
	INT32U addr;
	mf_F_CreateWork( 4096, 4 );
	mf_F_TextDisable(C_PPU_TEXT1);
	mf_F_TextDisable(C_PPU_TEXT2);
	mf_F_TextDisable(C_PPU_TEXT3);
	mf_F_TextDisable(C_PPU_TEXT4);
	mf_F_FontAllClear();

#if 1
	INT32U len1=0x58000,len2=0x22800;
	mf_F_ReleaseUnpack(1);//この領域はダミー
	INT8S* dst;
	dst = mf_F_CellDataUnpack("SP_PROLOG.BIN",2);
	addr = unpacked_channel_add(len1,len2,1,2,3);
#else
	//------------------------------------------------------------------
			INT32U len1=0x58000,len2=0x22800,size;
			INT8S* dst;
			//pWork->sp_addr = mf_F_CellDataUnpack((INT8U *)"SP_AP09.BIN", 1);
			//addr = mf_F_CellDataUnpack1((INT8U *)SP_USER, 2);
			addr = mf_F_CellDataUnpack("SP_PROLOG.BIN",2);
			mf_F_ReleaseUnpack(0);
			size = len1 + len2;
			 dst = (INT8S *)mf_gp_malloc_align(size, 4);
		    if(!dst) {
		     mf_gp_free((void *)dst);
		     return;
		    }
			mp_R_UnpackAddr[0] = (INT32U)dst;
			//gp_memcpy(dst,(INT8S*)pWork->sp_addr,len1);
			mf_gp_memcpy(&dst[len1],(INT8S*)addr,len2);//
			//mf_gp_memcpy(dst,pWork->sp_addr,len1);
		 	//mf_gp_memcpy(&dst[len1],addr,len2);
			//F_ReleaseUnpack(1);
			mf_F_ReleaseUnpack(2);
			addr = (INT32U)dst;
#endif


	mf_F_SpriteInit((INT32U)_SPRITE_user_Palette1, (INT32U)_SPRITE_user_Palette3, (INT32U)addr );
	//F_SpriteInit((INT32U)_SPRITE_0525home_Palette1, (INT32U)_SPRITE_0525home_Palette3, (INT32U)addr );
#if 0
	addr= F_CellDataUnpack1((INT8U *)TX_USER, 1);
	F_TextLayerInit( C_PPU_TEXT2, (INT32U) _TEXT_user_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
	F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_USER_Info[0], D_TEXT_LAYER_TATE);
	F_TextPutChar( C_PPU_TEXT2, 0, 480, (PTEXTINFO)&T_TX_USER_Info[1], D_TEXT_LAYER_TATE);
	F_TextPutChar( C_PPU_TEXT2, -320,0, (PTEXTINFO)&T_TX_USER_Info[2], D_TEXT_LAYER_TATE);
	F_TextPutChar( C_PPU_TEXT2, -320, 480, (PTEXTINFO)&T_TX_USER_Info[3], D_TEXT_LAYER_TATE);

	F_TextEnable(C_PPU_TEXT2);

#else
	int i = mp_R_JpadReserved[63];
	CHARADATA_ARRAY(99)=i;

	switch(i){
		case 0:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOR, 1);//元のやつ
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0603dooropen_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0603dooropen_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN_Info[4], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN_Info[3], D_TEXT_LAYER_YOKO);
		break;
		case 1:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN1, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen1_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen1_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN1_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN1_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN1_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN1_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN1_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 2:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN2, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen2_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen2_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN2_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN2_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN2_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN2_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN2_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 3:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN3, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen3_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen3_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN3_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN3_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN3_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN3_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN3_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 4:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN4, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen4_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen4_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN4_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN4_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN4_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN4_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN4_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 5:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN5, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen5_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen5_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN5_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN5_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN5_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN5_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN5_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 6:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN6, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen6_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen6_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN6_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN6_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN6_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN6_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN6_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 7:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN7, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen7_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen7_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN7_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN7_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN7_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN7_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN7_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 8:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN8, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen8_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen8_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN8_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN8_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN8_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN8_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN8_Info[4], D_TEXT_LAYER_YOKO);
		break;
		case 9:
			addr= mf_F_CellDataUnpack1((INT8U *)TX_DOOROPEN9, 1);
			mf_F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0630dooropen9_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0630dooropen9_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
			mf_F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN9_Info[0], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN9_Info[1], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN9_Info[2], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN9_Info[3], D_TEXT_LAYER_YOKO);
			mf_F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN9_Info[4], D_TEXT_LAYER_YOKO);
		break;
	}
	/*
	addr= F_CellDataUnpack1((INT8U *)TX_DOOR, 1);
	//gplib_ppu_text_size_set(video_ppu_register_set, C_PPU_TEXT1, C_TXN_SIZE_1024X1024);	// Set TEXT size to 1024x512
	///F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0503charabg_Palette0, (INT32U)addr, TXN_HS64, TXN_VS64);
	F_TextLayerInit( C_PPU_TEXT1, (INT32U)_TEXT_0603dooropen_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);
	F_TextLayerInit( C_PPU_TEXT2, (INT32U)_TEXT_0603dooropen_Palette0, (INT32U)addr, TXN_HS32, TXN_VS16);

	F_TextPutChar( C_PPU_TEXT1, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN_Info[4], D_TEXT_LAYER_YOKO);
	F_TextPutChar( C_PPU_TEXT2, 0, 0, (PTEXTINFO)&T_TX_DEMOOPEN_Info[0], D_TEXT_LAYER_YOKO);
	F_TextPutChar( C_PPU_TEXT2, 480, 0, (PTEXTINFO)&T_TX_DEMOOPEN_Info[1], D_TEXT_LAYER_YOKO);
	F_TextPutChar( C_PPU_TEXT2, 0, 320, (PTEXTINFO)&T_TX_DEMOOPEN_Info[2], D_TEXT_LAYER_YOKO);
	F_TextPutChar( C_PPU_TEXT2, 480, 320, (PTEXTINFO)&T_TX_DEMOOPEN_Info[3], D_TEXT_LAYER_YOKO);
	*/

	mf_F_TextEnable(C_PPU_TEXT1);
	mf_F_TextEnable(C_PPU_TEXT2);
#endif
	mf_F_TextEnable(C_PPU_TEXT4);//////////
	

	mf_F_SpriteAllClear();

	// 情報バー初期化
	/*F_InfoBarInit(D_IB_ENABLE, D_IB_DIR_TATE, (INT32U *)cDEMOInfoBarTbl );
	pHoshi[R_DEMO_Ret] = 0;*/
}
//======================================================================
//	[状態] 遷移初期00
//======================================================================
void Seq_DEMOOPEN_Start00(void)
{
	Init_DEMOOPEN_Start();
	m_R_SeqNo = D_DEMO_SEQ_MAIN;
}

#define _y (272/2)
#define _x (480/2)
//======================================================================
//	[初期] 計算画面
//======================================================================
void Init_DEMOOPEN_Main(void)
{
	mf_F_CreateWork(sizeof(DEMOWORK), 4);
	mf_F_MyMemSet(m_R_WorkAddr, 0, sizeof(DEMOWORK), D_MY_MEM_UNIT8);
	/*INT64U *pA, *pB;
	pA = (INT64U*)Get_DEMO_A();
	pB = (INT64U*)Get_DEMO_B();

	pHoshi[R_DEMO_Cur_Number] = 0;
	pHoshi[R_DEMO_Cur_Operator] = 0;
	pHoshi[R_DEMO_Operator] = D_DEMO_OP_NORMAL;

	pHoshi[R_DEMO_BTN_Wait] = 0;
	pHoshi[R_DEMO_BTN_Flag] = FALSE;
	pHoshi[R_DEMO_BTN_Pos] = D_DEMO_BTN_NONE;
	pHoshi[R_DEMO_Point_Flag] = 0;
	pHoshi[R_DEMO_Neg_Flag] = 0;
	pHoshi[R_DEMO_Dec_Count] = 0;
	pHoshi[R_DEMO_In_B_Flg] = 0;

	*pA = 0;
	*pB = 0;

	pHoshi[R_DEMO_In_Flg] = D_DEMO_INPUT_A;

	Reset_DEMO_ICT(D_DEMO_SEQ_MAIN);

	F_TextDisable(C_PPU_TEXT2);
	F_TextDisable(C_PPU_TEXT3);
	F_FontAllClear();
	F_TextEnable(C_PPU_TEXT4);
	F_SpriteAllClear();
*/
	/*Clear_DEMO_Input_Number();
	Disp_DEMO_Result_Value(0.0);
	Disp_DEMO_Operator();
	Disp_DEMO_CtrlBtn();

	int i;
	//int y = 0;
	int sp_no=0;
	int sp_id=0;
	for(i=0;i<3;i++){
		//T_DEMO_SPRITE_POS
	Disp_DEMO_Sp( sp_no+i, sp_id+i*2,T_DEMO_SPRITE_POS[i][1] , T_DEMO_SPRITE_POS[i][0] );
	}

*/
	//Disp_DEMO_Sp();
	m_R_FrameCountP = 0;
	//Disp_DEMOOPEN_Sp(1,0,_x,_y);

}
//======================================================================
//	[状態] 計算画面00
//======================================================================
void Seq_DEMOOPEN_Main00(void)
{
	Init_DEMOOPEN_Main();
//	pHoshi[R_DEMO_SubSeq] = 0;
	mcrFadeIn(m_R_SeqNo+1);
}
void Seq_DEMOOPEN_Main01(void)
{

INT16S px,py;
if (m_R_FrameCountP%200 ==10) {
	mf_F_PlayDrm(D_ASD_SE_CH,DRM_SE_MAX+15);//FASE_07_64K.drm
	mf_F_TextPosSet(C_PPU_TEXT2, 480, 0);
	//Disp_DEMOOPEN_Sp(1,4+1,_x,_y);
	//F_SpriteZoomSet(1,32+4);
	}
if (m_R_FrameCountP%200 ==30) {
	mf_F_TextPosSet(C_PPU_TEXT2, 480,0);
	//F_TextPosSet(C_PPU_TEXT2,0,320);
	//Disp_DEMOOPEN_Sp(1,8+2,_x,_y);
	//F_SpriteZoomSet(1,32+4+4);
	}
if (m_R_FrameCountP%200 ==40) {
	mf_F_TextPosSet(C_PPU_TEXT2, 0,320);
//	F_TextPosSet(C_PPU_TEXT2, 480,320);
//	Disp_DEMOOPEN_Sp(1,12+3,_x,_y);
//	F_SpriteZoomSet(1,32+4+4+4);
	}
if (m_R_FrameCountP%200 == 55) {
	//F_TextPosSet(C_PPU_TEXT2, 480,320);
	mcrFadeOut(D_DEMO_SEQ_TO_HOME);
	}
/*if(F_TpGetTrigOnXY(&px, &py) == D_TP_SINGLE) {
	mcrFadeOut(D_DEMO_SEQ_TO_HOME);
}*/
mf_F_PPU_DispFlagOn();

}
void Seq_DEMOOPEN_Main02(void)
{
if( mf_F_CheckDrmAll() )
		return;

	mf_F_MyMemSet((INT32U)mp_R_JpadReserved, 0, sizeof(R_JpadReserved) - 16, D_MY_MEM_UNIT8);

    mf_F_InfoBarOnOffSet(FALSE);
	mf_F_ReleaseAllUnpack();
	mf_F_ReleaseWork();
	R_SubProgState = D_SPROG_FINISH;
	switch(mp_R_JpadReserved[63]){
		case 9://広場
			m_R_ModeNo = D_FL11_MODE;
			mp_R_JpadReserved[63] = 1;
			mp_R_JpadReserved[62] = 0;
		break;
		case 8://スクール
			m_R_ModeNo = D_FL12_MODE;
			mp_R_JpadReserved[63] = 1;
			mp_R_JpadReserved[62] = 0;
		break;

		default:
		m_R_ModeNo = D_FL03_MODE;
	}
	//m_R_ModeNo = D_AP02_MODE;
	m_R_SeqNo = 0;
	}
