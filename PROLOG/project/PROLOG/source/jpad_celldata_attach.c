#include	"gplib.h"
#include	"jpad_header.h"
//#include 	"sprite.h"
#include 	"jpad_celldata_attach.h"



//pack後の 前の　src1 src2　展開後サイズ len1 len2 結合ch1 ch2 結合後wch
INT32U celldata_atache_unpackdata(INT8S *src1, INT8S *src2, INT32U len1,
		INT32U len2, INT16U lch1, INT16U lch2, INT16U wch) {

	INT32U size;
	INT8S* dst;

	size = len1 + len2;
	dst = (INT8S *) mf_gp_malloc_align(size, 4);
	if (!dst) {
		mf_gp_free((void *)dst);
	}
	if(src1){
	mf_gp_memcpy(dst, src1, len1);
	}
	if(src2){
	mf_gp_memcpy(&dst[len1], src2, len2);
	}
	if(src1){
	mf_F_ReleaseUnpack(lch1);
	}
	if(src2){
	mf_F_ReleaseUnpack(lch2);
	}
	mf_F_ReleaseUnpack(wch);
	mp_R_UnpackAddr[wch] = (INT32U) dst;
	return (INT32U) dst;
}

//unpack 前の　Data AddData　展開後サイズ len1 len2 結合ch1 ch2 結合後wch
INT32U celldata_atache(INT8U *Data, INT8U *AddData, INT32U len1, INT32U len2,
		INT16S lch1, INT16S lch2, INT16S wch) {

	INT32U addr0, addr1;

	// sprite
	addr0 = mf_F_CellDataUnpack1(Data, lch1);

	addr1 = mf_F_CellDataUnpack1(AddData, lch2);

	return celldata_atache_unpackdata((INT8S*) addr0, (INT8S*) addr1, len1,
			len2, lch1, lch2, wch);
}
//unpack 後の　データーのデーターサイズ取得
INT32U getpackdata_size(INT8U *src) {
	INT32U hd[4], sum;
	// 解凍先バッファ確保＆解凍
	hd[0] = *(src + 0);
	hd[1] = *(src + 1);
	hd[2] = *(src + 2);
	hd[3] = *(src + 3);
	sum = (hd[1] << 16) + (hd[2] << 8) + hd[3];
	return sum;
}
//unpack 前の　src1 src2 結合前ch1 ch2 結合後wch
INT32U celldata_atache_from_packdata(INT8U *src1, INT8U *src2, INT16S lch1,
		INT16S lch2, INT16S wch) {
	INT32U len1 = getpackdata_size(src1);
	INT32U len2 = getpackdata_size(src2);
	return celldata_atache(src1, src2, len1, len2, lch1, lch2, wch);

}
//結合ch1 ch2 結合後wch
INT32U unpacked_channel_add(INT32U len1, INT32U len2,
		INT16U lch1, INT16U lch2, INT16U wch) {

	return celldata_atache_unpackdata((INT8S *)mp_R_UnpackAddr[lch1], mp_R_UnpackAddr[lch2], len1,
			len2, lch1, lch2, wch) ;

}



//指定アドレスsrcp+srcofst から　指定アドレスdstp dstofst　を　指定サイズlength分入れかえる
//void data_replace(INT8S* srcp,INT8S* dstp, INT32U srcofst, INT32U dstofst, INT32U length){
void data_replace(INT32U srcp,INT32U dstp, INT32U srcofst, INT32U dstofst, INT32U length){
//mf_gp_memcpy(&srcp[86*64*64/2],&dstp[1*64*64/2],10*64*64/2);
mf_gp_memcpy(&((INT8S*)srcp)[srcofst],&((INT8S*)dstp)[dstofst],length);
 //mf_gp_memcpy(&dstp[srcofst],&srcp[dstofst],length);
}

//指定アドレスsrcp+srcofst から　指定no 指定no　を　指定ﾁｯﾌﾟ数分入れかえる  チップサイズH チップサイズV

void cell_data_replace(INT32U srcp,INT32U dstp, INT16U srcno, INT16U dstno ,INT16U hsize,INT16U vsize,INT16U length){
INT32U chipsize = vsize*hsize/2;
data_replace(srcp, dstp, srcno*chipsize, dstno*chipsize, length*chipsize);

}
