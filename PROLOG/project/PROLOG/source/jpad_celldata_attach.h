#ifndef __JPAD_CELLDATA_ATACH
#define __JPAD_CELLDATA_ATACH
//２つの　celldata　を　結合する
// 

//pack後の 　src1 src2　展開後サイズ len1 len2 結合ch1 ch2 結合後wch
INT32U celldata_atache_unpackdata(INT8S *src1, INT8S *src2, INT32U len1,
		INT32U len2, INT16U lch1, INT16U lch2, INT16U wch);

//unpack 前の　Data AddData　展開後サイズ len1 len2 結合ch1 ch2 結合後wch
INT32U celldata_atache(INT8U *Data, INT8U *AddData, INT32U len1, INT32U len2,
		INT16S lch1, INT16S lch2, INT16S wch);

//unpack 前の　src1 src2 結合前ch1 ch2 結合後wch
INT32U celldata_atache_from_packdata(INT8U *src1, INT8U *src2,
		INT16S lch1, INT16S lch2, INT16S wch);

//結合ch1 ch2 結合後wch
INT32U unpacked_channel_add(INT32U len1, INT32U len2,
		INT16U lch1, INT16U lch2, INT16U wch);

//unpack 後の　データーのデーターサイズ取得
INT32U getpackdata_size(INT8U *src) ;

//指定アドレスsrcp+srcofst から　指定アドレスdstp dstofst　を　指定サイズlength分入れかえる
//void data_replace(INT8S* srcp,INT8S* dstp, INT32U srcofst, INT32U dstofst, INT32U length);
void data_replace(INT32U srcp,INT32U dstp, INT32U srcofst, INT32U dstofst, INT32U length);
//指定アドレスsrcp+srcofst から　指定no 指定no　を　指定ﾁｯﾌﾟ数分入れかえる  チップサイズH チップサイズV

void cell_data_replace(INT32U srcp,INT32U dstp, INT16U srcno, INT16U dstno ,INT16U hsize,INT16U vsize,INT16U length);

#endif
