#ifndef	__JPAD_VIDEO_HEADER__
#define	__JPAD_VIDEO_HEADER__
//======================================================================
//======================================================================
#include	"application.h"
//======================================================================
//======================================================================
#define	D_VIDEO_REC_STOP		0x00
#define	D_VIDEO_REC_PREV		0x01
#define	D_VIDEO_REC_PREV_PAUSE	0x02
#define	D_VIDEO_REC_START		0x04
#define	D_VIDEO_REC_PAUSE		0x08

//------------------------------
#define	D_MVLIST_MAX			(365 * 10 + 2)
#define	D_MVLIST_MAX_SIZE		(sizeof(MVLIST)*D_MVLIST_MAX)
#define	D_MVLIST_DAY_MAX		(99)
#define	D_MVLIST_STATE_NON		0
#define	D_MVLIST_STATE_NORMAL	1
#define	D_MVLIST_STATE_EDIT		2
#define	D_MVEDIT_CHAPTER_MAX	(30)

//------------------------------
//#define	C_VIDEO_FOLDER				"C:\\MOVE"
//#define	C_VIDEO_REC_FOLDER			"C:\\MOVE\\REC"
//#define	C_VIDEO_TEMP_FOLDER			"C:\\MOVE\\TEMP"
//#define	C_VIDEO_MVLIST_FILE_PATH	"C:\\MOVE\\REC\\mvlist"

//======================================================================
//======================================================================
//------------------------------
//	mvlist(32byte)
//------------------------------
typedef struct tagMVList {
	INT16U		count;
	INT16U		last_no;
	INT32U		state[((D_MVLIST_DAY_MAX << 1) + 31) / 32];
}MVLIST, *PMVLIST;
//------------------------------
//	edit chapter(20byte)
//------------------------------
typedef struct tagMVEditChapter {
	INT32U		sec;
	INT8U		frame_no[2];
	INT8U		frame_cnt[2];
	INT16U		frame_anim_time;
	INT16U		bgm_no;
	INT32U		bgm_offset;
	INT32U		bgm_size;
}MVEDITCHAPTER, *PMVEDITCHAPTER;
//------------------------------
//	edit work(12byte)
//------------------------------
typedef struct tagMVEdit {
	INT16U		num;
	INT16U		frame_src;
	INT16U		bgm_src;
	INT16U		dummy;
	INT32U		chapter_addr;
}MVEDIT, *PMVEDIT;

//======================================================================
//======================================================================
extern	INT8U R_VideoFlag;

//======================================================================
//======================================================================
extern	void F_VideoWorkInit();
extern	void F_VideoPreviewStart(INT8U in_out);
extern	void F_VideoPreviewStartEx(INT8U in_out, VIDEO_ARGUMENT arg);
extern	void F_VideoPreviewStop();
extern	void F_VideoPreviewPause();
extern	void F_VideoPreviewResume();
extern	INT8U F_VideoRecStart(char *path);
extern	void F_VideoRecStop();
extern	void F_VideoRecPause();
extern	void F_VideoRecResume();

#endif
