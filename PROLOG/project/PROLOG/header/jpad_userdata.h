#ifndef	__JPAD_USERDATA_HEADER__
#define	__JPAD_USERDATA_HEADER__
//======================================================================
//	保存パス
//======================================================================
//----- system -----
#define	A_JPAD_SYS_FOLDER_PATH			"A:\\JPADSYS"
#define	A_TPCAL_PATH					"A:\\JPADSYS\\tpcal.bin"
#define	A_INIT_TIME_PATH				"A:\\JPADSYS\\init_time.bin"
#define	A_SYSTEM_TIME_PATH				"A:\\JPADSYS\\system_time.bin"
#define	A_JPAD_MANUAL_VER_PATH			"A:\\MANUAL\\ver"

//----- jpad save folder -----
#define	A_JPAD_SAVE_FOLDER_PATH			"A:\\JPADSAVE"
#define	C_JPAD_SAVE_FOLDER_PATH			"C:\\JPADSAVE"

//----- userdata -----
#define	A_JPAD_USERDATA_FOLDER_PATH		"A:\\JPADSAVE\\user"
#define	A_JPAD_USERDATA_PATH			"A:\\JPADSAVE\\user\\userdata.bin"
#define	A_JPAD_USERDATA_BAK_PATH		"A:\\JPADSAVE\\user\\userdata.bak"
#define	A_JPAD_CALENDAR_PATH			"A:\\JPADSAVE\\user\\calendar.bin"

//----- photo -----
#define	A_JPAD_PHOTO_FOLDER_PATH		"A:\\JPADSAVE\\photo"
#define	A_JPAD_PHOTO_PATH_BASE			"A:\\JPADSAVE\\photo\\photo%02d.jpg"
#define	C_PHOTO_FOLDER_PATH				"C:\\JPDPH"
#define	C_PHOTO_LIST_PATH				"C:\\JPDPH\\list"
#define	C_PHOTO_SUB_FOLDER_PATH_BASE	"C:\\JPDPH\\JPD%04d"
#define	C_PHOTO_PATH_BASE				"C:\\JPDPH\\JPD%04d\\JPD%05d.jpg"

//----- prof -----
#define	A_JPAD_PROF_FOLDER_PATH			"A:\\JPADSAVE\\prof"
#define	A_JPAD_PROF_LIST_PATH			"A:\\JPADSAVE\\prof\\prof_list.bin"
#define	A_JPAD_PROF_PATH_BASE			"A:\\JPADSAVE\\prof\\friend%02d.bin"

//----- mail -----
#define	A_JPAD_MAIL_FOLDER_PATH			"A:\\JPADSAVE\\mail"
#define	A_JPAD_SEND_MAIL_PATH			"A:\\JPADSAVE\\mail\\send_mail.bin"
#define	A_JPAD_REC_MAIL_LIST_PATH		"A:\\JPADSAVE\\mail\\rec_mail_list.bin"
#define	A_JPAD_REC_MAIL_PATH			"A:\\JPADSAVE\\mail\\rec_mail.bin"

//----- voice memo -----
#define	A_JPAD_VOICE_FOLDER_PATH		"A:\\JPADSAVE\\voice"
#define	A_JPAD_VOICE_PATH_BASE			"A:\\JPADSAVE\\voice\\voice%02d.wav"
#define	A_JPAD_VOICE_TEMP_PATH			"A:\\JPADSAVE\\voice\\temp.wav"
#define	C_JPAD_VOICE_FOLDER_PATH		"C:\\JPADSAVE\\voice"
#define	C_JPAD_VOICE_PATH_BASE			"C:\\JPADSAVE\\voice\\voice%02d.wav"
#define	C_JPAD_VOICE_TEMP_PATH			"C:\\JPADSAVE\\voice\\temp.wav"

//----- free memo -----
#define	A_JPAD_MEMO_FOLDER_PATH			"A:\\JPADSAVE\\oekaki"
//#define	A_JPAD_MEMO_LIST_PATH			"A:\\JPADSAVE\\oekaki\\oekaki_list.bin"
#define	A_JPAD_MEMO_PATH_BASE			"A:\\JPADSAVE\\oekaki\\oekaki%03d.bmp"
#define	A_JPAD_MEMO_TEMP_PATH			"A:\\JPADSAVE\\oekaki\\temp.bmp"
#define	C_JPAD_MEMO_FOLDER_PATH			"C:\\JPADSAVE\\oekaki"
//#define	C_JPAD_MEMO_LIST_PATH			"C:\\JPADSAVE\\oekaki\\list"
#define	C_JPAD_MEMO_PATH_BASE			"C:\\JPADSAVE\\oekaki\\oekaki%03d.bmp"
#define	C_JPAD_MEMO_TEMP_PATH			"C:\\JPADSAVE\\oekaki\\temp.bmp"

//----- closet -----
#define	A_JPAD_CLOSET_FOLDER_PATH		"A:\\JPADSAVE\\closet"
#define	A_JPAD_CLOSET_LIST_PATH			"A:\\JPADSAVE\\closet\\closet_list.bin"
#define	A_JPAD_CLOSET_T_PATH_BASE		"A:\\JPADSAVE\\closet\\tops%02d.jpg"
#define	A_JPAD_CLOSET_B_PATH_BASE		"A:\\JPADSAVE\\closet\\bottoms%02d.jpg"
#define	C_JPAD_CLOSET_FOLDER_PATH		"C:\\JPADSAVE\\closet"
#define	C_JPAD_CLOSET_LIST_PATH			"C:\\JPADSAVE\\closet\\list"
#define	C_JPAD_CLOSET_T_PATH_BASE		"C:\\JPADSAVE\\closet\\tops%02d.jpg"
#define	C_JPAD_CLOSET_B_PATH_BASE		"C:\\JPADSAVE\\closet\\bottoms%02d.jpg"

//----- movie ----
#define	A_JPAD_MOVIE_FOLDER_PATH		"A:\\JPADSAVE\\movie"
#define	A_JPAD_MOVIE_MVLIST_PATH		"A:\\JPADSAVE\\movie\\mvlist"
#define	A_JPAD_MOVIE_TEMP_PATH			"A:\\JPADSAVE\\movie\\temp.jmv"
#define	A_JPAD_MOVIE_PATH				"A:\\JPADSAVE\\movie\\movie.jmv"
#define	A_JPAD_MOVIE_NAME				"movie.jmv"
#define	A_JPAD_MOVIE_LST_PATH			"A:\\JPADSAVE\\movie\\list.lst"
#define	A_JPAD_LAST_MOVIE_PATH			"A:\\JPADSAVE\\movie\\last_movie.bin"
#define	C_VIDEO_FOLDER					"C:\\MOVE"
#define	C_VIDEO_REC_FOLDER				"C:\\MOVE\\REC"
#define	C_VIDEO_MVLIST_FILE_PATH		"C:\\MOVE\\REC\\mvlist"
#define	C_VIDEO_REC_FOLDER_PATH_BASE	"C:\\MOVE\\REC\\%02d%02d%02d"
#define	C_VIDEO_REC_TEMP_PATH_BASE		"C:\\MOVE\\REC\\%02d%02d%02d\\temp.jmv"
#define	C_VIDEO_REC_PATH_BASE			"C:\\MOVE\\REC\\%02d%02d%02d\\%02d%02d%02d_%02d.jmv"
#define	C_VIDEO_REC_LST_PATH_BASE		"C:\\MOVE\\REC\\%02d%02d%02d\\%02d%02d%02d_%02d.lst"
#define	C_VIDEO_TEMP_FOLDER				"C:\\MOVE\\TEMP"
#define	C_VIDEO_TEMP_REC_PATH			"C:\\MOVE\\TEMP\\temp.jmv"
#define	C_VIDEO_TEMP_PATH_BASE			"C:\\MOVE\\TEMP\\%02d%02d%02d_%02d.jmv"
#define	C_VIDEO_FREE_SEARCH_PATH		"C:\\MOVE\\*.jmv"
#define	C_VIDEO_FREE_PATH_BASE			"C:\\MOVE\\%s"
#define	C_VIDEO_PLAY_PATH_BASE			C_VIDEO_REC_PATH_BASE
#define	C_VIDEO_PLAY_TEMP_PATH_BASE		C_VIDEO_TEMP_PATH_BASE
#define	C_VIDEO_PLAY_LST_PATH_BASE		C_VIDEO_REC_LST_PATH_BASE
#define	C_VIDEO_LAST_PATH_BASE			"C:\\MOVE\\REC\\%02d%02d%02d\\%s"

//----- music -----
#define	A_JPAD_MUSIC_FOLDER_PATH		"A:\\JPADSAVE\\music"
#define	A_JPAD_MUSIC_SEARCH_PATH		"A:\\JPADSAVE\\music\\*.jmp"
#define	A_JPAD_MUSIC_TEMP_PATH			"A:\\JPADSAVE\\music\\temp.jmp"
#define	A_JPAD_MUSIC_PATH_BASE			"A:\\JPADSAVE\\music\\%s"
#define	A_JPAD_MUSIC_DATA_PATH			"A:\\JPADSAVE\\music\\data.bin"
#define	STR_MUSIC_PRE_NAME1				"ＭＵＳＩＣ００１.jmp"
#define	STR_MUSIC_PRE_NAME2				"ＭＵＳＩＣ００２.jmp"
#define	STR_MUSIC_PRE_NAME3				"ＭＵＳＩＣ００３.jmp"
#define	C_JPAD_MUSIC_FOLDER_PATH		"C:\\JMPREC"
#define	C_JPAD_MUSIC_SEARCH_PATH		"C:\\JMPREC\\*.jmp"
#define	C_JPAD_MUSIC_TEMP_PATH			"C:\\JMPREC\\temp.jmp"
#define	C_JPAD_MUSIC_PATH_BASE			"C:\\JMPREC\\%s"

//----- town -----
#define	A_JPAD_TOWN_FOLDER_PATH			"A:\\JPADSAVE\\town"
#define	A_JPAD_TOWN_PATH				"A:\\JPADSAVE\\town\\town.bin"

//---- stamp talk -----
#define	A_JPAD_TALK_FOLDER_PATH			"A:\\JPADSAVE\\talk"
#define	A_JPAD_TALK_PATH				"A:\\JPADSAVE\\talk\\talk.bin"

//---- coupon -----
//#define	A_JPAD_COUPON_FOLDER_PATH		"A:\\JPADSAVE\\coupon"
//#define	STR_COUPON_PRE_NAME1			"coupon001.bin"
#define	C_JPAD_COUPON_FOLDER_PATH		"C:\\JPADSAVE\\coupon"

//----- manga -----
#define	A_JPAD_MANGA_SAVE1_PATH			"A:\\MANGA\\_MAT_ジュエルペット_4コマげきじょう1\\save.bin"
#define	A_JPAD_MANGA_SAVE2_PATH			"A:\\MANGA\\_MAT_ジュエルペット_4コマげきじょう2\\save.bin"
#define	A_JPAD_MANGA_SAVE3_PATH			"A:\\MANGA\\_MAT_ジュエルペット_4コマげきじょう3\\save.bin"
#define	A_JPAD_MANGA_SAVE4_PATH			"A:\\MANGA\\_MAT_ジュエルペット_4コマげきじょう4\\save.bin"
#define	A_JPAD_MANGA_SAVE5_PATH			"A:\\MANGA\\_MDY_こっちがだいじ\\save.bin"

//----- novel -----
#define	A_JPAD_NOVEL_SAVE1_PATH			"A:\\NOVEL\\_NDT_ジュエルペット_ジュエルフェスティバルはおおさわぎ！？1\\save.bin"
#define	A_JPAD_NOVEL_SAVE2_PATH			"A:\\NOVEL\\_NDT_ジュエルペット_ジュエルフェスティバルはおおさわぎ！？2\\save.bin"
#define	A_JPAD_NOVEL_SAVE3_PATH			"A:\\NOVEL\\_NDT_ジュエルペット_ジュエルフェスティバルはおおさわぎ！？3\\save.bin"
#define	A_JPAD_NOVEL_SAVE4_PATH			"A:\\NOVEL\\_NDT_ジュエルペット_ジュエルフェスティバルはおおさわぎ！？4\\save.bin"
#define	A_JPAD_NOVEL_SAVE5_PATH			"A:\\NOVEL\\_NDT_ジュエルペット_ジュエルフェスティバルはおおさわぎ！？5\\save.bin"

//----- backup -----
#define	C_JPAD_BAK_FOLDER_PATH			"C:\\JPAD_BAK"
#define	C_BAK_USERDATA_FOLDER_PATH		"C:\\JPAD_BAK\\user"
#define	C_BAK_USERDATA_PATH				"C:\\JPAD_BAK\\user\\userdata.bin"
#define	C_BAK_CALENDAR_PATH				"C:\\JPAD_BAK\\user\\calendar.bin"
#define	C_BAK_PHOTO_FOLDER_PATH			"C:\\JPAD_BAK\\photo"
#define	C_BAK_PHOTO_PATH_BASE			"C:\\JPAD_BAK\\photo\\photo%02d.jpg"
#define	C_BAK_PROF_FOLDER_PATH			"C:\\JPAD_BAK\\prof"
#define	C_BAK_PROF_LIST_PATH			"C:\\JPAD_BAK\\prof\\prof_list.bin"
#define	C_BAK_PROF_PATH_BASE			"C:\\JPAD_BAK\\prof\\friend%02d.bin"
#define	C_BAK_MAIL_FOLDER_PATH			"C:\\JPAD_BAK\\mail"
#define	C_BAK_SEND_MAIL_PATH			"C:\\JPAD_BAK\\mail\\send_mail.bin"
#define	C_BAK_REC_MAIL_LIST_PATH		"C:\\JPAD_BAK\\mail\\rec_mail_list.bin"
#define	C_BAK_REC_MAIL_PATH				"C:\\JPAD_BAK\\mail\\rec_mail.bin"
#define	C_BAK_VOICE_FOLDER_PATH			"C:\\JPAD_BAK\\voice"
#define	C_BAK_VOICE_LIST_PATH			"C:\\JPAD_BAK\\voice\\voice_list.bin"
#define	C_BAK_VOICE_PATH_BASE			"C:\\JPAD_BAK\\voice\\voice%02d.wav"
#define	C_BAK_MEMO_FOLDER_PATH			"C:\\JPAD_BAK\\oekaki"
//#define	C_BAK_MEMO_LIST_PATH			"C:\\JPAD_BAK\\oekaki\\oekaki_list.bin"
#define	C_BAK_MEMO_PATH_BASE			"C:\\JPAD_BAK\\oekaki\\oekaki%03d.bmp"
#define	C_BAK_CLOSET_FOLDER_PATH		"C:\\JPAD_BAK\\closet"
#define	C_BAK_CLOSET_LIST_PATH			"C:\\JPAD_BAK\\closet\\closet_list.bin"
#define	C_BAK_CLOSET_T_PATH_BASE		"C:\\JPAD_BAK\\closet\\tops%02d.jpg"
#define	C_BAK_CLOSET_B_PATH_BASE		"C:\\JPAD_BAK\\closet\\bottoms%02d.jpg"
#define	C_BAK_MOVIE_FOLDER_PATH			"C:\\JPAD_BAK\\movie"
#define	C_BAK_MOVIE_MVLIST_PATH			"C:\\JPAD_BAK\\movie\\mvlist"
#define	C_BAK_MOVIE_PATH				"C:\\JPAD_BAK\\movie\\movie.jmv"
#define	C_BAK_MOVIE_LST_PATH			"C:\\JPAD_BAK\\movie\\list.lst"
#define	C_BAK_LAST_MOVIE_PATH			"C:\\JPAD_BAK\\movie\\last_movie.bin"
#define	C_BAK_MUSIC_FOLDER_PATH			"C:\\JPAD_BAK\\music"
#define	C_BAK_MUSIC_SEARCH_PATH			"C:\\JPAD_BAK\\music\\*.jmp"
#define	C_BAK_MUSIC_PATH_BASE			"C:\\JPAD_BAK\\music\\%s"
#define	C_BAK_MUSIC_DATA_PATH			"C:\\JPAD_BAK\\music\\data.bin"
#define	C_BAK_TOWN_PATH					"C:\\JPAD_BAK\\user\\town.bin"
#define	C_BAK_TALK_PATH					"C:\\JPAD_BAK\\user\\talk.bin"
#define	C_BAK_MANGA_FOLDER_PATH			"C:\\JPAD_BAK\\MANGA"
#define	C_BAK_MANGA_SAVE1_PATH			"C:\\JPAD_BAK\\MANGA\\save1.bin"
#define	C_BAK_MANGA_SAVE2_PATH			"C:\\JPAD_BAK\\MANGA\\save2.bin"
#define	C_BAK_MANGA_SAVE3_PATH			"C:\\JPAD_BAK\\MANGA\\save3.bin"
#define	C_BAK_MANGA_SAVE4_PATH			"C:\\JPAD_BAK\\MANGA\\save4.bin"
#define	C_BAK_MANGA_SAVE5_PATH			"C:\\JPAD_BAK\\MANGA\\save5.bin"
#define	C_BAK_NOVEL_FOLDER_PATH			"C:\\JPAD_BAK\\NOVEL"
#define	C_BAK_NOVEL_SAVE1_PATH			"C:\\JPAD_BAK\\NOVEL\\save1.bin"
#define	C_BAK_NOVEL_SAVE2_PATH			"C:\\JPAD_BAK\\NOVEL\\save2.bin"
#define	C_BAK_NOVEL_SAVE3_PATH			"C:\\JPAD_BAK\\NOVEL\\save3.bin"
#define	C_BAK_NOVEL_SAVE4_PATH			"C:\\JPAD_BAK\\NOVEL\\save4.bin"
#define	C_BAK_NOVEL_SAVE5_PATH			"C:\\JPAD_BAK\\NOVEL\\save5.bin"

#define	D_BAK_USERDATA_MAX	(4+5+5)
#define	D_BAK_PHOTO_MAX		(10)
#define	D_BAK_PROF_MAX		(1+50)
#define	D_BAK_MAIL_MAX		(1+1+1)
#define	D_BAK_VOICE_MAX		(5)
#define	D_BAK_MEMO_MAX		(5)
#define	D_BAK_CLOSET_MAX	(1+10+10)
#define	D_BAK_MOVIE_MAX		(1+1+1+1)

#define	D_BAK_VERIFY_SIZE	(1048576)		// 1 * 1024 * 1024 = 1M

//----- update -----
#define	C_JPAD_UPDATE_0_PATH			"C:\\JPAD\\JPADUPD0.bin"
#define	C_JPAD_UPDATE_0U_PATH			"C:\\JPAD\\JPADUPD0U.bin"
#define	C_JPAD_UPDATE_1_PATH			"C:\\JPAD\\JPADUPD1.bin"
#define	C_JPAD_UPDATE_1U_PATH			"C:\\JPAD\\JPADUPD1U.bin"

//======================================================================
//	userdata
//======================================================================
//------------------------------
//	avatar
//------------------------------
enum tagAvatarPartsID {
	D_AVATAR_PARTS_ID_HEAR_F,
	D_AVATAR_PARTS_ID_HEAR_B,
	D_AVATAR_PARTS_ID_FACE,
	D_AVATAR_PARTS_ID_CLOTHES,
	D_AVATAR_PARTS_ID_ACC1,
	D_AVATAR_PARTS_ID_ACC2,
	D_AVATAR_PARTS_ID_MAX
};
enum tagAvatarColorID {
	D_AVATAR_COLOR_ID_BROWN,
	D_AVATAR_COLOR_ID_PINK,
	D_AVATAR_COLOR_ID_BLUE,
	D_AVATAR_COLOR_ID_YELLOW,
	D_AVATAR_COLOR_ID_BLACK,
	D_AVATAR_COLOR_ID_GREEN,
	D_AVATAR_COLOR_ID_WHITE,
	D_AVATAR_COLOR_ID_DUMMY07,
	D_AVATAR_COLOR_ID_DUMMY08,
	D_AVATAR_COLOR_ID_DUMMY09,
	D_AVATAR_COLOR_ID_DUMMY10,
	D_AVATAR_COLOR_ID_DUMMY11,
	D_AVATAR_COLOR_ID_DUMMY12,
	D_AVATAR_COLOR_ID_DUMMY13,
	D_AVATAR_COLOR_ID_DUMMY14,
	D_AVATAR_COLOR_ID_DUMMY15,
	D_AVATAR_COLOR_ID_MAX
};
//***#define	D_AVATAR_COLOR_USE			6
//***#define	D_AVATAR_COLOR_USE_BIT		0x003F
#define	D_AVATAR_COLOR_USE			7
#define	D_AVATAR_COLOR_USE_BIT		0x007F
#define	D_AVATAR_COLOR_NUM			16
#define	D_AVATAR_COLOR_PAL_MAX		(D_AVATAR_COLOR_NUM*D_AVATAR_COLOR_ID_MAX)
#define	D_AVATAR_WORD_LEN			10
#define	D_AVATAR_WORD_SIZE			(D_AVATAR_WORD_LEN*3)
#define	D_AVATAR_PARTS_KIND_MAX		30
#define	D_AVATAR_PARTS_NUM_MAX		99

typedef struct tagAvatarParts {
	INT8U	kind[D_AVATAR_PARTS_ID_MAX];
	INT8U	color[D_AVATAR_PARTS_ID_MAX];
	INT16U	word[D_AVATAR_WORD_SIZE];
} AVATARPARTS, *PAVATARPARTS;

typedef struct tagPartsBox {
	INT8U	hear_f[D_AVATAR_COLOR_ID_MAX][D_AVATAR_PARTS_KIND_MAX];
	INT8U	hear_b[D_AVATAR_COLOR_ID_MAX][D_AVATAR_PARTS_KIND_MAX];
	INT8U	face[D_AVATAR_COLOR_ID_MAX][D_AVATAR_PARTS_KIND_MAX];
	INT8U	clothes[D_AVATAR_COLOR_ID_MAX][D_AVATAR_PARTS_KIND_MAX];
	INT8U	acc[D_AVATAR_COLOR_ID_MAX][D_AVATAR_PARTS_KIND_MAX];
} PARTSBOX, *PPARTSBOX;

//------------------------------
//	prof
//------------------------------
#define	D_PROF_NAME_LEN				10
#define	D_PROF_PARAM_MAX			4
#define	D_PROF_DREAM_LEN			10
#define	D_PROF_ANIMAL_LEN			10
#define	D_PROF_TYPE_LEN				10
#define	D_PROF_TV_LEN				20
#define	D_PROF_BOOK_LEN				20
#define	D_PROF_PLACE_LEN			20
#define	D_PROF_SECRET_LEN			30
#define	D_PROF_FREE_LEN				104
#define	D_PROF_MEMO_LEN				(10+10+10+20+20+20+30+104)
#define	D_PROF_MEMO_COMP_SIZE		(((D_PROF_MEMO_LEN*9)+7)>>3)
#define	D_PROF_RESERVED_SIZE		32

#define	D_PROF_DREAM_OFS			0
#define	D_PROF_ANIMAL_OFS			(D_PROF_DREAM_OFS + D_PROF_DREAM_LEN)
#define	D_PROF_TYPE_OFS				(D_PROF_ANIMAL_OFS + D_PROF_ANIMAL_LEN)
#define	D_PROF_TV_OFS				(D_PROF_TYPE_OFS + D_PROF_TYPE_LEN)
#define	D_PROF_BOOK_OFS				(D_PROF_TV_OFS + D_PROF_TV_LEN)
#define	D_PROF_PLACE_OFS			(D_PROF_BOOK_OFS + D_PROF_BOOK_LEN)
#define	D_PROF_SECRET_OFS			(D_PROF_PLACE_OFS + D_PROF_PLACE_LEN)
#define	D_PROF_FREE_OFS				(D_PROF_SECRET_OFS + D_PROF_SECRET_LEN)

typedef struct tagProfData {
	INT16U			name[D_PROF_NAME_LEN];
	INT8U			birth_month;
	INT8U			birth_day;
	INT8U			param[D_PROF_PARAM_MAX];
	INT8U			memo[D_PROF_MEMO_COMP_SIZE];
	INT32U			reserved[D_PROF_RESERVED_SIZE];
	AVATARPARTS		parts;
} PROFDATA, *PPROFDATA;

//------------------------------
//	userdata
//------------------------------
#define	D_USERDATA_INPUT_FINISH		0x00000001
#define	D_DUMMY_POWER_OFF_ALARM		0x00000002

#define	D_UD_JEWEL_COIN_MAX			99999
#define	D_UD_HEART_COUNT_MAX		999
#define	D_UD_APP_BONUS_MAX			64
#define	D_UD_APP_BONUS_SIZE			((D_UD_APP_BONUS_MAX+7)>>3)
#define	D_UD_CMAIL_RAND_MAX			72
#define	D_UD_CMAIL_RAND_SIZE		((D_UD_CMAIL_RAND_MAX+7)>>3)
#define	D_UD_KEY_KIND_MAX			2048
#define	D_UD_KEY_KIND_SIZE			((D_UD_KEY_KIND_MAX+7)>>3)
#define	D_UD_FRAME_KIND_MAX			24
#define	D_UD_STAMP_KIND_MAX			109
#define	D_UD_MINI_ANIM_MAX			25
#define	D_UD_MINI_ANIM_SIZE			((D_UD_MINI_ANIM_MAX+7)>>3)
#define	D_UD_DECO_MOJI_MAX			10
#define	D_UD_DECO_MOJI_SIZE			((D_UD_DECO_MOJI_MAX+7)>>3)
#define	D_UD_HOME_ICON_MAX			52
#define	D_UD_HOME_ICON_SIZE			80
#define	D_UD_HONTAI_PHOTO_MAX		10
//----- standby mode type ------
#define	D_STANDBY_KIND_NORMAL		0
#define	D_STANDBY_KIND_CALENDAR		1
#define	D_STANDBY_KIND_PHOTO		2
//----- home mode type ------
#define	D_HOME_KIND_NORMAL			0
//#define	D_HOME_KIND_CALENDAR		1
#define	D_HOME_KIND_PHOTO			2
#define	D_SMART_MODE_BIT			0x01
#define	D_KURUKURU_MODE_BIT			0x02

enum tagTownFlowerID {
	D_FLOWER_ID_RED,
	D_FLOWER_ID_BLUE,
	D_FLOWER_ID_YELLOW,
	D_FLOWER_ID_MAX
};
enum tagTownOreID {
	D_ORE_ID_AMBER,
	D_ORE_ID_BRASS,
	D_ORE_ID_CRYSTAL,
	D_ORE_ID_DIAMOND,
	D_ORE_ID_EMERALD,
	D_ORE_ID_CUBE,
	D_ORE_ID_MAX
};
enum tagTownSeedID {
	D_SEED_ID_RED,
	D_SEED_ID_BLUE,
	D_SEED_ID_YELLOW,
	D_SEED_ID_UNKNOWN,
	D_SEED_ID_MAX
};
#define	D_TOWN_KAGU_KIND_MAX	116
#define	D_TOWN_KAGU_KIND_SIZE	128

//------------------------------
//	ユーザーデータ(常駐)
//------------------------------
typedef struct tagUserData {
	// system
	INT32U		flag;
	int			version;
	INT16U		last_date;
	// global
	INT32U		jewel_coin;
	INT16U		heart_count;
	INT8U		app_bonus[D_UD_APP_BONUS_SIZE];				// 57個使用 2014.05.01 oka
	INT8U		town_name_flag;
	INT8U		town_seed_grow;
	INT16U		chara_mail_date;
	INT8U		chara_mail_rand[D_UD_CMAIL_RAND_SIZE];		// 70個使用 2014.05.01 oka
	INT8U		mail_kidoku;
	INT8U		complete;
	INT8U		key[D_UD_KEY_KIND_SIZE];					// 2000個使用 2014.05.01 oka
	INT32U		play_time;
	INT32U		news_kidoku[2];
	// collection
	INT8U		frame[D_UD_FRAME_KIND_MAX];
	INT8U		stamp[D_UD_STAMP_KIND_MAX];
	INT8U		mini_anim[D_UD_MINI_ANIM_SIZE];				// 25個使用 2014.05.01 oka
	INT8U		deco_moji[D_UD_DECO_MOJI_SIZE];				// 10個使用 2014.05.01 oka
	// alarm
	INT8U		alarm_en;
	INT8U		alarm_hour;
	INT8U		alarm_min;
	INT8U		alarm_sound;
	// setting
	INT8U		volume;
	INT8U		bright;
	INT8U		tonedown;
	INT8U		smart_mode;
	INT8U		standby_kind;
	INT8U		standby_no;
	INT8U		home_kind;
	INT8U		home_no;
	// home
	INT16U		home_icon[D_UD_HOME_ICON_SIZE];				// 52個使用 2014.05.01 oka
	// photo
	INT8U		photo[D_UD_HONTAI_PHOTO_MAX];				// 10個使用 2014.05.01 oka
	// mytown
	INT8U		flower[D_FLOWER_ID_MAX];
	INT8U		ore[D_ORE_ID_MAX];
	INT8U		seed[D_SEED_ID_MAX];
	INT8U		kagu[D_TOWN_KAGU_KIND_SIZE];				// 116個使用 2014.06.17
	PARTSBOX	parts_box;
	// prof
	PROFDATA	myprof;
	// checksum
	INT32U	checksum;
} USERDATA, *PUSERDATA;
//======================================================================
//	非常駐データ
//======================================================================
//------------------------------
//	カレンダーメモ
//------------------------------
#define	D_CALENDAR_MEMO_LEN			16
#define	D_CALENDAR_MEMO_COMP_SIZE	(((D_CALENDAR_MEMO_LEN*9)+7)>>3)
#define	D_CALENDAR_MEMO_MAX			(365 * 5 + 1)						// ５年分・うるう年１回含む

typedef struct tagCalendarMemo {
	INT8U	flag;
	INT8U	stamp;
	INT16U	memo[D_CALENDAR_MEMO_LEN];
} CALENDARMEMO, *PCALENDARMEMO;
//------------------------------
//	友達プロフ
//------------------------------
#define	D_PROF_LIST_MAX			50

typedef struct tagProfList {
	INT8U	count;
	INT8U	list[D_PROF_LIST_MAX];
} PROFLIST, *PPROFLIST;
//------------------------------
//	メール
//------------------------------
#define	D_MAIL_LIST_MAX			40
#define	D_MAIL_NAME_LEN			D_PROF_NAME_LEN
#define	D_MAIL_TEXT_LEN			(104+1)
#define	D_MAIL_TEXT_COMP_SIZE	(((D_MAIL_TEXT_LEN*9)+7)>>3)

//----- list -----
typedef struct tagMailList {
	INT8U	count;
	INT8U	list[D_MAIL_LIST_MAX];
} MAILLIST, *PMAILLIST;

//----- data -----
typedef struct tagMailData {
	INT16U	date;
	INT8U	time[3];
	INT16U	name[D_MAIL_NAME_LEN];
	INT8U	text[D_MAIL_TEXT_COMP_SIZE];
} MAILDATA, *PMAILDATA;
//------------------------------
//	ボイスメモ
//------------------------------
#define	D_VOICE_MEMO_NAND		0
#define	D_VOICE_MEMO_SD			1
#define	D_VOICE_MEMO_NAND_MAX	5
#define	D_VOICE_MEMO_SD_MAX		15
//------------------------------
//	フリーメモ
//------------------------------
#define	D_FREE_MEMO_NAND			0
#define	D_FREE_MEMO_SD				1
#define	D_FREE_MEMO_NAND_MAX		5
#define	D_FREE_MEMO_SD_MAX			250
#define	D_FREE_MEMO_FHEADER_SIZE	14
#define	D_FREE_MEMO_IHEADER_SIZE	40
#define	D_FREE_MEMO_DATA_SIZE		(360*(270*3+2))		// フルカラー(24bit)
#define	D_FREE_MEMO_FILE_SIZE		(D_FREE_MEMO_FHEADER_SIZE+D_FREE_MEMO_IHEADER_SIZE+D_FREE_MEMO_DATA_SIZE)

typedef struct tagMemoList {
	INT32U	count;
	INT32U	list[(D_FREE_MEMO_SD_MAX+31)/32];
} MEMOLIST, *PMEMOLIST;
//------------------------------
//	クローゼット
//------------------------------
#define	D_CLOSET_NAND			0
#define	D_CLOSET_SD				1
#define	D_CLOSET_KIND_T			0
#define	D_CLOSET_KIND_B			1
#define	D_CLOSET_T_NAND_MAX		10
#define	D_CLOSET_B_NAND_MAX		10
#define	D_CLOSET_T_SD_MAX		30
#define	D_CLOSET_B_SD_MAX		30

typedef struct tagClosetList {
	INT8U	t_count;
	INT8U	t_list[D_CLOSET_T_SD_MAX];
	INT8U	b_count;
	INT8U	b_list[D_CLOSET_B_SD_MAX];
} CLOSETLIST, *PCLOSETLIST;
//------------------------------
//	最後に見た動画ファイル
//------------------------------
#define	D_MOVIE_NAME_LEN		30
#define	D_MOVIE_NAME_SIZE		((D_MOVIE_NAME_LEN<<1)+4+1)

typedef struct tagLastMVInfo {
	INT8U	flag;
	INT8S	name[D_MOVIE_NAME_SIZE];
	INT32U	sec;
	INT32U	size;
} LASTMOVIE, *PLASTMOVIE;
//------------------------------
//	ミュージック
//------------------------------
#define	D_MUSIC_NAME_LEN		30
#define	D_MUSIC_NAME_SIZE		((D_MUSIC_NAME_LEN<<1)+4+1)
#define	D_MUSIC_COUNT_NAND_MAX	100
#define	D_MUSIC_COUNT_SD_MAX	250
#define	D_MUSIC_SEC_MAX			600

typedef struct tagMusicData {
	INT8U	mode;
	INT16U	count;
	INT16U	kanri_no;
	INT8S	last_name_n[D_MUSIC_NAME_SIZE];
	INT8S	last_name_s[D_MUSIC_NAME_SIZE];
	INT8S	pre_name1[D_MUSIC_NAME_SIZE];
	INT8S	pre_name2[D_MUSIC_NAME_SIZE];
	INT8S	pre_name3[D_MUSIC_NAME_SIZE];
	INT8S	pre_music;
} MUSICDATA, *PMUSICDATA;
//------------------------------
//	マイタウン
//------------------------------
#define	D_TOWN_NAME_LEN				10
#define	D_TOWN_KAGU_SET_MAX			80
#define	D_TOWN_PLANT_KIND_MAX		8
#define	D_TOWN_NPC_KIND_MAX			32
#define	D_TOWN_ITEM_MAX				99
#define	D_TOWN_EVENT_CLEAR_MAX		128

enum tagTownShopID {
	D_SHOP_ID_FASHION,
	D_SHOP_ID_GOODS,
	D_SHOP_ID_CAFE,
	D_SHOP_ID_EXCHANGE,
	D_SHOP_ID_MAX
};
#define	D_TOWN_SHOP_LIST_MAX		10

typedef struct tagTownKaguSet {
	INT8U	id;
	INT8U	dir;
	INT16S	px;
	INT16S	py;
} TOWNKAGUSET, *PTOWNKAGUSET;

typedef struct tagTownPlant {
	INT8U	id;
	INT32U	time;
} TOWNPLANT, *PTOWNPLANT;

typedef struct tagTownEvent {
	INT32U	id;
	INT32U	no;
	INT32U	state;
	INT32U	clear[D_TOWN_EVENT_CLEAR_MAX];
} TOWNEVENT, *PTOWNEVENT;

typedef struct tagTownTime {
	INT32U	first;
	INT32U	start;
	INT32U	end;
	INT32U	play;
} TOWNTIME, *PTOWNTIME;

typedef struct tagTownData {
	INT16U		version;
	INT16U		name[D_TOWN_NAME_LEN];
	INT8U		level;
	INT8U		wallpaper;
	TOWNKAGUSET	kagu_set[D_TOWN_KAGU_SET_MAX];
	TOWNPLANT	plant[D_TOWN_PLANT_KIND_MAX];
	TOWNEVENT	event;
	TOWNTIME	play_time;
	INT16U		avatar_pal[D_AVATAR_COLOR_PAL_MAX];
	INT16U		shop_list[D_SHOP_ID_MAX][D_TOWN_SHOP_LIST_MAX];
} TOWNDATA, *PTOWNDATA;
//------------------------------
//	スタンプトーク
//------------------------------
#define	D_TALK_NAME_LEN		10
#define	D_TALK_TEXT_LEN		24
#define	D_TALK_LEN_MAX		(D_TALK_NAME_LEN+D_TALK_TEXT_LEN+1)
#define	D_TALK_LOG_MAX		500

//----- log -----
typedef struct tagLogData {
	INT16U	kind;
	INT16U	icon;
	INT16U	name[D_TALK_NAME_LEN];
	INT16U	text[D_TALK_TEXT_LEN+1];
} LOGDATA, *PLOGDATA;

//----- log save -----
typedef struct tagTalkLog {
	INT32U	system;
	INT16U	ver;
	INT16U	bg;
	INT16U	icon;
	INT16U	name[D_TALK_NAME_LEN];
	INT16U	log_cnt;
	INT16U	log_index[D_TALK_LOG_MAX];
	LOGDATA	log[D_TALK_LOG_MAX];
} TALKLOG, *PTALKLOG;
//======================================================================
//======================================================================
extern const INT16U cHomeIconDefaultTable[];

//======================================================================
//======================================================================
extern void F_JpadSaveFolderCreate();
extern INT8U F_SaveUserData();
extern INT8U F_LoadUserData();
extern void F_InitialUserData();
//----- calendar -----
extern INT8U F_SaveCalendarMemo(INT32U addr);
extern INT8U F_LoadCalendarMemo(INT32U addr);
//----- photo ------
extern INT8U F_SaveHontaiPhoto(INT32U addr, INT8U no, INT32U size);
extern INT32U F_GetHontaiPhotoSize(INT8U no);
extern INT8U F_LoadHontaiPhoto(INT32U addr, INT8U no);
//----- prof -----
extern INT8U F_SaveProfList(INT32U addr);
extern INT8U F_LoadProfList(INT32U addr);
extern INT8U F_SaveFriendProf(INT32U addr, INT8U no);
extern INT8U F_LoadFriendProf(INT32U addr, INT8U no);
//----- mail -----
extern INT8U F_SaveSendMail(INT32U addr);
extern INT8U F_LoadSendMail(INT32U addr);
extern INT8U F_SaveRecMailList(INT32U addr);
extern INT8U F_LoadRecMailList(INT32U addr);
extern INT8U F_SaveRecMail(INT32U addr);
extern INT8U F_LoadRecMail(INT32U addr);
//----- voice memo -----
extern INT8U F_SaveVoiceMemo(INT32U addr, INT8U dst_type, INT8U no, INT32U size);
extern INT32U F_GetVoiceMemoSize(INT8U dst_type, INT8U no);
extern INT8U F_LoadVoiceMemo(INT32U addr, INT8U dst_type, INT8U no);
//----- free memo -----
extern INT8U F_SaveFreeMemoList(INT32U addr, INT8U dst_type);
extern INT8U F_LoadFreeMemoList(INT32U addr, INT8U dst_type);
extern INT8U F_SaveFreeMemo(INT32U addr, INT8U dst_type, INT32U no);
extern INT8U F_LoadFreeMemo(INT32U addr, INT8U dst_type, INT32U no);
//----- closet -----
extern INT8U F_SaveClosetList(INT32U addr, INT8U dst_type);
extern INT8U F_LoadClosetList(INT32U addr, INT8U dst_type);
extern INT8U F_SaveClosetPhoto(INT32U addr, INT8U dst_type, INT8U kind, INT8U no, INT32U size);
extern INT32U F_GetClosetPhotoSize(INT8U dst_type, INT8U kind, INT8U no);
extern INT8U F_LoadClosetPhoto(INT32U addr, INT8U dst_type, INT8U kind, INT8U no);
//----- movie -----
extern INT8U F_SaveLastMovie(INT32U addr);
extern INT8U F_LoadLastMovie(INT32U addr);
//----- music -----
extern INT8U F_SaveMusicData(INT32U addr);
extern INT8U F_LoadMusicData(INT32U addr);
extern INT32U F_GetMusicCountN();
//----- town -----
extern INT8U F_SaveTownData(INT32U addr);
extern INT8U F_LoadTownData(INT32U addr);
//----- stamp talk -----
extern INT8U F_SaveTalkLog(INT32U addr);
extern INT8U F_LoadTalkLog(INT32U addr);

extern void F_JewelCoinPlusMinus(INT32S val, INT8U save);
extern void F_FlowerPlusMinus(INT8U id, INT8S val, INT8U save);
extern INT8U F_GetFlowerCount(INT8U id);
extern void F_OrePlusMinus(INT8U id, INT8S val, INT8U save);
extern INT8U F_GetOreCount(INT8U id);
extern void F_SeedPlusMinus(INT8U id, INT8S val, INT8U save);
extern INT8U F_GetSeedCount(INT8U id);
extern void F_HeartCountPlusMinus(INT16S val, INT8U save);
extern void F_AvartaPartsPlusMinus(INT8U category, INT8U color, INT8U kind, INT8S val, INT8U save);
extern INT8U F_GetAvatarPartsCount(INT8U category, INT8U color, INT8U kind);
extern void F_KaguPlusMinus(INT8U kind, INT8S val, INT8U save);
extern INT8U F_GetKaguCount(INT8U kind);

#endif
