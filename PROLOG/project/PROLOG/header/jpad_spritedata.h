#ifndef	__JPAD_SPRITE_DATA_HEADER__
#define	__JPAD_SPRITE_DATA_HEADER__
/*
Cellモードの　SPRITE の　チップNO　の　位置を変えるためのマクロ 下記のように　SP_OFST を　再設定したのち
 該当　CellIdx の1行目の並びを　OFSTSPRITE(a0,a1,.....a19),
に　変更します

#undef SP_OFST
#define SP_OFST 0x58000
#define OFSTSPRITE(a0,a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19) a0+(SP_OFST*2)/((8<<(vsize>>6))*(8<<(hsize>>4))),a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19//#define OFSTSPRITE(a0,a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19) a1,a2,a0,a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19
const INT16U ALIGN4 _fs02001_IMG0000_CellIdx[]={
OFSTSPRITE(17,	SP_ROTATE0,	   0-64,	SP_ZOOM0,	   0-64,	SP_PBANK3,	SPBLEND_DISABLE,	SP_DEPTH1,	SP_PALETTE0,	SP_VSIZE64,	SP_HSIZE64,	SPVFLIP_DISABLE,	SPHFLIP_DISABLE,	SP_COLOR16,	SP_MOSAIC0,	SPBLEND_LEVEL0,	0,	SpriteNoGroupID,	SPLarge_DISABLE,	SPInterpolation_DISABLE),


CodeBench での　置換は　Ctrl+F の検索　置換メニューでRegular expressionsチェックしたのち

Find→(\d{1,3}),\tSP_ROTATE0,
Replace With →OFSPRITE($1,,\tSP_ROTATE0,

に設定し　ReplaceALL で　置換したのち

Find→SPInterpolation_DISABLE,
Replace With →SPInterpolation_DISABLE\),
に設定し　ReplaceALL で　置換で
書き換えが終わります

*/

//
//

#define OFSTSPRITE(a0,a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19) a0+(SP_OFST*2)/((8<<(vsize>>6))*(8<<(hsize>>4))),a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19//#define OFSTSPRITE(a0,a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19) a1,a2,a0,a1,a2,a3,a4,a5,a6,a7,a8,vsize,hsize,a11,a12,color,a14,a15,a16,a17,a18,a19
#endif
