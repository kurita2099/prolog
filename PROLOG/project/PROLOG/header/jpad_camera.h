#ifndef	__JPAD_CAMERA_HEADER__
#define	__JPAD_CAMERA_HEADER__
//======================================================================
//======================================================================
#include	"application.h"
//======================================================================
//======================================================================
#define	D_CAMERA_WIDTH		640
#define	D_CAMERA_HEIGHT		480

#define	D_CAMERA_INSIDE		0
#define	D_CAMERA_OUTSIDE	1

#define	D_CAMERA_STOP		0x00
#define	D_CAMERA_START		0x01
#define	D_CAMERA_PAUSE		0x02

#define	D_PHOTO_SRC_NAND			0
#define	D_PHOTO_SRC_SD				1
#define	D_PHOTO_SAVE_KIND_CAMERA	0
#define	D_PHOTO_SAVE_KIND_DECO		1
#define	D_PHOTO_SAVE_QUALITY		100

#define	D_PHOTO_LIST_LEN			(2+10000)
#define	D_PHOTO_LIST_SIZE			mcrSize32(D_PHOTO_LIST_LEN)
#define	OFS_PHOTO_LIST_COUNT		0
#define	OFS_PHOTO_LIST_LAST			1
#define	OFS_PHOTO_LIST_NO			2
#define	D_PHOTO_LIST_COUNT_MAX		10000
#define	D_PHOTO_LIST_NUMBER_MAX		99999
#define	D_PHOTO_FILE_SIZE_LIMIT		(524288)			// 512 * 1024

//#define	C_PHOTO_FOLDER			"c:\\JPDPH"
//#define	C_PHOTO_LIST_PATH		"c:\\JPDPH\\list"
//#define	C_PHOTO_SUB_FOLDER		"c:\\JPDPH\\JPD"

//#define	D_PHOTO_DISP_WIDTH		320
//#define	D_PHOTO_DISP_HEIGHT		240
#define	D_PHOTO_DISP_WIDTH		360
#define	D_PHOTO_DISP_HEIGHT		270

//------------------------------
#define	D_PHOTO_RES_OK			0
#define	D_PHOTO_RES_MOUNT_ERR	1
#define	D_PHOTO_RES_ERR			2

#define	D_PHOTO_CAP_OK			D_PHOTO_RES_OK
#define	D_PHOTO_CAP_MOUNT_ERR	D_PHOTO_RES_MOUNT_ERR
#define	D_PHOTO_CAP_ERR			D_PHOTO_RES_ERR
#define	D_PHOTO_CAP_COUNT_FULL	3
#define	D_PHOTO_CAP_NUMBER_FULL	4
#define	D_PHOTO_CAP_OVER		5

//------------------------------
#define	D_PHOTO_SCALE_UP	0
#define	D_PHOTO_SCALE_DOWN	1

#define	D_PHOTO_EFFECT_NONE		0
#define	D_PHOTO_EFFECT_SEPIA	1
#define	D_PHOTO_EFFECT_MONO		2
//======================================================================
//======================================================================
typedef struct tagPhotoInfo {
	INT16S	px;					// 移動用中心座標
	INT16S	py;
	INT16S	sx;					// 座標左上始点
	INT16S	sy;
	INT16U	rw;					// 実サイズ(画像サイズ)
	INT16U	rh;
	INT16U	tw;					// 転送サイズ
	INT16U	th;
	INT16U	hosei_s;			// 転送アドレス補正値
	INT16U	hosei_d;
	INT16U	width;				// 全体サイズ(バッファサイズ)
	INT16U	height;
} PHOTOINFO, *PPHOTOINFO;

//======================================================================
//======================================================================
extern INT8U R_CameraFlag;
extern INT8U R_PhotoEffect;
extern INT8U R_PhotoScaler;
extern INT32U R_PhotoListAddr;
extern INT32U R_PhotoDecodeBuffAddr;
extern INT32U R_PhotoScalerBuffAddr;
extern INT32U R_PhotoDispBuffAddr;
extern PHOTOINFO R_PhotoInfo;

//======================================================================
//======================================================================
extern void F_CameraWorkInit();
extern void F_CameraStart(INT8U in_out);
extern void F_CameraStartEx(INT8U in_out, VIDEO_ARGUMENT arg);
extern void F_CameraStop();
extern void F_CameraPause();
extern void F_CameraResume();
extern void F_CameraSwitch();

extern void F_CreatePhotoBuffer();
extern void F_ReleasePhotoBuffer();
extern INT8U F_PhotoDecode(MEDIA_SOURCE *src, INT16U buf_w, INT16U buf_h, INT16U dat_w, INT16U dat_h);

extern INT32U F_GetPhotoCount(INT8U src_type);
extern INT8U F_LoadPhotoDisp(INT32U index, INT8U src_type, INT16U sx, INT16U sy, INT16U width, INT16U height);

extern INT8U F_SavePhotoNand(INT8U kind, PFRAMEINFO pFrame);
extern INT8U F_LoadPhotoNand(INT32U index);
extern INT8U F_DeletePhotoNand(INT32U index);

extern INT8U F_LoadPhotoList(INT8U mount);
extern void F_ReleasePhotoList();
extern void F_DeletePhotoList(INT32U index);
extern void F_AddPhotoList(INT32U no);
extern INT8U F_SavePhotoList(INT8U mount);
extern INT8U F_SavePhotoSDC(INT8U kind, PFRAMEINFO pFrame);
extern INT8U F_LoadPhotoSDC(INT32U index);
extern INT8U F_DeletePhotoSDC(INT32U index);

extern void F_PhotoScaleSet(INT8U scale);
extern void F_PhotoScaleChange(INT8U up_down);
extern void F_PhotoPosMove(INT16S dx, INT16S dy);
extern void F_PhotoEffect(INT16U *src, INT16U *dst);
extern void F_PhotoEffectSet(INT8U effect);

extern INT8U F_PhotoCapacityCheck(INT8U src_type);

//------------------------------
//	drv_l1_sensor.c
//------------------------------
extern void CSI_Pwdn_Flip();
extern void CSI_MirrorSet(INT8U mode);

#endif
