#ifndef	__JPAD_FPS_HEADER__
#define	__JPAD_FPS_HEADER__
//======================================================================
//======================================================================
#define	D_MAIN_FRAME_RATE	(30)
#define	D_TIME100			3
#define	D_TIME150			5
#define	D_TIME200			7
#define	D_TIME250			8
#define	D_TIME300			10
#define	D_TIME400			13
#define	D_TIME500			17
#define	D_TIME750			25
#define	D_TIME1000			33
#define	D_TIME1250			42
#define	D_TIME1500			50
#define	D_TIME2000			67
#define	D_TIME2500			83
#define	D_TIME3000			100
#define	D_TIME4000			133
#define	D_TIME5000			167
#define	D_TIME_10SEC		333
#define	D_TIME_15SEC		500
#define	D_TIME_30SEC		1000
#define	D_TIME_1MIN			2000
#define	D_TIME_1M30S		3000
#define	D_TIME_2MIN			4000
#define	D_TIME_3MIN			6000

#endif
