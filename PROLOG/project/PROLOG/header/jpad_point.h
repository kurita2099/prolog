#ifndef	__JPAD_POINT_HEADER__
#define	__JPAD_POINT_HEADER__
//======================================================================
//======================================================================
#define	D_POINT_YOKO	0
#define	D_POINT_TATE	1

#define	D_POINT_AMBER	D_ORE_ID_AMBER
#define	D_POINT_BRASS	D_ORE_ID_BRASS
#define	D_POINT_CRYSTAL	D_ORE_ID_CRYSTAL
#define	D_POINT_DIAMOND	D_ORE_ID_DIAMOND
#define	D_POINT_EMERALD	D_ORE_ID_EMERALD
#define	D_POINT_CUBE	D_ORE_ID_CUBE
#define	D_POINT_COIN	D_ORE_ID_MAX

//======================================================================
//======================================================================
extern	void	F_PointGetInit(INT8U,INT8U,INT32U);
extern	INT16U	F_PointGetLoop(void);

#endif
