#ifndef	__JPAD_HEADER_HEADER__
#define	__JPAD_HEADER_HEADER__
//======================================================================
//======================================================================
#include	"project.h"
#include	"os.h"
#include	"drv_l1_gpio.h"
#include	"drv_l1_rtc.h"
#include	"gplib_calendar.h"

#include	"io_extender.h"
#include	"ol.h"
#include	"trgb_driver.h"
#include	"zt2092.h"
#include	"touchpad.h"
#include	"sdc.h"
#include	"asd.h"
#include	"asr.h"
//#include	"libhwr.h"

#include	"jpad_fps.h"
#include	"jpad_font.h"
#include	"jpad_struct.h"
#include	"jpad_userdata.h"
#include	"jpad_code.h"
#include	"jpad_ppu.h"
#include	"jpad_interrupt.h"
#include	"jpad_camera.h"
#include	"jpad_load_app.h"
#include	"jpad_macro.h"
#include	"jpad_ir.h"
#include	"jpad_common.h"
#include	"jpad_video.h"
#include	"jpad_infobar.h"
#include	"jpad_moji.h"
#include	"jpad_notice.h"

//======================================================================
//======================================================================
#define	C_STR_JPAD_SIGNATURE	"JPADPRG"

//======================================================================
//======================================================================
#define	D_BIT0		(1<<0)
#define	D_BIT1		(1<<1)
#define	D_BIT2		(1<<2)
#define	D_BIT3		(1<<3)
#define	D_BIT4		(1<<4)
#define	D_BIT5		(1<<5)
#define	D_BIT6		(1<<6)
#define	D_BIT7		(1<<7)
#define	D_BIT8		(1<<8)
#define	D_BIT9		(1<<9)
#define	D_BIT10		(1<<10)
#define	D_BIT11		(1<<11)
#define	D_BIT12		(1<<12)
#define	D_BIT13		(1<<13)
#define	D_BIT14		(1<<14)
#define	D_BIT15		(1<<15)
#define	D_BIT16		(1<<16)
#define	D_BIT17		(1<<17)
#define	D_BIT18		(1<<18)
#define	D_BIT19		(1<<19)
#define	D_BIT20		(1<<20)
#define	D_BIT21		(1<<21)
#define	D_BIT22		(1<<22)
#define	D_BIT23		(1<<23)
#define	D_BIT24		(1<<24)
#define	D_BIT25		(1<<25)
#define	D_BIT26		(1<<26)
#define	D_BIT27		(1<<27)
#define	D_BIT28		(1<<28)
#define	D_BIT29		(1<<29)
#define	D_BIT30		(1<<30)
#define	D_BIT31		(1<<31)
//======================================================================
//======================================================================
//----- wakeup flag -----
#define	D_WAKEUP_RESET	0
#define	D_WAKEUP_HOME	1
#define	D_WAKEUP_ALARM	2
//----- memory set / copy -----
#define	D_MY_MEM_UNIT8		0
#define	D_MY_MEM_UNIT16		1
#define	D_MY_MEM_UNIT32		2
//----- adc(low voltage detect) -----
#define	D_ADC_KARAYOMI		0x01
#define	D_ADC_GET_ADC		0x02
#define	D_ADC_LVD			0x04
#define	D_ADC_DISABLE		0x08
#define	D_JPAD_ADC_OFFSET	28			// ADC=28, V=0.04V
//----- rtc -----
#define	D_RTC_INI_YEAR	2016
#define	D_RTC_INI_MONTH	1
#define	D_RTC_INI_DAY	1
#define	D_RTC_INI_HOUR	0
#define	D_RTC_INI_MIN	0
#define	D_RTC_INI_SEC	0
#define	D_RTC_MAX_YEAR	2018
#define	mcrMinuteCheck()	(m_R_OldMinute & 0x80000000)
//----- alarm -----
#define	D_ALARM_ENABLE	0x01
#define	D_ALARM_OCCUR	0x02
//----- backlight -----
#define	D_BACKLIGHT_EN		0x01
#define	D_BACKLIGHT_PORT	IO_F2				//IO_G10
#define	D_BL_PORT_ON		DATA_LOW			//DATA_HIGH
#define	D_BL_PORT_OFF		DATA_HIGH			//DATA_LOW
#define	mcrGetToneDownT()	(mp_R_BackLight->td_flag & 0x3F)
//----- tonedown -----
#define	D_BACKLIGHT_TD_EN	0x80
#define	D_BACKLIGHT_TD_ON	0x40
//----- auto power off
#define	D_AUTO_POWER_OFF_EN	0x80000000
//----- idle task ctrl
#define	D_JPAD_IDLE_TASK_ON_OFF_EN	1
#define	D_JPAD_IDLE_ENABLE_BIT		0x8000
#define	D_JPAD_IDLE_RECORD_BIT		0x0001
#define	D_JPAD_IDLE_TASK_PRIO		(OS_LOWEST_PRIO - 1)
#define	mcrIdleTaskPause()			OSTaskSuspend(D_JPAD_IDLE_TASK_PRIO)
#define	mcrIdleTaskResume()			OSTaskResume(D_JPAD_IDLE_TASK_PRIO)
//----- home button -----
//#define	D_HOME_BTN_PORT			IO_F2
#define	D_HOME_BTN_TIME			3000
#define	D_HOME_BTN_BIT			0x01
#define	D_HOME_BTN_TRIG_ON		0x02
#define	D_HOME_BTN_TRIG_OFF		0x04
#define	D_HOME_BTN_LONG_F		0x08
#define	D_HOME_BTN_LONG_ON		0x10
//----- charge -----
#define	D_CHARGE_UNKNOWN		0
#define	D_CHARGE_NOT_CONNECT	1
#define	D_CHARGE_ING			2
#define	D_CHARGE_FULL			3
#define	D_CHARGE_H_WAIT			0		// ms
#define	D_CHARGE_Z_WAIT			100		// ms
//----- camera/WiFi -----
#define	D_CAMERA_SCK_BIT		IO_C2
#define	D_WIFI_MOSI_BIT			IO_C2
#define	D_WIFI_MISO_BIT			IO_C3

//----- update -----
#define	D_UPD_CHK_SIZE_UNIT		4096

//======================================================================
//	mode
//======================================================================
#define	D_TEST_MODE		0
#define	D_STARTUP_MODE	1
#define	D_LVD_MODE		2
#define	D_OPENING_MODE	3
#define	D_STANDBY_MODE	4
#define	D_JPAD_HM_MODE	5

#define	D_AP01_MODE		11
#define	D_AP02_MODE		12
#define	D_AP03_MODE		13
#define	D_AP04_MODE		14
#define	D_AP05_MODE		15
#define	D_AP06_MODE		16
#define	D_AP07_MODE		17
#define	D_AP08_MODE		18
#define	D_AP09_MODE		19
#define	D_AP10_MODE		20
#define	D_AP11_MODE		21
#define	D_AP12_MODE		22
#define	D_AP13_MODE		23
#define	D_AP14_MODE		24
#define	D_AP15_MODE		25
#define	D_AP16_MODE		26
#define	D_AP17_MODE		27
#define	D_AP18_MODE		28
#define	D_AP19_MODE		29
#define	D_AP20_MODE		30
#define	D_AP21_MODE		31
#define	D_AP22_MODE		32
#define	D_AP23_MODE		33
#define	D_AP24_MODE		34
#define	D_AP25_MODE		35
#define	D_AP26_MODE		36
#define	D_AP27_MODE		37
#define	D_AP28_MODE		38
#define	D_AP29_MODE		39
#define	D_AP30_MODE		40
#define	D_AP31_MODE		41
#define	D_AP32_MODE		42
#define	D_AP33_MODE		43
#define	D_AP34_MODE		44
#define	D_AP35_MODE		45
#define	D_AP36_MODE		46
#define	D_AP37_MODE		47
#define	D_AP38_MODE		48
#define	D_AP39_MODE		49
#define	D_AP40_MODE		50
#define	D_AP41_MODE		51
#define	D_AP42_MODE		52
#define	D_AP43_MODE		53
#define	D_AP44_MODE		54
#define	D_AP45_MODE		55
#define	D_AP46_MODE		56
#define	D_AP47_MODE		57
#define	D_AP48_MODE		58
#define	D_AP49_MODE		59
#define	D_AP50_MODE		60
#define	D_AP51_MODE		61
#define	D_AP52_MODE		62

#define D_FS01_MODE     70
#define D_FS02_MODE  D_OPENING_MODE
#define D_FS03_MODE     72
#define D_FS04_MODE     73
#define D_FS05_MODE     74
#define D_FL01_MODE     75
#define D_FL02_MODE     76
#define D_FL03_MODE     77
#define D_FL04_MODE     78
#define D_FL05_MODE     79
#define D_FL06_MODE     80
#define D_FL07_MODE     81
#define D_FL08_MODE     82
#define D_FL09_MODE     83
#define D_FL10_MODE     84
#define D_FL11_MODE     85
#define D_FL12_MODE     86
#define D_FL13_MODE     87
#define D_FL14_MODE     88
#define D_FG01_MODE     89
#define D_FG02_MODE     90
#define D_FG03_MODE     91
#define D_FG04_MODE     92
#define D_FJ01_0MODE     93

#define	D_DEMO_MODE		100

#define	D_NONE_MODE		0xFFFF

//======================================================================
//	macro
//======================================================================
#define	mcrSize8(size)		((size) * sizeof(INT8U))
#define	mcrSize16(size)		((size) * sizeof(INT16U))
#define	mcrSize32(size)		((size) * sizeof(INT32U))
#define	mcrAbs(num)			(((num) > 0) ? (num) : -(num))

//======================================================================
//	extern (変数)
//======================================================================
//----- jpad_main.c -----
extern const int cJpadVer;
extern const unsigned char cJpadSignature[];

//----- jpad_system.c -----
extern INT8U R_WakeupFlag;
extern INT8U R_AlarmFlag;
extern INT8U R_HomePage;
extern INT8U R_HomeBtn;
extern INT8U R_ChargeSeq;
extern INT8U R_ChargeState;
extern INT8U R_BrightLevel;

extern INT16U R_ModeNo;
extern INT16U R_SeqNo;
extern INT16U R_NextMode;
extern INT16U R_NextSeq;
extern INT16U R_SubMode;
extern INT16U R_SubSeq;
extern INT16U R_SoundNo;
extern INT16U R_MainFrame;
extern INT16U R_FrameCountP;
extern INT16U R_FrameCountM;
extern INT16U R_RandBase;
extern INT16U R_SubProgram;
extern INT16U R_JpadIdleCtrl;
extern INT16U R_HomeBtnTimer;
extern INT16U R_ChargeTimer;

extern INT32U R_WorkAddr;
extern INT32U R_Timer1ms;
extern INT32U R_AutoPowerOff;
extern INT32U R_OldMinute;
extern INT32U R_HanyoWork[16];
extern INT32U R_JpadReserved[64];			// 万が一の時の予備領域。通常時は使用しない。

extern JPADSYSTIME		R_JpadSysTime;

extern JPADADC			R_JpadAdc;
extern JPADBACKLIGHT	R_BackLight;
extern USERDATA			R_UserData;

//----------------------------------------------
//	jpad_main.c
//----------------------------------------------
extern const int cJpadVer;
extern const unsigned char cJpadSignature[];
extern const INT8U cDayCheckTbl[];
extern INT32U qrcode_frame;
//----------------------------------------------
//	jpad_system.c
//----------------------------------------------
extern const INT16U cLowVoltageTbl[];
extern const INT8U cBackLightDutyTbl[];
extern const INT32U cBackLightTDTimerTbl[];

//======================================================================
//	extern (関数)
//======================================================================
extern INT32U F_CreateWork(INT32U size, INT32U align);
extern void F_ReleaseWork();
extern void F_MyMemSet(INT32U dst, INT32U val, INT32U size, INT8U type);
extern void F_MyMemCpy(INT32U dst, INT32U src, INT32U size, INT8U type);
extern void F_PortInit();
extern void F_JpadAdcInit();
extern void F_JpadAdcEnableSet(INT8U flag);
extern INT16U F_JpadAdcGetAverage();
extern void F_JpadAdcUpdate();
extern void F_BackLightDutySet(INT8U duty);
extern void F_BackLightLvSet(INT8U lv);
extern void F_BackLightOn();
extern void F_BackLightOff();
extern void F_BackLightTDEnSet(INT8U flag);
extern void F_BackLightTDTimeSet(INT8U time);
extern void F_BackLightTDRestart();
extern INT8U F_BackLightTDCtrl(INT8U key);
extern void F_AutoPowerOffEnSet(INT8U flag);
extern void F_AutoPowerOffRestart();
extern void F_AutoPowerOffCtrl(INT8U key);
extern void F_TokeiReset();
extern void F_TokeiSet(INT32S year, INT32S month, INT32S day, INT32S hour, INT32S min, INT32S sec);
extern void F_TokeiSetYMD(INT32S year, INT32S month, INT32S day);
extern void F_TokeiSetHMS(INT32S hour, INT32S min, INT32S sec);
extern void F_TokeiCheck();
extern void F_AlarmOn();
extern void F_AlarmOff();
extern INT8U F_AlarmOccurCheck();
extern void F_HomeBtnInit();
extern void F_HomeBtnUpdate();
extern INT8U F_HomeBtnGetTrigOn();
extern INT8U F_HomeBtnGetTrigOff();
extern INT8U F_HomeBtnGetLongOn();
extern INT8U F_HomeBtnGetNow();
extern void F_ChargeCheckInit();
extern void F_ChargeCheck();
extern void F_JpadPowerOff();
//------------------------------
//	jpad_app_startup.c
//------------------------------
extern void F_JpadBackupCopyUserData(INT32U cnt);
extern void F_JpadBackupCopyPhoto(INT32U cnt);
extern void F_JpadBackupCopyProf(INT32U cnt);
extern void F_JpadBackupCopyMail(INT32U cnt);
extern void F_JpadBackupCopyVoice(INT32U cnt);
extern void F_JpadBackupCopyMemo(INT32U cnt);
extern void F_JpadBackupCopyCloset(INT32U cnt);
extern void F_JpadBackupCopyMovie(INT32U cnt);
extern void F_JpadBackupCopyMusic(INT32U cnt);
extern INT8U F_JpadBackupVerifyUserData(INT32U cnt);
extern INT8U F_JpadBackupVerifyPhoto(INT32U cnt);
extern INT8U F_JpadBackupVerifyProf(INT32U cnt);
extern INT8U F_JpadBackupVerifyMail(INT32U cnt);
extern INT8U F_JpadBackupVerifyVoice(INT32U cnt);
extern INT8U F_JpadBackupVerifyMemo(INT32U cnt);
extern INT8U F_JpadBackupVerifyCloset(INT32U cnt);
extern INT8U F_JpadBackupVerifyMovie(INT32U cnt);
extern INT8U F_JpadBackupVerifyMusic(INT32U cnt);
extern INT8U F_JpadBackupVerify1(char *path1, char *path2, INT8U ofs);
extern INT8U F_JpadBackupVerify2(char *path1, char *path2, INT8U ofs);
extern void F_JpadUpdateDelUserData(INT32U cnt);
extern void F_JpadUpdateDelPhoto(INT32U cnt);
extern void F_JpadUpdateDelProf(INT32U cnt);
extern void F_JpadUpdateDelMail(INT32U cnt);
extern void F_JpadUpdateDelVoice(INT32U cnt);
extern void F_JpadUpdateDelMemo(INT32U cnt);
extern void F_JpadUpdateDelCloset(INT32U cnt);
extern void F_JpadUpdateDelMovie(INT32U cnt);
extern void F_JpadUpdateDelMusic(INT32U cnt);
//------------------------------
//	jpad_app_testmode.c
//------------------------------
extern void F_JpadRestoreCopyUserData(INT32U cnt);
extern void F_JpadRestoreCopyPhoto(INT32U cnt);
extern void F_JpadRestoreCopyProf(INT32U cnt);
extern void F_JpadRestoreCopyMail(INT32U cnt);
extern void F_JpadRestoreCopyVoice(INT32U cnt);
extern void F_JpadRestoreCopyMemo(INT32U cnt);
extern void F_JpadRestoreCopyCloset(INT32U cnt);
extern void F_JpadRestoreCopyMovie(INT32U cnt);
extern void F_JpadRestoreCopyMusic(INT32U cnt);
extern INT8U F_JpadRestoreVerifyUserData(INT32U cnt);
extern INT8U F_JpadRestoreVerifyPhoto(INT32U cnt);
extern INT8U F_JpadRestoreVerifyProf(INT32U cnt);
extern INT8U F_JpadRestoreVerifyMail(INT32U cnt);
extern INT8U F_JpadRestoreVerifyVoice(INT32U cnt);
extern INT8U F_JpadRestoreVerifyMemo(INT32U cnt);
extern INT8U F_JpadRestoreVerifyCloset(INT32U cnt);
extern INT8U F_JpadRestoreVerifyMovie(INT32U cnt);
extern INT8U F_JpadRestoreVerifyMusic(INT32U cnt);
//------------------------------
//	jpad_app_lvd.c
//------------------------------
extern void F_LvdModeInit();

//------------------------------
//	nand_app_upgrade.c
//------------------------------
extern INT32S Nand_app_upgrade(INT8U part, CHAR *path);
extern INT8U F_NandAppUpgradeVerify(INT8U part, CHAR *path);
extern void F_NandAppUpgradeCompleteSet();
extern INT8U F_NandAppCheckInfo(INT8U part, INT8U *buff);
extern INT8U F_NandAppChecksum(INT8U part, INT32U *ret_sum);

#endif
