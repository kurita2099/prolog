#ifndef	__JPAD_SPRITE_HEADER__
#define	__JPAD_SPRITE_HEADER__
//======================================================================
//======================================================================
extern void F_SpritePosExchange(INT32U sp, INT16U idx, INT16S *px, INT16S *py);
extern INT32U F_GetSpriteAddr(INT32U sp, INT16U idx);

#endif
