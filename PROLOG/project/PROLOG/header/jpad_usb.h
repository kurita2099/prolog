#ifndef	__JPAD_USB_HEADER__
#define	__JPAD_USB_HEADER__
//======================================================================
//======================================================================
extern INT32U	CBI_Byte1;
extern INT16U	usbh_vid;
extern INT16U	usbh_pid;
extern INT32U	g_ifchirp_pass;
extern INT32U	CapacitySize;
extern INT32U	gBlk2BufMask;
extern INT32U	gSec2BlkSft;
extern INT32U	gBlk2BufCnt;
extern INT16S	bUSB_Init;
extern INT16S	bUSB_LUN_Init;
extern INT32U	arUSBHostBuffer[64];
extern INT32U	arUSBHostCSWBuffer[16];
extern INT32U	INTINEP;
extern INT32U	BulkINEP;
extern INT32U	BulkOUTEP;
extern INT32U	EP0MaxPacketSize;
extern INT32U	ResetUSBHost;
extern INT32U	R_Device_Address;
extern INT32U	DeviceClass;
extern INT32U	USBH_MaxLUN;
extern INT32U	INTPipeDataToggle;
extern INT32U	BulkINDataToggle;
extern INT32U	BulkOUTDataToggle;
extern INT32U	R_USBHDMAEN;
extern INT32U	R_USBHLUNStatus;
extern INT32U	USBSectorSize;
extern INT32U	InterfaceProtocol;
extern INT32U	InterfaceNumber;
extern INT32U	InterfaceSubClass;
extern INT32U	Sense_Code;
extern INT32U	Bulk_Stall;
extern unsigned int	R_USB_State_Machine;
extern unsigned int	USB_Status;
extern unsigned int	R_USB_Suspend;
extern INT32U	s_usbd_user_string;
extern char	*user_string_pt;
extern INT32U	user_string_size;
extern INT8U	CDROMDelay;
extern INT64U	Start_CDROM_Tick;
extern INT32U	CSW_Residue;
extern INT32U	USBD_PlugIn;
extern INT32U	CSW_Status;
extern str_USB_Lun_Info	USB_Lun_Define[];
//----------------------------------------------
//----------------------------------------------
extern INT32S	from_GetDesc_to_GetMaxLUN(void);
extern INT32U	StandardCommandWithINData(INT32U bmRequestType, INT32U bRequest, INT32U wValue, INT32U wIndex, INT32U wLength);
extern INT32U	StandardCommandWithoutData(INT32U bmRequestType, INT32U bRequest, INT32U wValue, INT32U wIndex, INT32U wLength);
extern INT32U	GetMaxLUN(INT32U Number);
extern INT32U	BulkInData(INT32U DataTransferLength, INT32U* StoreAddr);
extern INT32U	BulkOutData(INT32U DataTransferLength, INT32U * StoreAddr);
extern INT32U	SCSICBWWithINData(INT32U* StoreAddr);
extern INT32U	SCSICBWWithNOData(void);
extern INT32U	SCSICBWWithDataOut(INT32U* StoreAddr);
extern INT32S	DrvUSBHLUNInit(INT32U LUN);
extern void	Update_USBLUN(INT32U ret, INT32U LUN);
extern INT32U	BulkOnly_RequestSense(INT32U LUN);
extern INT32S	StandardCommand_CBI(INT32U bmRequestType, INT32U bRequest, INT32U wValue, INT32U wIndex, INT32U wLength);
extern INT32S	CBI_Status(void);
extern INT32U	DrvUSBHGetSectorSize(void);
extern INT8U	usbh_mount_status(void);
extern void	usbh_isr(void);
extern INT32S	DrvUSBHMSDCInitial(void);
extern INT32S	DrvUSBHMSDCUninitial(void);
extern INT32S	MSDC_Read28(INT32U StartLBA, INT32U* StoreAddr, INT32U SectorCnt, INT32U LUN);
extern INT32S	DrvUSBHReadMultiSector(INT32U LBA, INT32U* StoreAddr, INT32U SectorCnt, INT32U LUN);
extern INT32S	CBI_Read28(INT32U LUN, INT32U LBA, INT32U SectorCnt, INT32U * DataBuffer);
extern INT32S	MSDC_Write2A(INT32U StartLBA, INT32U * StoreAddr, INT32U SectorCnt, INT32U LUN);
extern INT32S	DrvUSBHWriteMultiSector(INT32U StartLBA, INT32U* StoreAddr, INT32U SectorCnt, INT32U LUN);
extern INT32U	CBI_Write2A(INT32U LUN, INT32U LBA, INT32U SectorCnt, INT32U* DataBuffer);
extern INT32U	CheckLUNStatus(INT32U LUN);
extern INT32U	SCSI_Data_Stage_IN(unsigned long DataTransferLength, INT32U * StoreAddr);
extern INT32U	SCSI_Data_Stage_OUT(unsigned long DataTransferLength, INT32U * StoreAddr);
extern INT32U	SCSISendCMD(void);
extern void	usbh_usb20_chirp(void);
extern INT32U	DrvUSBHGetCurrent(void);
extern INT8U	fs_usbh_plug_out_flag_get(void);
extern void	fs_usbh_plug_out_flag_reset(void);
extern INT32S	USB_host_buffer_init(INT16U SectorSize);
extern INT32S	USB_host_bufferflush(void);
extern INT32S	USB_host_initial(void);
extern INT32S	USB_host_uninitial(void);
extern INT32S	USBHost_ReadSector(INT32U blkno , INT32U blkcnt ,  INT32U buf, INT16S dsk);
extern INT32S	USBHost_WriteSector(INT32U blkno, INT32U blkcnt, INT32U buf, INT16S dsk);
extern INT32S	USB_host_read_sector(INT32U blkno, INT32U blkcnt, INT32U buf);
extern INT32S	USB_host_write_sector(INT32U blkno, INT32U blkcnt, INT32U buf);
extern INT32S	USB_host_flush(void);
extern INT32U	usb_host_libraryversion(void);
extern INT32U	USBH_Device_Reset(void);
extern INT32U	Send_EP0_Status_Stage(INT32U idata);
extern INT32U	USBH_Check_Status(void);
extern INT32U	EP0StandardCommand(INT32U bmRequestType, INT32U bRequest, INT32U wValue, INT32U wIndex, INT32U wLength);
extern INT32U	EP0_Data_Stage_OUT(INT32U Data_Number);
extern INT32U	EP0_Data_Stage_IN(INT32U Data_Number);
extern void	F_Delay(void);
extern void	USBH_Delay(INT32U t);
extern void	USB_Host_DevAdr(INT32U addr);
extern void	USB_Host_EndPoint(INT32U edp);
extern INT32U	CheckPortStatus(void);
extern INT32U	Data_IN(INT32U DataTransferLength ,INT32U* GetHostBuffer_PTR);
extern INT32U	GetConfigurationNumber(void);
extern INT32U	DecodeDescriptorConfiguration(void);
extern INT32U	GetRequestData(void);
extern void	USB_Host_Enable(void);
extern INT32U	USB_Host_Signal(void);
extern void	USB_Host_Clrirqflag(void);
extern void	USBPlugOutEN(void);
extern void	GetDeviceLength(INT32U Lun);
extern INT32U	CSWStatus(INT32U * GetHostBuffer_PTR);
extern INT32U	GetMaxLUNNumber(void);
extern void	USBH_SELF_RST(void);
extern INT32U	USBG_GET_ACK_CNT(void);
extern void	usbh_usb20_chirp(void);
extern void	usbd_interrupt_register(void (*pt_function)(void));
extern void	usbd_ep0descriptor_register(INT32U (*pt_function)(INT32U nTableType));
extern void	usbd_suspend_register(void (*pt_function)(void));
extern void	usbd_resume_register(void (*pt_function)(void));
extern void	usbd_set_configuration_register(void (*pt_function)(void));
extern void	usbd_reset_register(void (*pt_function)(void));
extern void	usb_initial(void);
extern void	usb_uninitial(void);
extern void	usb_reset(void);
extern void	usb_isr(void);
extern void	usb_std_service(INT32U unUseLoop);
extern void	Read_EP0_FIFO(void);
extern void	usbd_set_trigger(void);
extern unsigned int	USB_CheckRegister_Complete(volatile unsigned int *RegisterAddr,unsigned int CheckValue,unsigned int CheckResult);
extern unsigned int	USB_CheckRegister_Complete_Long(volatile unsigned int *RegisterAddr,unsigned int CheckValue,unsigned int CheckResult);
extern INT32S	usb_os_event_init(void);
extern void	usb_os_event_uninit(void);
extern INT32S	Drv_USBD_AUDIO_ISOIN_DMA_START(INT8U *buf, INT32U len);
extern INT32S	Drv_USBD_AudioISOI_DMA_Finish(void);
extern void	usbd_phy_clock_on(void);
extern INT32U	USB_Host_Disable(void);
extern void	USBH_INT_CLR_IMMEDIATE(void);
extern INT32U	USBH_INT_CLR(void);
extern INT32U	USBH_INT_R(void);
extern void	USBH_INT_W(INT32U value);
extern INT32U	INT_IN(void);
extern void	USB_Host_Handup(INT16U dpdm);
extern void	USB_Host_ClrFIFO(void);
extern INT32S	USBH_Device_Plug_IN(INT8U wait);
extern void	usbd_setuserstring(INT32U size,INT8U *pbuf);
extern void	usbd_setlun(INT16U ilun);
extern void	SetLuntpye(INT8U lun, INT8U ltype);
extern void	usb_isr_clear(void);
extern void	usbd_write10_register(INT32S (*pt_function)(str_USB_Lun_Info* luninfo));
extern void	usbd_read10_register(INT32S (*pt_function)(str_USB_Lun_Info* luninfo));
extern void	usbd_writeprotected_register(INT32U (*pt_function)(void));
extern void	usbd_vendorcallback_register (INT32U (*pt_function)(void));
extern void	usbd_CDROM_ReadSector_register(INT32S (*pt_function)(INT32U blkno, INT32U blkcnt, INT32U buf));
extern void	usb_initial_udisk(void);
extern void	usb_std_service_udisk(INT32U unUseLoop);
extern void	usb_msdc_service( INT32U unUseLoop);
extern INT32U	Receive_From_USB_DMA_USB( INT32U* USB_Buffer_PTR,INT8U playload, INT8U ifcheck);
extern INT32U	Send_To_USB_DMA_USB( INT32U* USB_Buffer_PTR,INT8U playload, INT8U ifcheck);
extern INT32U	GetTransLeng(void);
extern INT32U	GetLBA(void);
extern INT32U	GetCMDValueEx(INT32U index);
extern void	CommandFail(INT32U nSenseIndex);
extern void	GetDriveStatus(void);
extern void	GetICVersion(void);
extern void	SetVenderID(void);
extern void	RegWrite(INT32U REG ,INT32U Count);
extern void	RegRead(INT32U REG,INT32U Count);
extern INT32U	GetDL_length(void);
extern void	JDataWrite(INT32U REG ,INT32U Count,INT32U type);
extern void	USB_PHY_I2C_CMD(INT8U addr, INT8U data,INT8U type);
extern void	DataPointer(unsigned int DescriptorType);
extern unsigned int	Get_Descriptor_Length(unsigned int DescriptorType);
//----------------------------------------------
//----------------------------------------------
extern INT32S	uart0_sw_fifo_init(void);
extern INT32S	uart0_sw_fifo_get(INT8U *data);
extern INT32S	uart0_sw_fifo_put(INT8U data);
extern void	uart0_set_ctrl(INT32U val);
extern INT32U	uart0_get_ctrl(void);
extern INT32U	uart0_get_status(void);
extern void	uart0_fifo_ctrl(INT8U, INT8U);
extern void	uart0_rx_tx_en(INT32U dir, INT32U enable);
extern void	uart0_fifo_en(INT32U enable);
extern void	uart0_fifo_enable(void);
extern void	uart0_fifo_disable(void);
extern void	uart0_init(void);
extern void	uart0_buad_rate_set(INT32U bps);
extern void	uart0_rx_enable(void);
extern void	uart0_rx_disable(void);
extern void	uart0_tx_enable(void);
extern void	uart0_tx_disable(void);
extern INT32S	uart0_word_len_set(INT8U word_len);
extern INT32S	uart0_stop_bit_size_set(INT8U stop_size);
extern INT32S	uart0_parity_chk_set(INT8U status, INT8U parity);
extern void	uart0_data_send(INT8U data, INT8U wait);
extern INT32S	uart0_data_get(INT8U *data, INT8U wait);
extern void	uart0_isr(void);
extern INT32S	uart_device_protect(void);
extern void	uart_device_unprotect(INT32S mask);
//======================================================================
//======================================================================
extern	const void *T_JpadUsbVariableTable[];
//======================================================================
//======================================================================
enum tagJpadUsbVariable {
	ENUM_JUSB_CBI_BYTE1,
	ENUM_JUSB_USBH_VID,
	ENUM_JUSB_USBH_PID,
	ENUM_JUSB_G_IFCHIRP_PASS,
	ENUM_JUSB_CAPACITYSIZE,
	ENUM_JUSB_GBLK2BUFMASK,
	ENUM_JUSB_GSEC2BLKSFT,
	ENUM_JUSB_GBLK2BUFCNT,
	ENUM_JUSB_BUSB_INIT,
	ENUM_JUSB_BUSB_LUN_INIT,
	ENUM_JUSB_ARUSBHOSTBUFFER,
	ENUM_JUSB_ARUSBHOSTCSWBUFFER,
	ENUM_JUSB_INTINEP,
	ENUM_JUSB_BULKINEP,
	ENUM_JUSB_BULKOUTEP,
	ENUM_JUSB_EP0MAXPACKETSIZE,
	ENUM_JUSB_RESETUSBHOST,
	ENUM_JUSB_R_DEVICE_ADDRESS,
	ENUM_JUSB_DEVICECLASS,
	ENUM_JUSB_USBH_MAXLUN,
	ENUM_JUSB_INTPIPEDATATOGGLE,
	ENUM_JUSB_BULKINDATATOGGLE,
	ENUM_JUSB_BULKOUTDATATOGGLE,
	ENUM_JUSB_R_USBHDMAEN,
	ENUM_JUSB_R_USBHLUNSTATUS,
	ENUM_JUSB_USBSECTORSIZE,
	ENUM_JUSB_INTERFACEPROTOCOL,
	ENUM_JUSB_INTERFACENUMBER,
	ENUM_JUSB_INTERFACESUBCLASS,
	ENUM_JUSB_SENSE_CODE,
	ENUM_JUSB_BULK_STALL,
	ENUM_JUSB_R_USB_STATE_MACHINE,
	ENUM_JUSB_USB_STATUS,
	ENUM_JUSB_R_USB_SUSPEND,
	ENUM_JUSB_S_USBD_USER_STRING,
	ENUM_JUSB_USER_STRING_PT,
	ENUM_JUSB_USER_STRING_SIZE,
	ENUM_JUSB_CDROMDELAY,
	ENUM_JUSB_START_CDROM_TICK,
	ENUM_JUSB_CSW_RESIDUE,
	ENUM_JUSB_USBD_PLUGIN,
	ENUM_JUSB_CSW_STATUS,
	ENUM_JUSB_USB_LUN_DEFINE,
	ENUM_JUSB_DUMMY43,
	ENUM_JUSB_DUMMY44,
	ENUM_JUSB_DUMMY45,
	ENUM_JUSB_DUMMY46,
	ENUM_JUSB_DUMMY47,
	ENUM_JUSB_DUMMY48,
	ENUM_JUSB_DUMMY49,
	ENUM_JUSB_DUMMY50,
	ENUM_JUSB_DUMMY51,
	ENUM_JUSB_DUMMY52,
	ENUM_JUSB_DUMMY53,
	ENUM_JUSB_DUMMY54,
	ENUM_JUSB_DUMMY55,
	ENUM_JUSB_DUMMY56,
	ENUM_JUSB_DUMMY57,
	ENUM_JUSB_DUMMY58,
	ENUM_JUSB_DUMMY59,
	ENUM_JUSB_DUMMY60,
	ENUM_JUSB_DUMMY61,
	ENUM_JUSB_DUMMY62,
	ENUM_JUSB_DUMMY63,
	ENUM_JUSB_VARIABLE_MAX
};
//======================================================================
//======================================================================
#define	m_CBI_Byte1						(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_CBI_BYTE1))
#define	m_usbh_vid						(*(INT16U *)m_T_JpadUsbVariableTable(ENUM_JUSB_USBH_VID))
#define	m_usbh_pid						(*(INT16U *)m_T_JpadUsbVariableTable(ENUM_JUSB_USBH_PID))
#define	m_g_ifchirp_pass				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_G_IFCHIRP_PASS))
#define	m_CapacitySize					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_CAPACITYSIZE))
#define	m_gBlk2BufMask					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_GBLK2BUFMASK))
#define	m_gSec2BlkSft					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_GSEC2BLKSFT))
#define	m_gBlk2BufCnt					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_GBLK2BUFCNT))
#define	m_bUSB_Init						(*(INT16S *)m_T_JpadUsbVariableTable(ENUM_JUSB_BUSB_INIT))
#define	m_bUSB_LUN_Init					(*(INT16S *)m_T_JpadUsbVariableTable(ENUM_JUSB_BUSB_LUN_INIT))
#define	m_arUSBHostBuffer				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_ARUSBHOSTBUFFER))
#define	m_arUSBHostCSWBuffer			(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_ARUSBHOSTCSWBUFFER))
#define	m_INTINEP						(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_INTINEP))
#define	m_BulkINEP						(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_BULKINEP))
#define	m_BulkOUTEP						(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_BULKOUTEP))
#define	m_EP0MaxPacketSize				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_EP0MAXPACKETSIZE))
#define	m_ResetUSBHost					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_RESETUSBHOST))
#define	m_R_Device_Address				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_R_DEVICE_ADDRESS))
#define	m_DeviceClass					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_DEVICECLASS))
#define	m_USBH_MaxLUN					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_USBH_MAXLUN))
#define	m_INTPipeDataToggle				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_INTPIPEDATATOGGLE))
#define	m_BulkINDataToggle				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_BULKINDATATOGGLE))
#define	m_BulkOUTDataToggle				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_BULKOUTDATATOGGLE))
#define	m_R_USBHDMAEN					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_R_USBHDMAEN))
#define	m_R_USBHLUNStatus				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_R_USBHLUNSTATUS))
#define	m_USBSectorSize					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_USBSECTORSIZE))
#define	m_InterfaceProtocol				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_INTERFACEPROTOCOL))
#define	m_InterfaceNumber				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_INTERFACENUMBER))
#define	m_InterfaceSubClass				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_INTERFACESUBCLASS))
#define	m_Sense_Code					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_SENSE_CODE))
#define	m_Bulk_Stall					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_BULK_STALL))
#define	m_R_USB_State_Machine			(*(unsigned int *)m_T_JpadUsbVariableTable(ENUM_JUSB_R_USB_STATE_MACHINE))
#define	m_USB_Status					(*(unsigned int *)m_T_JpadUsbVariableTable(ENUM_JUSB_USB_STATUS))
#define	m_R_USB_Suspend					(*(unsigned int *)m_T_JpadUsbVariableTable(ENUM_JUSB_R_USB_SUSPEND))
#define	m_s_usbd_user_string			(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_S_USBD_USER_STRING))
#define	m_user_string_pt				(*(char **)m_T_JpadUsbVariableTable(ENUM_JUSB_USER_STRING_PT))
#define	m_user_string_size				(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_USER_STRING_SIZE))
#define	m_CDROMDelay					(*(INT8U *)m_T_JpadUsbVariableTable(ENUM_JUSB_CDROMDELAY))
#define	m_Start_CDROM_Tick				(*(INT64U *)m_T_JpadUsbVariableTable(ENUM_JUSB_START_CDROM_TICK))
#define	m_CSW_Residue					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_CSW_RESIDUE))
#define	m_USBD_PlugIn					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_USBD_PLUGIN))
#define	m_CSW_Status					(*(INT32U *)m_T_JpadUsbVariableTable(ENUM_JUSB_CSW_STATUS))
#define	mpp_USB_Lun_Define				((str_USB_Lun_Info *)m_T_JpadUsbVariableTable(ENUM_JUSB_USB_LUN_DEFINE))
#define	mp_USB_Lun_Define(n)			(mpp_USB_Lun_Define + (n))
#define	m_USB_Lun_Define(n)				(*mp_USB_Lun_Define(n))

#endif
