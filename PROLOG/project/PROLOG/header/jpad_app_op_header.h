#ifndef	__JPAD_APP_OP_HEADER__
#define	__JPAD_APP_OP_HEADER__
//======================================================================
//======================================================================
#include	"jpad_ap09_sprite.h"
#include	"jpad_app_sys.h"
//======================================================================
//	define
//======================================================================
//----- main seq -----
#define	D_OPENING_FADE_IN_SEQ		1
#define	D_OPENING_FADE_OUT_SEQ		2
#define	D_OPENING_WAIT_ANIM_SEQ		3
#define	D_OPENING_NOTICE_SEQ		4
#define	D_OPENING_CAL_SEQ			5
#define	D_OPENING_STAY_SEQ			6
#define	D_OPENING_YMDHM_SEQ			7
#define	D_OPENING_NAME_SEQ			8
#define	D_OPENING_TO_AP04_SEQ		80
#define	D_OPENING_BACK_HOME_SEQ		0xFFFF

#define	D_AP04_FROM_HOME_SEQ		90

//----- notice -----
enum tagOPNoticeID {
	D_NOTICE_ID_CALINFO,
	D_NOTICE_ID_INTRO,
	D_NOTICE_ID_MAX
};

//----- ようこそ -----
#define	D_YOKOSO_BTN_X	72
#define	D_YOKOSO_BTN_Y	400
#define	D_YOKOSO_BTN_DX	6

//----- YMDHM btn -----
#define	D_YMDHM_OK_BTN_X	0
#define	D_YMDHM_OK_BTN_Y	32

#define	D_YMD_BTN_X1	72
#define	D_YMD_BTN_X2	136
#define	D_YMD_BTN_X3	184
#define	D_YMD_BTN_Y1	236
#define	D_YMD_BTN_Y2	292

#define	D_HM_BTN_X1		80
#define	D_HM_BTN_X2		160
#define	D_HM_BTN_Y1		236
#define	D_HM_BTN_Y2		292

//----- stay btn no -----
#define	D_BTN_NO_S_YOKOSO	1
#define	D_BTN_NO_S_YMD		2
#define	D_BTN_NO_S_HM		3
#define	D_BTN_NO_S_NAME		4

//----- YMDHM btn no -----
#define	D_BTN_NO_Y_BACK		1
#define	D_BTN_NO_Y_OK		2
#define	D_BTN_NO_Y_U1		3
#define	D_BTN_NO_Y_D1		4
#define	D_BTN_NO_Y_U2		5
#define	D_BTN_NO_Y_D2		6
#define	D_BTN_NO_Y_U3		7
#define	D_BTN_NO_Y_D3		8
#define	D_BTN_NO_Y_YMD		9
#define	D_BTN_NO_Y_HM		10
#define	D_BTN_NO_Y_SLIDE1	11
#define	D_BTN_NO_Y_SLIDE2	12
#define	D_BTN_NO_Y_SLIDE3	13

//----- wait anim -----
#define	D_WA_YOKOSO_BTN		0
#define	D_WA_YMDHM_BTN		1

//----- slide -----
#define	D_OPENING_SLIDE_DY		64

//----- tp calibration -----
#define	D_CALC_X1	D_TP_CALC_X1
#define	D_CALC_X2	D_TP_CALC_X2
#define	D_CALC_Y1	D_TP_CALC_Y1
#define	D_CALC_Y2	D_TP_CALC_Y2

//----- input id -----
enum tagInputID {
	D_OP_INPUT_ID_YEAR,
	D_OP_INPUT_ID_MONTH,
	D_OP_INPUT_ID_DAY,
	D_OP_INPUT_ID_HOUR,
	D_OP_INPUT_ID_MINUTE,
	D_OP_INPUT_ID_MAX
};

#define	D_OP_INPUT_SLIDE	0
#define	D_OP_INPUT_TAP		1
#define	D_OP_INPUT_PREV		0
#define	D_OP_INPUT_NEXT		1

//======================================================================
//	macro
//======================================================================
#define	mcrSetSE(no)				(m_R_SoundNo = (no))
#define	mcrPlaySE(no)				mf_F_PlayDrm(D_ASD_SE_CH, (no))
#define	mcrStopSE()					mf_F_StopDrm(D_ASD_SE_CH)
#define	mcrCheckSE()				mf_F_CheckDrm(D_ASD_SE_CH)
#define	mcrSpDisp(no,idx,x,y)		mf_F_SpriteUpdate(no, (INT32U)cAP09SpAddr[idx], (INT16U)cAP09SpIndex[idx], x, y)
#define	mcrSpClear(no)				mf_F_SpriteClear(no)
#define	mcrStrDisp(str,x,y)			mf_F_FontSetString((INT16U *)str, x, y)
#define	mcrStrDisp1(str,x,y)		mf_F_FontSetString1((INT16U *)str, x, y)
#define	mcrStrDisp2(str,x,y,n,k)	mf_F_FontSetString2((INT16U *)str, x, y, n, k)

#define	mcrSetWorkAddr(p)			((p) = (POPWORK)m_R_WorkAddr)
#define	mcrFadeIn(next)				mf_F_SetFadeInOka(D_FADE_DEFAULT_FRAME, D_FADE_DEFAULT_VALUE, next, D_OPENING_FADE_IN_SEQ)
#define	mcrFadeOut(next)			mf_F_SetFadeOutOka(D_FADE_DEFAULT_FRAME, D_FADE_DEFAULT_VALUE, next, D_OPENING_FADE_OUT_SEQ)
#define	mcrBackBtnOnSet()			mf_F_InfoBarDispSet(D_IB_ICON_BACK, D_IB_DISP_ID_ON, 0)
#define	mcrBackBtnOffSet()			mf_F_InfoBarDispSet(D_IB_ICON_BACK, D_IB_DISP_ID_OFF, 0)
#define	mcrBackBtnHalfSet()			mf_F_InfoBarDispSet(D_IB_ICON_BACK, D_IB_DISP_ID_HALF, 0)

#define	mcrPNSP(n)					(PNOTICESPRITE)&cAP09NoticeSprite[n]

//======================================================================
//	work
//======================================================================
typedef struct tagOpWork {
	INT8S	notice;
	INT8S	cur;
	INT8S	cnt;
	INT8U	kind;
	INT8U	finish;

	INT16S	anim_x;
	INT16S	anim_timer;
	INT16U	wa_kind;
	INT16U	wa_cnt;
	INT16U	ave_x[2];
	INT16U	ave_y[2];
	INT16U	name[D_PROF_NAME_LEN+1];

	INT32U	sp_addr;

	COMMONBTN	btn;
	TPADC		tp_his[6];
	TPCOMMON	tp;
	TIME_T		tm_now;
	TIME_T		tm_temp;
} OPWORK, *POPWORK;

//======================================================================
//	※一時的にAP09と定義が被るのでexternで※
//======================================================================
extern const INT32U cAP09SpAddr[];
extern const INT16U cAP09SpIndex[];
extern const INT32U cAP09InfoBarTbl[];
extern const NOTICESPRITE cAP09NoticeSprite[];

#endif
