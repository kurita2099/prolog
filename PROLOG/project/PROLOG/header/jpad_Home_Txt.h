#ifndef	__JPAD_HOME2_HEADER__
#define	__JPAD_HOME2_HEADER__
//======================================================================
// [共通ヘッダ] ホーム画面
//======================================================================
#include	"jpad_header.h"
//======================================================================
// [extern宣言]
//======================================================================
extern const INT16U** const T_HM_TLP_TXT_LIST[];
extern const INT16U* const T_HM_TLP_TXT_2200_0500[];
extern const INT16U* const T_HM_TLP_TXT_0500_0900[];
extern const INT16U* const T_HM_TLP_TXT_0900_1100[];
extern const INT16U* const T_HM_TLP_TXT_1100_1300[];
extern const INT16U* const T_HM_TLP_TXT_1300_1800[];
extern const INT16U* const T_HM_TLP_TXT_1800_2100[];
extern const INT16U* const T_HM_TLP_TXT_2100_2200[];

extern const INT16U T_HM_TLP_TXT_2200_0500_001[];
extern const INT16U T_HM_TLP_TXT_2200_0500_002[];
extern const INT16U T_HM_TLP_TXT_2200_0500_003[];
extern const INT16U T_HM_TLP_TXT_2200_0500_004[];
extern const INT16U T_HM_TLP_TXT_2200_0500_005[];
extern const INT16U T_HM_TLP_TXT_2200_0500_006[];
extern const INT16U T_HM_TLP_TXT_2200_0500_007[];
extern const INT16U T_HM_TLP_TXT_2200_0500_008[];
extern const INT16U T_HM_TLP_TXT_2200_0500_009[];
extern const INT16U T_HM_TLP_TXT_0500_0900_001[];
extern const INT16U T_HM_TLP_TXT_0500_0900_002[];
extern const INT16U T_HM_TLP_TXT_0500_0900_003[];
extern const INT16U T_HM_TLP_TXT_0500_0900_004[];
extern const INT16U T_HM_TLP_TXT_0500_0900_005[];
extern const INT16U T_HM_TLP_TXT_0500_0900_006[];
extern const INT16U T_HM_TLP_TXT_0500_0900_007[];
extern const INT16U T_HM_TLP_TXT_0500_0900_008[];
extern const INT16U T_HM_TLP_TXT_0500_0900_009[];
extern const INT16U T_HM_TLP_TXT_0500_0900_010[];
extern const INT16U T_HM_TLP_TXT_0500_0900_011[];
extern const INT16U T_HM_TLP_TXT_0500_0900_012[];
extern const INT16U T_HM_TLP_TXT_0500_0900_013[];
extern const INT16U T_HM_TLP_TXT_0500_0900_014[];
extern const INT16U T_HM_TLP_TXT_0500_0900_015[];
extern const INT16U T_HM_TLP_TXT_0500_0900_016[];
extern const INT16U T_HM_TLP_TXT_0500_0900_017[];
extern const INT16U T_HM_TLP_TXT_0500_0900_018[];
extern const INT16U T_HM_TLP_TXT_0500_0900_019[];
extern const INT16U T_HM_TLP_TXT_0500_0900_020[];
extern const INT16U T_HM_TLP_TXT_0500_0900_021[];
extern const INT16U T_HM_TLP_TXT_0500_0900_022[];
extern const INT16U T_HM_TLP_TXT_0500_0900_023[];
extern const INT16U T_HM_TLP_TXT_0500_0900_024[];
extern const INT16U T_HM_TLP_TXT_0500_0900_025[];
extern const INT16U T_HM_TLP_TXT_0500_0900_026[];
extern const INT16U T_HM_TLP_TXT_0500_0900_027[];
extern const INT16U T_HM_TLP_TXT_0500_0900_028[];
extern const INT16U T_HM_TLP_TXT_0500_0900_029[];
extern const INT16U T_HM_TLP_TXT_0500_0900_030[];
extern const INT16U T_HM_TLP_TXT_0900_1100_001[];
extern const INT16U T_HM_TLP_TXT_0900_1100_002[];
extern const INT16U T_HM_TLP_TXT_0900_1100_003[];
extern const INT16U T_HM_TLP_TXT_0900_1100_004[];
extern const INT16U T_HM_TLP_TXT_0900_1100_005[];
extern const INT16U T_HM_TLP_TXT_0900_1100_006[];
extern const INT16U T_HM_TLP_TXT_0900_1100_007[];
extern const INT16U T_HM_TLP_TXT_0900_1100_008[];
extern const INT16U T_HM_TLP_TXT_0900_1100_009[];
extern const INT16U T_HM_TLP_TXT_0900_1100_010[];
extern const INT16U T_HM_TLP_TXT_0900_1100_011[];
extern const INT16U T_HM_TLP_TXT_0900_1100_012[];
extern const INT16U T_HM_TLP_TXT_0900_1100_013[];
extern const INT16U T_HM_TLP_TXT_0900_1100_014[];
extern const INT16U T_HM_TLP_TXT_0900_1100_015[];
extern const INT16U T_HM_TLP_TXT_0900_1100_016[];
extern const INT16U T_HM_TLP_TXT_0900_1100_017[];
extern const INT16U T_HM_TLP_TXT_0900_1100_018[];
extern const INT16U T_HM_TLP_TXT_0900_1100_019[];
extern const INT16U T_HM_TLP_TXT_0900_1100_020[];
extern const INT16U T_HM_TLP_TXT_0900_1100_021[];
extern const INT16U T_HM_TLP_TXT_0900_1100_022[];
extern const INT16U T_HM_TLP_TXT_0900_1100_023[];
extern const INT16U T_HM_TLP_TXT_0900_1100_024[];
extern const INT16U T_HM_TLP_TXT_0900_1100_025[];
extern const INT16U T_HM_TLP_TXT_0900_1100_026[];
extern const INT16U T_HM_TLP_TXT_0900_1100_027[];
extern const INT16U T_HM_TLP_TXT_0900_1100_028[];
extern const INT16U T_HM_TLP_TXT_0900_1100_029[];
extern const INT16U T_HM_TLP_TXT_0900_1100_030[];
extern const INT16U T_HM_TLP_TXT_1100_1300_001[];
extern const INT16U T_HM_TLP_TXT_1100_1300_002[];
extern const INT16U T_HM_TLP_TXT_1100_1300_003[];
extern const INT16U T_HM_TLP_TXT_1100_1300_004[];
extern const INT16U T_HM_TLP_TXT_1100_1300_005[];
extern const INT16U T_HM_TLP_TXT_1100_1300_006[];
extern const INT16U T_HM_TLP_TXT_1100_1300_007[];
extern const INT16U T_HM_TLP_TXT_1100_1300_008[];
extern const INT16U T_HM_TLP_TXT_1100_1300_009[];
extern const INT16U T_HM_TLP_TXT_1100_1300_010[];
extern const INT16U T_HM_TLP_TXT_1100_1300_011[];
extern const INT16U T_HM_TLP_TXT_1100_1300_012[];
extern const INT16U T_HM_TLP_TXT_1100_1300_013[];
extern const INT16U T_HM_TLP_TXT_1100_1300_014[];
extern const INT16U T_HM_TLP_TXT_1100_1300_015[];
extern const INT16U T_HM_TLP_TXT_1100_1300_016[];
extern const INT16U T_HM_TLP_TXT_1100_1300_017[];
extern const INT16U T_HM_TLP_TXT_1100_1300_018[];
extern const INT16U T_HM_TLP_TXT_1100_1300_019[];
extern const INT16U T_HM_TLP_TXT_1100_1300_020[];
extern const INT16U T_HM_TLP_TXT_1100_1300_021[];
extern const INT16U T_HM_TLP_TXT_1100_1300_022[];
extern const INT16U T_HM_TLP_TXT_1100_1300_023[];
extern const INT16U T_HM_TLP_TXT_1100_1300_024[];
extern const INT16U T_HM_TLP_TXT_1100_1300_025[];
extern const INT16U T_HM_TLP_TXT_1100_1300_026[];
extern const INT16U T_HM_TLP_TXT_1100_1300_027[];
extern const INT16U T_HM_TLP_TXT_1100_1300_028[];
extern const INT16U T_HM_TLP_TXT_1100_1300_029[];
extern const INT16U T_HM_TLP_TXT_1100_1300_030[];
extern const INT16U T_HM_TLP_TXT_1300_1800_001[];
extern const INT16U T_HM_TLP_TXT_1300_1800_002[];
extern const INT16U T_HM_TLP_TXT_1300_1800_003[];
extern const INT16U T_HM_TLP_TXT_1300_1800_004[];
extern const INT16U T_HM_TLP_TXT_1300_1800_005[];
extern const INT16U T_HM_TLP_TXT_1300_1800_006[];
extern const INT16U T_HM_TLP_TXT_1300_1800_007[];
extern const INT16U T_HM_TLP_TXT_1300_1800_008[];
extern const INT16U T_HM_TLP_TXT_1300_1800_009[];
extern const INT16U T_HM_TLP_TXT_1300_1800_010[];
extern const INT16U T_HM_TLP_TXT_1300_1800_011[];
extern const INT16U T_HM_TLP_TXT_1300_1800_012[];
extern const INT16U T_HM_TLP_TXT_1300_1800_013[];
extern const INT16U T_HM_TLP_TXT_1300_1800_014[];
extern const INT16U T_HM_TLP_TXT_1300_1800_015[];
extern const INT16U T_HM_TLP_TXT_1300_1800_016[];
extern const INT16U T_HM_TLP_TXT_1300_1800_017[];
extern const INT16U T_HM_TLP_TXT_1300_1800_018[];
extern const INT16U T_HM_TLP_TXT_1300_1800_019[];
extern const INT16U T_HM_TLP_TXT_1300_1800_020[];
extern const INT16U T_HM_TLP_TXT_1300_1800_021[];
extern const INT16U T_HM_TLP_TXT_1300_1800_022[];
extern const INT16U T_HM_TLP_TXT_1300_1800_023[];
extern const INT16U T_HM_TLP_TXT_1300_1800_024[];
extern const INT16U T_HM_TLP_TXT_1300_1800_025[];
extern const INT16U T_HM_TLP_TXT_1300_1800_026[];
extern const INT16U T_HM_TLP_TXT_1300_1800_027[];
extern const INT16U T_HM_TLP_TXT_1300_1800_028[];
extern const INT16U T_HM_TLP_TXT_1300_1800_029[];
extern const INT16U T_HM_TLP_TXT_1300_1800_030[];
extern const INT16U T_HM_TLP_TXT_1800_2100_001[];
extern const INT16U T_HM_TLP_TXT_1800_2100_002[];
extern const INT16U T_HM_TLP_TXT_1800_2100_003[];
extern const INT16U T_HM_TLP_TXT_1800_2100_004[];
extern const INT16U T_HM_TLP_TXT_1800_2100_005[];
extern const INT16U T_HM_TLP_TXT_1800_2100_006[];
extern const INT16U T_HM_TLP_TXT_1800_2100_007[];
extern const INT16U T_HM_TLP_TXT_1800_2100_008[];
extern const INT16U T_HM_TLP_TXT_1800_2100_009[];
extern const INT16U T_HM_TLP_TXT_1800_2100_010[];
extern const INT16U T_HM_TLP_TXT_1800_2100_011[];
extern const INT16U T_HM_TLP_TXT_1800_2100_012[];
extern const INT16U T_HM_TLP_TXT_1800_2100_013[];
extern const INT16U T_HM_TLP_TXT_1800_2100_014[];
extern const INT16U T_HM_TLP_TXT_1800_2100_015[];
extern const INT16U T_HM_TLP_TXT_1800_2100_016[];
extern const INT16U T_HM_TLP_TXT_1800_2100_017[];
extern const INT16U T_HM_TLP_TXT_1800_2100_018[];
extern const INT16U T_HM_TLP_TXT_1800_2100_019[];
extern const INT16U T_HM_TLP_TXT_1800_2100_020[];
extern const INT16U T_HM_TLP_TXT_1800_2100_021[];
extern const INT16U T_HM_TLP_TXT_1800_2100_022[];
extern const INT16U T_HM_TLP_TXT_1800_2100_023[];
extern const INT16U T_HM_TLP_TXT_1800_2100_024[];
extern const INT16U T_HM_TLP_TXT_1800_2100_025[];
extern const INT16U T_HM_TLP_TXT_1800_2100_026[];
extern const INT16U T_HM_TLP_TXT_1800_2100_027[];
extern const INT16U T_HM_TLP_TXT_1800_2100_028[];
extern const INT16U T_HM_TLP_TXT_1800_2100_029[];
extern const INT16U T_HM_TLP_TXT_1800_2100_030[];
extern const INT16U T_HM_TLP_TXT_2100_2200_001[];
extern const INT16U T_HM_TLP_TXT_2100_2200_002[];
extern const INT16U T_HM_TLP_TXT_2100_2200_003[];
extern const INT16U T_HM_TLP_TXT_2100_2200_004[];
extern const INT16U T_HM_TLP_TXT_2100_2200_005[];
extern const INT16U T_HM_TLP_TXT_2100_2200_006[];
extern const INT16U T_HM_TLP_TXT_2100_2200_007[];
extern const INT16U T_HM_TLP_TXT_2100_2200_008[];
extern const INT16U T_HM_TLP_TXT_2100_2200_009[];
extern const INT16U T_HM_TLP_TXT_2100_2200_010[];
extern const INT16U T_HM_TLP_TXT_2100_2200_011[];
extern const INT16U T_HM_TLP_TXT_2100_2200_012[];
extern const INT16U T_HM_TLP_TXT_2100_2200_013[];
extern const INT16U T_HM_TLP_TXT_2100_2200_014[];
extern const INT16U T_HM_TLP_TXT_2100_2200_015[];
extern const INT16U T_HM_TLP_TXT_2100_2200_016[];
extern const INT16U T_HM_TLP_TXT_2100_2200_017[];
extern const INT16U T_HM_TLP_TXT_2100_2200_018[];
extern const INT16U T_HM_TLP_TXT_2100_2200_019[];
extern const INT16U T_HM_TLP_TXT_2100_2200_020[];
extern const INT16U T_HM_TLP_TXT_2100_2200_021[];
extern const INT16U T_HM_TLP_TXT_2100_2200_022[];
extern const INT16U T_HM_TLP_TXT_2100_2200_023[];
extern const INT16U T_HM_TLP_TXT_2100_2200_024[];
extern const INT16U T_HM_TLP_TXT_2100_2200_025[];
extern const INT16U T_HM_TLP_TXT_2100_2200_026[];
extern const INT16U T_HM_TLP_TXT_2100_2200_027[];
extern const INT16U T_HM_TLP_TXT_2100_2200_028[];
extern const INT16U T_HM_TLP_TXT_2100_2200_029[];
extern const INT16U T_HM_TLP_TXT_2100_2200_030[];
#endif
