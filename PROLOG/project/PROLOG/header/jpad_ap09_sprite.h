#ifndef	__JPAD_AP09_SPRITE_HEADER__
#define	__JPAD_AP09_SPRITE_HEADER__
//======================================================================
//	sprite
//======================================================================
#define	D_SP_PAL1_ADDR		(INT32U)_SP_AP09_Palette1
#define	D_SP_PAL3_ADDR		(INT32U)_SP_AP09_Palette3

//----- sp index -----
enum tagAP09SPINDEX {
	D_SP_INFO_001,
	D_SP_INFO_002,
	D_SP_INFO_003,
	D_SP_INFO_004,
	D_SP_INFO_005,
	D_SP_INFO_006,
	D_SP_INFO_007,
	D_SP_INFO_008,
	D_SP_INFO_009,
	D_SP_INFO_010,
	D_SP_INFO_011,
	D_SP_INFO_012,
	D_SP_INFO_013,
	D_SP_INFO_014,
	D_SP_INFO_015,
	D_SP_INFO_016,
	D_SP_INFO_017,
	D_SP_INFO_018,
	D_SP_INFO_019,
	D_SP_INFO_020,
	D_SP_INFO_021,
	D_SP_INFO_022,
	D_SP_INFO_045,
	D_SP_SY001,
	D_SP_SY002,
	D_SP_SY003,
	D_SP_SY004,
	D_SP_SY005,
	D_SP_SY006,
	D_SP_SY007,
	D_SP_SY008,
	D_SP_SY009,
	D_SP_SY010,
	D_SP_SY011,
	D_SP_AP09_001,
	D_SP_AP09_002,
	D_SP_AP09_003,
	D_SP_AP09_004,
	D_SP_AP09_005,
	D_SP_AP09_006,
	D_SP_AP09_007,
	D_SP_AP09_011,
	D_SP_AP09_012,
	D_SP_AP09_015,
	D_SP_AP09_016,
	D_SP_AP09_017,
	D_SP_AP09_018,
	D_SP_AP09_019,
	D_SP_AP09_022,
	D_SP_AP09_023,
	D_SP_AP09_026,
	D_SP_AP09_027,
	D_SP_AP09_028,
	D_SP_AP09_029,
	D_SP_AP09_030,
	D_SP_AP09_031,
	D_SP_AP09_032,
	D_SP_AP09_033,
	D_SP_AP09_034,
	D_SP_AP09_035,
	D_SP_AP09_036,
	D_SP_AP09_037,
	D_SP_AP09_038,
	D_SP_AP09_039,
	D_SP_AP09_040,
	D_SP_AP09_041,
	D_SP_AP09_042,
	D_SP_AP09_043,
	D_SP_AP09_044,
	D_SP_AP09_045,
	D_SP_AP09_046,
	D_SP_AP09_047,
	D_SP_AP09_048,
	D_SP_AP09_049,
	D_SP_AP09_050,
	D_SP_AP09_051,
	D_SP_AP09_052,
	D_SP_AP09_053,
	D_SP_AP09_054,
	D_SP_AP09_055,
	D_SP_AP09_056,
	D_SP_AP09_057,
	D_SP_AP09_060,
	D_SP_AP09_061,
	D_SP_AP09_062,
	D_SP_AP09_063,
	D_SP_AP09_064,
	D_SP_AP09_065,
	D_SP_AP09_066,
	D_SP_AP09_067,
	D_SP_AP09_068,
	D_SP_AP09_069,
	D_SP_AP09_070,
	D_SP_AP09_071,
	D_SP_AP09_072,
	D_SP_AP09_073,
	D_SP_AP09_074,
	D_SP_AP09_075,
	D_SP_AP09_076,
	D_SP_AP09_077,
	D_SP_AP09_082,
	D_SP_AP09_083,
	D_SP_AP09_084,
	D_SP_AP09_085,
	D_SP_AP09_086,
	D_SP_AP09_087,
	D_SP_AP09_088,
	D_SP_AP09_089,
	D_SP_AP09_090,
	D_SP_AP09_091,
	D_SP_AP09_092,
	D_SP_AP09_093,
	D_SP_AP09_094,
	D_SP_AP09_095,
	D_SP_AP09_096,
	D_SP_AP09_097,
	D_SP_AP09_098,
	D_SP_AP09_099,
	D_SP_AP09_100,
	D_SP_AP09_101,
	D_SP_AP09_102,
	D_SP_AP09_103,
	D_SP_AP09_104,
	D_SP_AP09_105,
	D_SP_AP09_106,
	D_SP_AP09_107,
	D_SP_AP09_108,
	D_SP_AP09_109,
	D_SP_AP09_110,
	D_SP_AP09_111,
	D_SP_AP09_112,
	D_SP_AP09_113,
	D_SP_AP09_114,
	D_SP_AP09_115,
	D_SP_AP09_116,
	D_SP_AP09_117,
	D_SP_AP09_118,
	D_SP_AP09_119,
	D_SP_AP09_120,
	D_SP_AP09_121,
	D_SP_AP09_122,
	D_SP_AP09_123,
	D_SP_AP09_124,
	D_SP_AP09_125,
	D_SP_AP09_126,
	D_SP_AP09_127,
	D_SP_AP09_128,
	D_SP_AP09_129,
	D_SP_AP19_073,
	D_SP_AP19_074,
	D_SP_AP19_075,
	D_SP_AP19_076,
};
//======================================================================
//======================================================================
#define	D_SP_NO_NOTICE		128
//----- op stay -----
#define	D_SP_NO_O_LABEL		0
#define	D_SP_NO_O_YMD		(D_SP_NO_O_LABEL+1)
#define	D_SP_NO_O_HM		(D_SP_NO_O_YMD+3)
#define	D_SP_NO_O_NAME		(D_SP_NO_O_HM+3)
#define	D_SP_NO_O_BTN_W		(D_SP_NO_O_NAME+3)
#define	D_SP_NO_O_BTN_C		(D_SP_NO_O_BTN_W+2)
#define	D_SP_NO_O_BTN_B		(D_SP_NO_O_BTN_C+1)
#define	D_SP_NO_O_BTN		(D_SP_NO_O_BTN_B+2)
#define	D_SP_NO_O_BTN_M		(D_SP_NO_O_BTN+1)
//----- YMDHM -----
#define	D_SP_NO_OY_LABEL		0
#define	D_SP_NO_OY_OK			(D_SP_NO_OY_LABEL+1)
#define	D_SP_NO_OY_YMD			(D_SP_NO_OY_OK+1)
#define	D_SP_NO_OY_HM			(D_SP_NO_OY_YMD+3)
#define	D_SP_NO_OY_BG			(D_SP_NO_OY_HM+3)
#define	D_SP_NO_OY_BTN			(D_SP_NO_OY_BG+1)

//----- menu -----
#define	D_SP_NO_MN_ICON			0

//----- volume -----
#define	D_SP_NO_VOL_LABEL		0
#define	D_SP_NO_VOL_BG			(D_SP_NO_VOL_LABEL+1)
#define	D_SP_NO_VOL_ICON		(D_SP_NO_VOL_BG+1)
#define	D_SP_NO_VOL_BTN			(D_SP_NO_VOL_ICON+1)

//----- backlight -----
#define	D_SP_NO_BL_LABEL		0
#define	D_SP_NO_BL_BG			(D_SP_NO_BL_LABEL+3)
#define	D_SP_NO_BL_ICON			(D_SP_NO_BL_BG+3)
#define	D_SP_NO_BL_CHK			(D_SP_NO_BL_ICON+3)
#define	D_SP_NO_BL_BTN			(D_SP_NO_BL_CHK+3)

//----- standby -----
#define	D_SP_NO_SB_LABEL		0
#define	D_SP_NO_SB_BG			(D_SP_NO_SB_LABEL+3)
#define	D_SP_NO_SB_ICON			(D_SP_NO_SB_BG+3)
#define	D_SP_NO_SB_CHK			(D_SP_NO_SB_ICON+6)
#define	D_SP_NO_SB_BTN			(D_SP_NO_SB_CHK+3)

//----- home -----
#define	D_SP_NO_HM_LABEL		0
#define	D_SP_NO_HM_BG			(D_SP_NO_HM_LABEL+3)
#define	D_SP_NO_HM_ICON			(D_SP_NO_HM_BG+3)
#define	D_SP_NO_HM_CHK			(D_SP_NO_HM_ICON+6)
#define	D_SP_NO_HM_BTN			(D_SP_NO_HM_CHK+3)

//----- sd clear -----
#define	D_SP_NO_SD_LABEL		0

//----- calibration -----
#define	D_SP_NO_CAL_MARK		0

//----- capacity -----
#define	D_SP_NO_CP_LABEL		0
#define	D_SP_NO_CP_ITEM			(D_SP_NO_CP_LABEL+3)
#define	D_SP_NO_CP_BAR			(D_SP_NO_CP_ITEM+6)
#define	D_SP_NO_CP_NUM			(D_SP_NO_CP_BAR+6)

#endif
