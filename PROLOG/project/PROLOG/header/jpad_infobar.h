#ifndef	__JPAD_INFOBAR_HEADER__
#define	__JPAD_INFOBAR_HEADER__
//======================================================================
//======================================================================
//----- flag -----
#define	D_IB_FLAG_ENABLE	0x01			// 0=無効(非表示), 1=有効(表示)
#define	D_IB_FLAG_DIR		0x02			// 0=縦, 1=横

//----- update -----
#define	D_IB_UPD_BG			0x0001
#define	D_IB_UPD_BACK		0x0002
#define	D_IB_UPD_SD			0x0004
#define	D_IB_UPD_USB		0x0008
#define	D_IB_UPD_ALARM		0x0010
#define	D_IB_UPD_WIFI		0x0020
#define	D_IB_UPD_BATT		0x0040
#define	D_IB_UPD_CHARGE		0x0080
#define	D_IB_UPD_TOKEI		0x0100
#define	D_IB_UPD_DIR		0x4000
#define	D_IB_UPD_ENABLE		0x8000

//----- bmp -----
enum tagInfoBarBmpID {
	D_IB_BMP_BG,
	D_IB_BMP_USB,
	D_IB_BMP_BACK,
	D_IB_BMP_WIFI,
	D_IB_BMP_ALARM,
	D_IB_BMP_SD,
	D_IB_BMP_CHARGE,
	D_IB_BMP_BATT3,
	D_IB_BMP_BATT2,
	D_IB_BMP_BATT1,
	D_IB_BMP_BATT0,
	D_IB_BMP_NUM0,
	D_IB_BMP_NUM1,
	D_IB_BMP_NUM2,
	D_IB_BMP_NUM3,
	D_IB_BMP_NUM4,
	D_IB_BMP_NUM5,
	D_IB_BMP_NUM6,
	D_IB_BMP_NUM7,
	D_IB_BMP_NUM8,
	D_IB_BMP_NUM9,
	D_IB_BMP_COLON,
	D_IB_BMP_BATT4,
	D_IB_BMP_MAX
};

#define	D_IB_SP_NO_BASE		240
#define	D_IB_SP_NO_BG		(D_IB_SP_NO_BASE+0)
#define	D_IB_SP_NO_BACK		(D_IB_SP_NO_BASE+1)
#define	D_IB_SP_NO_SD		(D_IB_SP_NO_BASE+2)
#define	D_IB_SP_NO_USB		(D_IB_SP_NO_BASE+3)
#define	D_IB_SP_NO_ALARM	(D_IB_SP_NO_BASE+4)
#define	D_IB_SP_NO_WIFI		(D_IB_SP_NO_BASE+5)
#define	D_IB_SP_NO_BATT		(D_IB_SP_NO_BASE+6)
#define	D_IB_SP_NO_CHARGE	(D_IB_SP_NO_BASE+7)
#define	D_IB_SP_NO_TOKEI	(D_IB_SP_NO_BASE+8)
#define	D_IB_SP_NO_MAX		(D_IB_SP_NO_BASE+13)		// 253

//----- icon -----
enum tagInfoBarIconID {
	D_IB_ICON_BG,
	D_IB_ICON_BACK,
	D_IB_ICON_SD,
	D_IB_ICON_USB,
	D_IB_ICON_ALARM,
	D_IB_ICON_WIFI,
	D_IB_ICON_BATT,
	D_IB_ICON_CHARGE,
	D_IB_ICON_TOKEI,
	D_IB_ICON_MAX
};

enum tagInfoBarDispID {
	D_IB_DISP_ID_OFF,			// 非表示
	D_IB_DISP_ID_ON,			// 表示
	D_IB_DISP_ID_HALF,			// 半透明
	D_IB_DISP_ID_EXT,			// 特殊(任意の処理)
	D_IB_DISP_ID_MAX
};

#define	D_IB_DISABLE		0
#define	D_IB_ENABLE			1
#define	D_IB_DIR_TATE		0
#define	D_IB_DIR_YOKO		1

#define	D_IB_BACK_BLINK_TIME	D_TIME250

//------------------------------
#define	D_IB_BACK_TATE_X	4
#define	D_IB_BACK_TATE_Y	4
#define	D_IB_SD_TATE_X		32
#define	D_IB_SD_TATE_Y		4
#define	D_IB_USB_TATE_X		56
#define	D_IB_USB_TATE_Y		4
#define	D_IB_ALARM_TATE_X	80
#define	D_IB_ALARM_TATE_Y	4
#define	D_IB_TOKEI_TATE_X	104
#define	D_IB_TOKEI_TATE_Y	4
#define	D_IB_WIFI_TATE_X	176
#define	D_IB_WIFI_TATE_Y	4
#define	D_IB_CHARGE_TATE_X	200
#define	D_IB_CHARGE_TATE_Y	4
#define	D_IB_BATT_TATE_X	224
#define	D_IB_BATT_TATE_Y	4

//------------------------------
#define	D_IB_BACK_YOKO_X	4
#define	D_IB_BACK_YOKO_Y	4
#define	D_IB_SD_YOKO_X		44
#define	D_IB_SD_YOKO_Y		4
#define	D_IB_USB_YOKO_X		84
#define	D_IB_USB_YOKO_Y		4
#define	D_IB_ALARM_YOKO_X	176
#define	D_IB_ALARM_YOKO_Y	4
#define	D_IB_TOKEI_YOKO_X	204
#define	D_IB_TOKEI_YOKO_Y	4
#define	D_IB_WIFI_YOKO_X	368
#define	D_IB_WIFI_YOKO_Y	4
#define	D_IB_CHARGE_YOKO_X	408
#define	D_IB_CHARGE_YOKO_Y	4
#define	D_IB_BATT_YOKO_X	432
#define	D_IB_BATT_YOKO_Y	4

//------------------------------
#define	D_IB_BACK_TATE_SX		4
#define	D_IB_BACK_TATE_X_SIZE	24
#define	D_IB_BACK_TATE_SY		4
#define	D_IB_BACK_TATE_Y_SIZE	24
#define	D_IB_BACK_TATE_EX		(D_IB_BACK_TATE_SX+D_IB_BACK_TATE_X_SIZE)
#define	D_IB_BACK_TATE_EY		(D_IB_BACK_TATE_SY+D_IB_BACK_TATE_Y_SIZE)

#define	D_IB_BACK_YOKO_SX		244
#define	D_IB_BACK_YOKO_X_SIZE	24
#define	D_IB_BACK_YOKO_SY		4
#define	D_IB_BACK_YOKO_Y_SIZE	24
#define	D_IB_BACK_YOKO_EX		(D_IB_BACK_YOKO_SX+D_IB_BACK_YOKO_X_SIZE)
#define	D_IB_BACK_YOKO_EY		(D_IB_BACK_YOKO_SY+D_IB_BACK_YOKO_Y_SIZE)

//======================================================================
//======================================================================
//------------------------------
//	アイコン別情報(12byte)
//------------------------------
typedef struct tagInfoBarIcon {
	INT8U	disp_id;
	INT8U	anim_cnt;
	INT8U	anim_frame[2];
	void	(*pfUpdate)();
	void	(*pfExtFunc)();
} IBICON, *PIBICON;

//------------------------------
//	情報バー全体の情報(245+?byte)
//------------------------------
typedef struct tagInfoBar {
	INT8U	flag;
	INT8U	batt_lv;
	INT8U	charge;
	INT16U	update;
	INT16U	bmp_index[D_IB_BMP_MAX];
	INT32U	bmp_addr[D_IB_BMP_MAX];
	IBICON	icon[D_IB_ICON_MAX];
} INFOBAR, *PINFOBAR;

//======================================================================
//======================================================================
extern INFOBAR R_InfoBar;

//======================================================================
//======================================================================
extern void F_InfoBarInit(INT8U enable, INT8U dir, INT32U *tbl);
extern void F_InfoBarOnOffSet(INT8U enable);
extern void F_InfoBarDirSet(INT8U dir);
extern void F_InfoBarBmpSet(INT8U id, INT32U addr, INT16U index);
extern void F_InfoBarDispSet(INT8U icon_id, INT8U disp_id, INT8U frame);
extern void F_InfoBarUpdateFuncDefault(INT8U icon_id);
extern void F_InfoBarUpdateFuncSet(INT8U icon_id, void (*pFunc)());
extern void F_InfoBarExtFuncDefault(INT8U icon_id);
extern void F_InfoBarExtFuncSet(INT8U icon_id, void (*pFunc)());
extern void F_InfoBarUpdate();
extern INT8U F_InfoBarGetDispID(INT8U icon_id);

#endif
