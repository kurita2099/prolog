#ifndef	__JPAD_COMMON_S_HEADER__
#define	__JPAD_COMMON_S_HEADER__
//======================================================================
//======================================================================
//	範囲チェック構造体
typedef struct tagRANGE_RECTANGLE
{
	INT16S	id;
	INT16S	x;
	INT16S	y;
	INT16S	x_size;
	INT16S	y_size;
} RANGE_RECTANGLE, *LPRANGE_RECTANGLE;
//----------------------------------------------------------------------
//	スライド
#define		D_SLIDE_CHECK		8
#define		D_SLIDE_POINT		60
#define		D_SLIDE_POINT_DRAG	8
#define		D_SLIDE_UP			0x0001
#define		D_SLIDE_DOWN		0x0002
#define		D_SLIDE_LEFT		0x0004
#define		D_SLIDE_RIGHT		0x0008
#define		D_SLIDE_MOVE		0x0010
#define		D_SLIDE_DRAG		0x0020
#define		D_SLIDE_CONTINUED	0x0100	//スライド継続
#define		D_SLIDE_DRAG_CONTINUED	0x0200	//ドラッグ継続

#define		D_SLIDE_YOKO_UP		0x0008	//横画面用
#define		D_SLIDE_YOKO_DOWN	0x0004	//横画面用
#define		D_SLIDE_YOKO_LEFT	0x0002	//横画面用
#define		D_SLIDE_YOKO_RIGHT	0x0001	//横画面用

typedef struct tagSLIDE
{
	INT16U	count;
	INT16U	dir;

	INT16S	sx;				//始点
	INT16S	sy;				//始点

	INT16S	ex;				//終点
	INT16S	ey;				//終点

	INT16S	dir_x;			//直前の方向性
	INT16S	dir_y;			//直前の方向性

	INT16S	x[D_SLIDE_CHECK];
	INT16S	y[D_SLIDE_CHECK];
} SLIDE, *LPSLIDE;		//Size(12*4)
//----------------------------------------------------------------------
//	スプライト構造体
typedef struct tagCOMMON_S_SPINFO
{
	INT32U addr;
	INT8U ofs;
} COMMON_S_SPINFO, *LPCOMMON_S_SPINFO;
//----------------------------------------------------------------------
//	ボタン処理
#define		D_BUTTON_CONTINUE	0x04000

//======================================================================
//======================================================================
extern INT16S	F_CommonS_RangeCheck(RANGE_RECTANGLE*,INT16S, INT16S );
extern INT16U	F_CommonS_Slide(INT16U,SLIDE*);
extern INT16U	F_CommonS_GetRand(INT16U);

extern void		F_CommonS_BtnCheckInit(RANGE_RECTANGLE*);
extern INT16S	F_CommonS_BtnCheck(void);

#endif
