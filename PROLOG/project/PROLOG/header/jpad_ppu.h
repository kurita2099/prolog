#ifndef	__JPAD_PPU_HEADER__
#define	__JPAD_PPU_HEADER__
//======================================================================
//======================================================================
#define	D_JPAD_LCM_WIDTH		480
#define	D_JPAD_LCM_HEIGHT		272
#define	D_JPAD_PPU_RESOLUTION	C_TFT_RESOLUTION_480X272

//------------------------------
#define	D_PPU_DRV_FRAME_NUM		2

#define	D_FADE_NONE		0
#define	D_FADE_OUT		1
#define	D_FADE_IN		2

#define	D_FADE_DEFAULT_FRAME	1
#define	D_FADE_DEFAULT_VALUE	32
#define	D_FADE_DEFAULT_DX		D_FADE_DEFAULT_VALUE

#define	D_TEXT_LAYER_TATE	0
#define	D_TEXT_LAYER_YOKO	1

#define	D_TEXT_BMP_RGB565	0
#define	D_TEXT_BMP_RGB555	1
#define	D_TEXT_BMP_ARGB		2

//======================================================================
//======================================================================
//------------------------------
//	フェード制御
//------------------------------
typedef struct tagFadeCtrl {
	INT8U	flag;
	INT8U	frame[2];
	INT8U	value;
	INT16U	now;
} FADECTRL, *PFADECTRL;
//------------------------------
//	TEXT Layer 管理
//------------------------------
typedef struct tagTextLayer {
	INT16U	char_x;
	INT16U	char_y;
	INT32U	num_array_addr;
	INT32U	att_array_addr;
} TEXTLAYER, *PTEXTLAYER;
//------------------------------
//	TEXT 画像情報
//------------------------------
typedef struct tagTextInfo {
	INT16U	width;
	INT16U	height;
	INT16U	pal_no;
	INT16U	offset;
	INT32U	index_addr;
} TEXTINFO, *PTEXTINFO;

//======================================================================
//======================================================================
extern INT8U R_DispFlag;
extern INT8U R_FontDecoAnim;

extern INT32U R_PPU_FrameAddr[D_PPU_DRV_FRAME_NUM];
extern INT32U R_UnpackAddr[4];
extern INT32U R_TextNumArrayAddr[4];

extern FADECTRL R_FadeCtrl;
extern TEXTLAYER R_TextLayer[4];

//======================================================================
//======================================================================
extern void F_MyPPUInit();
extern void F_SpriteInit(INT32U pal0, INT32U pal1, INT32U cell);
extern void F_SpriteUpdate(INT16U no, INT32U sp, INT16U idx, INT16S px, INT16S py);
extern void F_SpriteUpdate1(INT16U no, INT32U sp, INT16U idx, INT16S px, INT16S py);
extern void F_SpriteRotateSet(INT16U no, INT16U rotate);
extern void F_SpriteZoomSet(INT16U no, INT16U zoom);
extern void F_SpriteDepthSet(INT16U no, INT16U depth);
extern void F_SpriteBlendSet(INT16U no, INT16U blend);
extern void F_SpriteMosaicSet(INT16U no, INT16U mosaic);
extern void F_SpriteFlipSet(INT16U no, INT16U flip);
extern void F_SpritePaletteSet(INT16U no, INT16U palette);
extern void F_SpriteClear(INT16U no);
extern void F_SpriteAllClear();
extern void F_SpriteEnable();
extern void F_SpriteDisable();
extern void F_PPU_DispFlagOn();
extern void F_PPU_UpdateCheck();
extern INT8U F_PPU_GetBusy();
extern void F_FadeInit(INT8U flag, INT8U frame, INT8U value);
extern void F_FadeInitSetDef(INT8U flag, INT8U frame, INT8U value, INT8U def);
extern void F_FadeUpdate();
extern INT8U F_GetFadeFlag();
extern INT8U F_GetFadeNow();
extern void F_SetFadeInOka(INT8U frame, INT8U value, INT16U next, INT16U seq);
extern void F_SetFadeOutOka(INT8U frame, INT8U value, INT16U next, INT16U seq);
extern void F_FadeSetDirect(INT8U val);

extern void F_TextLayerInit(INT32U text, INT32U pal, INT32U cell, INT16U char_x, INT16U char_y);
extern void F_TextLayerInit64(INT32U text, INT32U pal, INT32U cell, INT16U char_x, INT16U char_y);
extern void F_TextPutChar(INT32U text, INT16S px, INT16S py, PTEXTINFO pInfo, INT8U dir);
extern void F_TextFillCell(INT32U text, INT16S px, INT16S py, INT16U widht, INT16U height, INT16U cell, INT16U pal);
extern void F_TextPutBlock(INT32U text, INT16S px, INT16S py, INT16U bx_st, INT16U by_st, INT16U bx_size, INT16U by_size, PTEXTINFO pInfo, INT8U dir);
extern void F_TextEnable(INT32U text);
extern void F_TextDisable(INT32U text);
extern void F_TextPosSet(INT32U text, INT16U px, INT16U py);
extern void F_TextPosGet(INT32U text, INT16U *px, INT16U *py);
extern void F_TextDepthSet(INT32U text, INT32U depth);

extern void F_TextBmpInit(INT32U text);
extern void F_TextBmpUninit(INT32U text);
extern void F_TextBmpSet(INT32U text, INT32U addr);
extern void F_TextBmpKindSet(INT32U text, INT8U kind);
extern void F_TextBmpMiscClear();

extern void F_FontInit();
extern void F_FontClear();
extern void F_FontAllClear();
extern void F_FontSet(INT16U no, INT16S px, INT16S py);
extern void F_FontSetString(INT16U *str, INT16S px, INT16S py);
extern void F_FontSetString1(INT16U *str, INT16S px, INT16S py);
extern void F_FontSetString2(INT16U *str, INT16S px, INT16S py, INT16U num, INT16U kaigyo);
extern void F_FontSet1(INT16U no, INT16S px, INT16S py);
extern void F_FontSet1String(INT16U *str, INT16S px, INT16S py);
extern void F_FontSet1String1(INT16U *str, INT16S px, INT16S py);
extern void F_FontSet1String2(INT16U *str, INT16S px, INT16S py, INT16U num, INT16U kaigyo);
extern void F_FontSetDeco(INT16U no, INT16S px, INT16S py, INT8U anim);
extern void F_FontSetDeco1(INT16U no, INT16S px, INT16S py);

extern void F_FontDecoAnim();
extern INT8U F_FontDecoAnimCheck();
extern INT8U F_GetFontDecoAnim();

extern INT32U F_MemoryUnpack(INT8U *dst, INT8U *src);
extern INT32U F_CellDataUnpack(INT8U *name, INT8U index);
extern INT32U F_CellDataUnpack1(INT8U *src, INT8U index);
extern INT32U F_CellDataUnpackSD(char *path, INT8U index);
extern void F_ReleaseUnpack(INT8U index);
extern void F_ReleaseAllUnpack();

extern void F_DispAllClear();

#endif
