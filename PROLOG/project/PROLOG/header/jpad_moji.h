#ifndef	__JPAD_MOJI_HEADER__
#define	__JPAD_MOJI_HEADER__
//======================================================================
//======================================================================
//----- moji input seq -----
#define	D_MOJI_INPUT_INPUT_SEQ		0
#define	D_MOJI_INPUT_WAIT_SEQ		1
#define	D_MOJI_INPUT_NOTICE_SEQ		2

//----- input buffer -----
#define	D_MOJI_INPUT_BUFF_MAX		104
#define	D_MOJI_INPUT_LINE_MOJI		13
#define	D_MOJI_INPUT_LINE_MAX		6
#define	D_MOJI_INPUT_DISP_MAX		(D_MOJI_INPUT_LINE_MOJI * D_MOJI_INPUT_LINE_MAX)
#define	D_MOJI_INPUT_DISP_SX		32
#define	D_MOJI_INPUT_DISP_SY		72

//----- mode -----
enum tagMojiModeID {
	D_MOJI_MODE_NAME,
	D_MOJI_MODE_MUSIC,
	D_MOJI_MODE_MAIL,
	D_MOJI_MODE_STAMPTALK,
	D_MOJI_MODE_PROF_FREE,
	D_MOJI_MODE_MAX
};

//----- state -----
enum tagMojiStateID {
	D_MOJI_STATE_STAY,
	D_MOJI_STATE_OKURI,
	D_MOJI_STATE_DAKU,
	D_MOJI_STATE_MAX
};

//----- keyboard type -----
enum tagMojiTypeID {
	D_MOJI_TYPE_NORMAL,
	D_MOJI_TYPE_DECO,
	D_MOJI_TYPE_QWERTY,
	D_MOJI_TYPE_MAX
};

//----- keyboard kind -----
enum tagMojiKindID {
	D_MOJI_KIND_HIRA,
	D_MOJI_KIND_KATA,
	D_MOJI_KIND_ALPHA,
	D_MOJI_KIND_NUM,
	D_MOJI_KIND_KIGOU,
	D_MOJI_KIND_MAX
};

//----- display update -----
#define	D_MOJI_DISP_CURSOR	0x01
#define	D_MOJI_DISP_BUTTON	0x02
#define	D_MOJI_DISP_MOJI	0x04

//----- bg layer -----
#define	D_MOJI_BG_NONE		-1
#define	D_MOJI_BG_TEXT1		C_PPU_TEXT1
#define	D_MOJI_BG_TEXT2		C_PPU_TEXT2
#define	D_MOJI_BG_TEXT3		C_PPU_TEXT3

//----- return -----
enum tagMojiReturnID {
	D_MOJI_RET_CONTINUE,
	D_MOJI_RET_BACK,
	D_MOJI_RET_ENTER,
	D_MOJI_RET_NOTICE_END,
	D_MOJI_RET_NOTICE_BTN1,
	D_MOJI_RET_NOTICE_BTN2,
	D_MOJI_RET_MAX
};

//----- button ------
#define	D_MOJI_BTN_NORMAL_MAX	25
#define	D_MOJI_BTN_DECO_MAX		25
#define	D_MOJI_BTN_QWERTY_MAX	70

#define	D_MOJI_BTN_ID_BACK		1
#define	D_MOJI_BTN_ID_ENTER		2
#define	D_MOJI_BTN_ID_INPUT		3
#define	D_MOJI_BTN_ID_NOTICE	100

#define	D_MOJI_BTN_QSX			1
#define	D_MOJI_BTN_QSY			210
#define	D_MOJI_BTN_QW			27
#define	D_MOJI_BTN_QH			36
#define	D_MOJI_BTN_QNUM			10
#define	D_MOJI_BTN_SX			1
#define	D_MOJI_BTN_SY			210
#define	D_MOJI_BTN_W			54
#define	D_MOJI_BTN_H			54
#define	D_MOJI_BTN_NUM			5

//----- slide -----
#define	D_MOJI_DIR_UP		0
#define	D_MOJI_DIR_DOWN		1
#define	D_MOJI_DIR_LEFT		2
#define	D_MOJI_DIR_RIGHT	3

#define	D_MOJI_SLIDE_DX		96
#define	D_MOJI_SLIDE_DY		96

//----- key -----
#define	D_MOJI_KEY_UP		0x8000
#define	D_MOJI_KEY_DOWN		0x8001
#define	D_MOJI_KEY_LEFT		0x8002
#define	D_MOJI_KEY_RIGHT	0x8003
#define	D_MOJI_KEY_SPACE	0x8004
#define	D_MOJI_KEY_DEL		0x8005
#define	D_MOJI_KEY_KEY		0x8006
#define	D_MOJI_KEY_KAIGYO	0x8007

//----- notice -----
enum tagMojiInputNoticeID {
	D_MOJI_NOTICE_ID_CAUTION,
	D_MOJI_NOTICE_ID_HINT,
	D_MOJI_NOTICE_ID_YES_NO,
	D_MOJI_NOTICE_ID_NO_YES,
	D_MOJI_NOTICE_ID_SEND_SEL,
	D_MOJI_NOTICE_ID_SENDING,
	D_MOJI_NOTICE_ID_MAX,
};

//======================================================================
//======================================================================
typedef struct tagMojiBtn {
	INT8U	state;
	INT8U	no;
	INT8U	num;
	INT8U	del_no;
	INT16S	timer;
	INT16S	w;
	INT16S	h;
	INT16S	sx;
	INT16S	sy;
	INT16S	sld_x;
	INT16S	sld_y;
	INT16U	*sp_tbl;
} MOJIBTN, *PMOJIBTN;

typedef struct tagMojiInput {
	INT8U	mode;
	INT8U	max;
	INT8U	pos;
	INT8U	state;
	INT8U	old;
	INT8U	same;
	INT8U	key_type;
	INT8U	key_kind;
	INT8U	deco_ofs;
	INT8U	kakutei;
	INT8U	cur_blink;
	INT8U	cur_x;
	INT8U	cur_y;
	INT8U	big_small;
	INT16U	notice;

	INT8U	disp_upd;
	INT8U	disp_buff_ofs;
	INT8U	disp_line_num[D_MOJI_INPUT_LINE_MAX];
	INT8U	disp_line_hosei[D_MOJI_INPUT_LINE_MAX];
	INT8U	disp_line_turn[D_MOJI_INPUT_LINE_MAX];
	INT8U	disp_y_hosei;

	INT16U	buff[D_MOJI_INPUT_BUFF_MAX+1];

	INT32S	bg_layer;

	MOJIBTN		btn;
	COMMONBTN	cbtn;
} MOJIINPUT, *PMOJIINPUT;

//======================================================================
//======================================================================
extern INT8U R_MojiInputSeq;
extern MOJIINPUT R_MojiInput;

//======================================================================
//======================================================================
extern void F_MojiInputInit(INT8U mode, INT8U max, INT16U *buff, INT32S text);
extern void F_MojiInputUninit();
extern INT8U F_MojiInputMain();
extern INT16U *F_GetMojiInputBuff();
extern void F_MojiInputUpdateSet(INT8U upd);
extern INT8U F_CheckDecoMojiUnlock(INT16U code);
extern void F_MojiInputSetSeq(INT8U seq);
extern INT8U F_MojiInputGetSeq();
extern void F_MojiInputNoticeDisp(INT16U id, INT16U *txt1, INT16U *txt2);
extern void F_MojiInputNoticeClear();

#endif
