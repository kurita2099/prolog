#ifndef	__JPAD_INTERRUPT_HEADER__
#define	__JPAD_INTERRUPT_HEADER__
//======================================================================
//======================================================================
extern void F_TimeBaseC_Init();
extern INT32U F_GetLocalPlayTime();
extern void F_SetLocalPlayTime(INT32U time);
extern void F_JpadAdcCallbackSet();
extern void F_AlarmInit();
extern void F_BackLightCtrlOn();
extern void F_BackLightCtrlOff();

#endif
