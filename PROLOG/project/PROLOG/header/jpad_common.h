#ifndef	__JPAD_COMMON_HEADER__
#define	__JPAD_COMMON_HEADER__
//======================================================================
//======================================================================
//----- notice -----
#define	D_NOTICE_DIR_TATE	0
#define	D_NOTICE_DIR_YOKO	1

#define	D_NOTICE_TXT1_TX	80
#define	D_NOTICE_TXT1_TY	88
#define	D_NOTICE_TXT2_TX	64
#define	D_NOTICE_TXT2_TY	120

#define	D_NOTICE_TXT1_YX	184
#define	D_NOTICE_TXT1_YY	88
#define	D_NOTICE_TXT2_YX	168
#define	D_NOTICE_TXT2_YY	120

#define	D_NOTICE_BTN1_DX		32
#define	D_NOTICE_BTN1_DY		43
#define	D_NOTICE_BTN2_DX		32
#define	D_NOTICE_BTN2_DY		123

#define	D_NOTICE_TATE_BASE_X	40
#define	D_NOTICE_TATE_BASE_Y	77
#define	D_NOTICE_TATE_BTN1_X	(D_NOTICE_TATE_BASE_X+D_NOTICE_BTN1_DX)
#define	D_NOTICE_TATE_BTN1_Y	(D_NOTICE_TATE_BASE_Y+D_NOTICE_BTN1_DY)
#define	D_NOTICE_TATE_BTN2_X	(D_NOTICE_TATE_BASE_X+D_NOTICE_BTN2_DX)
#define	D_NOTICE_TATE_BTN2_Y	(D_NOTICE_TATE_BASE_Y+D_NOTICE_BTN2_DY)

#define	D_NOTICE_YOKO_BASE_X	144
#define	D_NOTICE_YOKO_BASE_Y	77
#define	D_NOTICE_YOKO_BTN1_X	(D_NOTICE_YOKO_BASE_X+D_NOTICE_BTN1_DX)
#define	D_NOTICE_YOKO_BTN1_Y	(D_NOTICE_YOKO_BASE_Y+D_NOTICE_BTN1_DY-32)
#define	D_NOTICE_YOKO_BTN2_X	(D_NOTICE_YOKO_BASE_X+D_NOTICE_BTN2_DX)
#define	D_NOTICE_YOKO_BTN2_Y	(D_NOTICE_YOKO_BASE_Y+D_NOTICE_BTN2_DY-32)

//----- button -----
#define	D_CMN_BTN_STATE_STAY	0
#define	D_CMN_BTN_STATE_PUSH	1

#define	D_CMN_BTN_CHK_NONE		0
#define	D_CMN_BTN_CHK_TRIG_ON	1
#define	D_CMN_BTN_CHK_TRIG_OFF	2
#define	D_CMN_BTN_CHK_OUTRANGE	3

//======================================================================
//======================================================================
//----- notice -----
typedef struct tagNoticeSprite {
	INT32U	addr;
	INT32U	ofs;
} NOTICESPRITE, *PNOTICESPRITE;

typedef struct tagNoticeInfo {
	PNOTICESPRITE	sp[5];
	INT16U			txt[2];
	INT16S			pos[2*2];
} NOTICEINFO, *PNOTICEINFO;

//----- button -----
typedef struct tagCommonBtn {
	INT8U		state;
	INT8U		no;
	INT8U		max;
	PTPRANGE	pTbl;
} COMMONBTN, *PCOMMONBTN;

//======================================================================
//======================================================================
extern void F_CommonNumberDisp(INT32U num, INT16S px, INT16S py, INT8U keta, INT8U zero);
extern void F_CommonHexDisp(INT32U num, INT16S px, INT16S py, INT8U keta);
extern INT16U F_GetRand(INT16U max);
extern INT16U F_CommonNoticeDisp(INT16U sp_no, INT8U dir, INT8U line, PNOTICEINFO pInfo);
extern void F_CommonBtnInitOka(PCOMMONBTN pBtn, PTPRANGE pTbl, INT16U max);
extern INT8U F_CommonBtnCheckOka(PCOMMONBTN pBtn);
extern INT32S F_CommonGetKeikaNissu(INT16U year, INT16U month, INT16U day);
extern INT32S F_CommonGetKeikaNissu1(INT16U year, INT16U month, INT16U day);
extern INT32U F_TestFuncOka(INT32U addr);

#endif
