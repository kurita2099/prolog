#ifndef	__JPAD_STRUCT_HEADER__
#define	__JPAD_STRUCT_HEADER__
//======================================================================
//	adc (18+?byte)
//======================================================================
#define	D_JPAD_ADC_BUFF_MAX		8

typedef struct tagJpadAdc {
	INT8U	flag;
	INT8U	level;
	INT16U	timer;
	INT16U	buff[D_JPAD_ADC_BUFF_MAX];
} JPADADC, *PJPADADC;
//======================================================================
//	backlight (8byte)
//======================================================================
typedef struct tagJpadBackLight {
	INT8U	flag;
	INT8U	duty;
	INT8U	cnt;
	INT8U	td_flag;
	INT32U	td_timer;
} JPADBACKLIGHT, *PJPADBACKLIGHT;
//======================================================================
//	system time (32byte)
//======================================================================
#define	D_JPAD_SYS_TIME_RT_EN	0x00000001
#define	D_JPAD_SYS_TIME_CT_EN	0x00000002
#define	D_JPAD_SYS_TIME_CC_EN	0x00000004
#define	D_JPAD_SYS_TIME_CC_DIS	0x00000008

typedef struct tagJpadSysTime {
	INT32U	ctrl;
	INT16U	run_time_1ms;
	INT16U	chg_time_1ms;
	INT32U	run_time;
	INT32U	chg_time;
	INT32U	chg_count;
	INT32U	old[3];
} JPADSYSTIME, *PJPADSYSTIME;

#endif
