#ifndef	__JPAD_IR_HEADER__
#define	__JPAD_IR_HEADER__
//======================================================================
//======================================================================
#define	D_IR_TX_PORT		IO_G13
#define	D_IR_RX_PORT		IO_G5

//------------------------------
#define	D_IR_SEND_ON		0x0001		// bit0 : 送信中
#define	D_IR_SEND_START		0x0002		// bit1 : スタート信号送信中
#define	D_IR_SEND_0			0x0004		// bit2 : 「０」送信中
#define	D_IR_SEND_1			0x0008		// bit3 : 「１」送信中
#define	D_IR_SEND_END		0x0010		// bit4 : エンド信号送信中
#define	D_IR_REC_ON			0x0020		// bit5 : 受信中
#define	D_IR_REC_PORT_WAIT	0x0040		// bit6 : ポート安定待ち
#define	D_IR_REC_START_WAIT	0x0080		// bit7 : スタート信号受信待機
#define	D_IR_REC_START_L	0x0100		// bit8 : スタート信号(Ｌ)受信中
#define	D_IR_REC_START_H	0x0200		// bit9 : スタート信号(Ｈ)受信中
#define	D_IR_REC_DATA_L		0x0400		// bit10 : データ(Ｌ)受信中
#define	D_IR_REC_DATA_H		0x0800		// bit11 : データ(Ｈ)受信中
#define	D_IR_STATE_WAIT		0x1000		// bit12 : ウェイト
#define	D_IR_FINISH			0x2000		// bit13 : ＩＲ終了
#define	D_IR_TIMEOUT		0x4000		// bit14 : タイムアウト
#define	D_IR_ERR			0x8000		// bit15 : エラー

//----- 32KHz(=0.03125ms)単位のカウンタ
#define	D_IR_SEND_1T		13					// 0.03125ms * 13 = 0.40625ms
#define	D_IR_SEND_2T		(D_IR_SEND_1T*2)
#define	D_IR_SEND_3T		(D_IR_SEND_1T*3)
#define	D_IR_SEND_4T		(D_IR_SEND_1T*4)
#define	D_IR_SEND_8T		(D_IR_SEND_1T*8)
#define	D_IR_SEND_12T		(D_IR_SEND_1T*12)

#define	D_IR_REC_1T_MIN		5			// 0.15625ms		//7			// 0.21875ms
#define	D_IR_REC_1T_MAX		21			// 0.65625ms		//19		// 0.59375ms
#define	D_IR_REC_LOW_T		24			// 0.75ms(Low/High の境界)
#define	D_IR_REC_3T_MIN		34			// 1.0625ms
#define	D_IR_REC_3T_MAX		48			// 1.5ms
#define	D_IR_REC_4T_MIN		45			// 1.40625ms
#define	D_IR_REC_4T_MAX		64			// 2.0ms
#define	D_IR_REC_8T_MIN		90			// 2.8125ms
#define	D_IR_REC_8T_MAX		128			// 4.0ms
#define	D_IR_REC_2MS		64			// 2.0ms		// port 安定待ち
#define	D_IR_REC_8MS		256			// 8.0ms

#define	D_IR_WAIT_250MS		8000		// 250.0ms

#define	D_IR_TIMEOUT_LIMIT	(30*1000)

//------------------------------
#define	D_IR_ALPHA_ID		0x04
#define	D_IR_BETA_ID		0x04
#define	D_IR_DIAMOND_ID		0x05
#define	D_IR_PREMIUM_ID		0x06
#define	D_IR_JSPOD_ID		0x07
#define	D_IR_MUSHI_ID		0x08
#define	D_IR_JSPREMIUM_ID	0x09
#define	D_IR_JPAD_ID		0x0B

#define	D_IR_A_MAIL_SIZE	15
#define	D_IR_B_MAIL_SIZE	133
#define	D_IR_P_MAIL_SIZE	145
#define	D_IR_PROF_SIZE		146
#define	D_IR_ITEM_SIZE		7
#define	D_IR_IKUSEI_SIZE	8
#define	D_IR_LINE_T_SIZE	48
#define	D_IR_LINE_S_SIZE	30
#define	D_IR_PAD_PROF_SIZE	484
#define	D_IR_PAD_ITEM_SIZE	10
#define	D_IR_MYTOWN_SIZE	12

#define	D_IR_B_MAIL_KIND	0xFF
#define	D_IR_P_MAIL_KIND	0xEF
#define	D_IR_PROF_KIND		0xEE
#define	D_IR_ITEM_KIND		0xED
#define	D_IR_IKUSEI_KIND	0xEC
#define	D_IR_LINE_T_KIND	0xEB
#define	D_IR_LINE_S_KIND	0xEA
//#define	D_IR_JSPP_AP52_KIND	0xE9

#define	D_IR_PAD_PROF_KIND	0xDF
#define	D_IR_PAD_ITEM_KIND	0xDE
#define	D_IR_MYTOWN_KIND	0xDD

//------------------------------
#define	D_IR_BUFF_SIZE		512

#define	D_IR_ONCE_SIZE_LIMIT	256

#define	D_IR_OFS_CUSTOM_CODE1	0
#define	D_IR_OFS_CUSTOM_CODE2	1
#define	D_IR_OFS_PARITY			2
#define	D_IR_OFS_BODY_ID		3
#define	D_IR_OFS_KIND_ID		4
#define	D_IR_OFS_DATA			5

#define	D_IR_DATA_CUSTOM_CODE1	0x74
#define	D_IR_DATA_CUSTOM_CODE2	0x53
#define	D_IR_DATA_PARITY		0x05

//======================================================================
//======================================================================
typedef struct tagIrData {
	INT16U	status;
	INT8U	*buff_addr;
	INT16U	count_l;
	INT16U	count_h;
	INT8U	high_low;
	INT8U	data;
	INT16U	size;
	INT16U	wait_count;
	INT8U	bit_count;
	INT8U	temp;
} IRDATA, PIRDATA;

//======================================================================
//======================================================================
extern INT8U R_IrBuff[D_IR_BUFF_SIZE];
extern INT32U R_IrTimer;
extern IRDATA R_IrData;

//======================================================================
//======================================================================
extern	void F_IrInit();
extern	void F_IrUninit();
extern	void F_IrBuffClear();
extern	INT16U F_GetIrStatus();
extern	void F_IrSendStart(INT8U code, INT16U size);
extern	void F_IrRecStart();

extern	void F_IrSendMailBuffSet(INT8U kind, INT16U *pMail);
extern	void F_IrSendProfBuffSet(INT8U kind, PPROFDATA pProf);
extern	void F_IrRecProfBuffSet(PPROFDATA pProf);

#endif
