//======================================================================
//	ジュエルパッド・外部アプリ
//======================================================================
#include	"jpad_header.h"
#include	"jpad_app_sys.h"

//======================================================================
//======================================================================
extern void F_AP104ModeMain();
extern void F_AP21ModeMain();
extern void F_OpeningModeMain();
extern void F_DEMOOPEN_Main();
//======================================================================
//======================================================================
void AppMain()
{
	INT8U	key;

	DBG_PRINT("sub program start\r\n");

	m_R_SeqNo = 0;

	while(1)
	{
		key = 0;

		m_R_MainFrame = 0;
		m_R_FrameCountP++;
		if(m_R_FrameCountM) {
			m_R_FrameCountM--;
		}

		// audio power check
		mf_F_AsdCheck();

		// charge check
		mf_F_ChargeCheck();

		// adc check
		mf_F_JpadAdcUpdate();
		if((mp_R_JpadAdc->level == 1) && (m_R_ModeNo > D_STARTUP_MODE) && (m_R_ModeNo != D_LVD_MODE)) {
			// 電池切れ画面
			mf_F_LvdModeInit();
			R_SubProgState = D_SPROG_FINISH;
			break;
		} else if(mp_R_JpadAdc->level == 0) {
			// 即パワーオフ
			mf_F_JpadPowerOff();
			R_SubProgState = D_SPROG_FINISH;
			break;
		}

		// tokei check
		mf_F_TokeiCheck();

		// sdc check
		mf_F_SdcCheck();

		// input check
		mf_F_TpUpdate();
		mf_F_HomeBtnUpdate();
		if(m_R_HomeBtn & (D_HOME_BTN_TRIG_ON)) {
			// ＨＯＭＥボタンが押された
			key |= 0x01;
		}
		if(mp_R_TpCtrl->flag & (D_TP_CF_TRIG_ON|D_TP_CF_TRIG_OFF)) {
			// タッチパネルが押された／離された
			key |= 0x02;
		}

		// power off check
		if((m_R_ModeNo > D_STARTUP_MODE) && mf_F_HomeBtnGetLongOn()) {
			// ＨＯＭＥボタン長押しによるスリープ
			mf_F_JpadPowerOff();
		}

		// auto power off check
		mf_F_AutoPowerOffCtrl(key);
		if(m_R_ModeNo == D_LVD_MODE) {
			R_SubProgState = D_SPROG_FINISH;
			break;
		}

		// backlight tonedown check
		if(mf_F_BackLightTDCtrl(key) == 0x02) {
			// 点灯した直後は１回空ループ
			while(m_R_MainFrame < D_MAIN_FRAME_RATE);
			continue;
		}

		// mode
		//F_AP104ModeMain();
		F_DEMOOPEN_Main();
		if(R_SubProgState != D_SPROG_CONTINUE) {
			break;
		}

		// font deco anim
		mf_F_FontDecoAnim();

		// infobar update
		mf_F_InfoBarUpdate();

		// fade update
		mf_F_FadeUpdate();

		// ppu update
		mf_F_PPU_UpdateCheck();

		// frame wait
		while(m_R_MainFrame < D_MAIN_FRAME_RATE)
		{
			if(mf_F_AsrGetCtrlFlag() & D_ASR_CTRL_CHK) {
				// 音声認識中
				mf_F_AsrCheck();
			} else if((D_MAIN_FRAME_RATE - m_R_MainFrame) > 10) {
				// 通常時：フレームウェイト
				mf_OSTimeDly(1);
			}
		}
	}

	DBG_PRINT("sub program finish\r\n");
}
